KVSの製作者である坂本先生が作ったDepthPeeling
ポスター発表での比較画像を作るのに

コンパイル方法:
	make

実行方法:
	./DepthPeelingForKVS PLY_FILE

補足:
	ピール回数は動的ではない、
	renderer->setNumberOfPeels( 200 )
	で指定