#include <iostream>

#include <kvs/glut/Application>
#include <kvs/glut/Screen>
#include <kvs/PolygonObject>
#include <kvs/Light>
#include <kvs/Ply>
#include <kvs/ObjectManager>
#include <kvs/RotationMatrix33>

#include "DepthPeelingRenderer.h"

int main(int argc, char *argv[]){

	if(argc < 2){
		std::cerr << "DepthPeelingForKVS PLY_FILE" << std::endl;
		return 1;
	}
	
	kvs::glut::Application app(argc, argv);
	kvs::Ply ply;
	ply.read(argv[1]);

	kvs::PolygonObject* object = new kvs::PolygonObject();
	object->setCoords(ply.coords());
	object->setColors(ply.colors());
	object->setNormals(ply.normals());
	object->setConnections(ply.connections());
	object->setPolygonTypeToTriangle();
	object->setOpacity(0.2 * 255);

	local::DepthPeelingRenderer* renderer = new local::DepthPeelingRenderer();
	renderer->setName("Depth Peeling for KVS");
	renderer->setBackgroundColor( kvs::RGBColor( 0, 0, 0));
	renderer->setNumberOfPeels( 200 );


	kvs::glut::Screen screen(&app);
	screen.setTitle("Depth Peeling for KVS");
	screen.setSize( 1024 , 1024);
	screen.registerObject(object, renderer);
	screen.setBackgroundColor( kvs::RGBColor ( 0, 0, 0));

	screen.scene()->camera()->setPosition( kvs::Vector3f(0, 0, 8.0f));
	screen.scene()->objectManager()->rotate(kvs::XRotationMatrix33(90.0f));
	screen.create();
	screen.show();

	return app.run();
}
