#!/bin/sh

INPUT_DIR="../../models/Teapot/*.ply"

for file in `\find $INPUT_DIR  -maxdepth 1 -type f`; do

	LOG=`basename $file`

	spbrpol $file 0.2 1000 | tee $LOG.log

done
