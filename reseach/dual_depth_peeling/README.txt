src/dual_depth_peelingの中にプログラムがある

コンパイル方法:
	make
実行方法:
	./dual_depth_peeling OBJ_FILE
補足:
	対応してる3DフォーマットはOBJのみ
	必要に応じてMeshLabか自分でプログラム組んで変換すること
	
	Linuxで動かすときはMakefileの
	OPENGL_LIB=-framework OpenGL
	GLUT_LIB=-framework GLUT
	をそれぞれ変更すること(どっちか空で、もう片方に-lglut -lGLU -lGLとかでいい)
	
	また、sprintf辺りでエラー吐いたら該当ソースコードに
	#include <stdio.h>
	を追加
	