#include <iostream>
#include <LeastSquaresExp.h>
#include <LeastSquaresInv.h>
#include <LeastSquaresLine.h>

int main(int argc, char const *argv[]){

	// LeastSquaresExp least_squares_method;
	LeastSquaresInv least_squares_method;
	// LeastSquaresLine least_squares_method;

	if(argc < 2){
		std::cout << "LeastSquaresMethod INPUT_CSV_FILE" << std::endl;
		return 1;
	}

	least_squares_method.read(argv[1]);
	least_squares_method.exec();

	// std::cout << "y=" << least_squares_method.a() << "*" 
	// 		  << least_squares_method.b() << "^x" << std::endl;

	std::cout << "y=" << least_squares_method.a() << "x^" << least_squares_method.b() << std::endl;
	
	// std::cout << "y=" << least_squares_method.b() << "x+" <<least_squares_method.a() << std::endl;  


	return 0;
}