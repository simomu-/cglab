#include <LeastSquaresExp.h>
#include <fstream>
#include <iostream>

LeastSquaresExp::LeastSquaresExp(const std::vector<Data>& data){
	m_dataArray = data;
}

void LeastSquaresExp::read(const std::string& filepath){
	std::ifstream fin;
	fin.open(filepath.c_str());
	if(fin.fail()){
		std::cerr << "can not open " << filepath << std::endl;
		return;
	}

	std::cout << "read " << filepath << std::endl;

	double x = 0;
	double y = 0;
	char buf[256];

	while(!fin.eof()){
		fin.getline(buf, sizeof(buf));
		sscanf(buf, "%lf,%lf", &x, &y);
		std::cout << x << "," << y << std::endl;
		y = log(y);
		Data d(x, y);
		m_dataArray.push_back(d);
	}

	fin.close();
	std::cout << "read complete" << std::endl;
}

void LeastSquaresExp::setDataArray(const std::vector<Data>& data){
	m_dataArray = data;
}

void LeastSquaresExp::exec(){

	std::cout << "start calculate" << std::endl;

	double sum_xy = 0;
	double sum_x = 0;
	double sum_y = 0;
	double sum_x2 = 0;

	for(Data d : m_dataArray){
		sum_xy += d.x() * d.y();
		sum_x += d.x();
		sum_y += d.y();
		sum_x2 += pow(d.x(), 2);
	}

	size_t N = m_dataArray.size();
	m_a = (N * sum_xy - sum_x * sum_y) / (N * sum_x2 - pow(sum_x, 2));
	m_b = (sum_x2 * sum_y - sum_xy * sum_x) / (N * sum_x2 - pow(sum_x, 2));

	std::cout << "calculate complete" << std::endl;

}