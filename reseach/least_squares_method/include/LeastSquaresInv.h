#pragma once
#include <vector>
#include <string>
#include <cmath>

#include <Data.h>

class LeastSquaresInv{

	std::vector<Data> m_dataArray;
	double m_a;
	double m_b;

public:
	LeastSquaresInv() : m_a(0), m_b(0){};
	LeastSquaresInv(const std::vector<Data>& data);
	~LeastSquaresInv() = default;

	double a() const{ return pow(M_E, m_a);}
	double b() const{ return m_b;}

	void read(const std::string& filepath);
	void setDataArray(const std::vector<Data>& data);
	void exec();
	
};