#pragma once
#include <vector>
#include <string>
#include <cmath>

#include <Data.h>

class LeastSquaresExp{

	std::vector<Data> m_dataArray;
	double m_a;
	double m_b;

public:
	LeastSquaresExp() : m_a(0), m_b(0){};
	LeastSquaresExp(const std::vector<Data>& data);
	~LeastSquaresExp() = default;

	double a() const{ return pow(M_E, m_a);}
	double b() const{ return pow(M_E, m_b);}

	void read(const std::string& filepath);
	void setDataArray(const std::vector<Data>& data);
	void exec();
	
};