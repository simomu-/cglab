#pragma once

class Data{

	double m_x;
	double m_y;

public:
	Data() : m_x(0), m_y(0){}
	Data(double x, double y): m_x(x), m_y(y){}
	~Data() = default;

	double x() const { return m_x;}
	double y() const { return m_y;}
	void setData(double x, double y){m_x = x; m_y = y;}
	
};