#pragma once
#include <vector>
#include <string>
#include <cmath>

#include <Data.h>

class LeastSquaresLine{

	std::vector<Data> m_dataArray;
	double m_a;
	double m_b;

public:
	LeastSquaresLine() : m_a(0), m_b(0){};
	LeastSquaresLine(const std::vector<Data>& data);
	~LeastSquaresLine() = default;

	double a() const{ return m_a;}
	double b() const{ return m_b;}

	void read(const std::string& filepath);
	void setDataArray(const std::vector<Data>& data);
	void exec();
	
};