#!/bin/sh

LOG_DIR="Log";

for file in `\find $LOG_DIR/*.log -maxdepth 1 -type f`; do

	./LogExtraction.php $file;

done
