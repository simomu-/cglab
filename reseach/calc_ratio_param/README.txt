最小二乗法を行うためのデータを作成する
./MakeLog.shでログを取り、./LogExtractionAll.shで必要なデータを抽出する

./MakeLog.sh
	../models/SquarePolygonForParam(ポリゴン数2からhogeの正方形ポリゴンモデル)
	に対して順番にspbrpolを実行し、その時のログを残す
	この時のspbrpolは描画処理をスキップし即終了する設定にすること
	出力したログ群はLog/に格納される

./LogExtractionAll.sh
	Log/の中のログからポリゴン数、生成点数、ポリゴン面積のリストを出力する
	データは標準出力されるので、
	./LogExtractionAll.sh > data.txt
	とかで適当なテキストファイルにリダイレクトする
