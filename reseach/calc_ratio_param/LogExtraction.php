#!/usr/bin/php
<?php
$extract_line1 = 24;
$extract_row1 = 4;

$extract_line2 = 15;
$extract_row2 = 1;

$logs = explode("\n", file_get_contents($argv[1]));
$log_line = explode(" ", $logs[$extract_line1]);
$extract1 = str_replace("\n", "", $log_line[$extract_row1]);

$log_line = explode(" ", $logs[$extract_line2]);
$extract2 = str_replace("\n", "", $log_line[$extract_row2]);
$extract2 = str_replace("P(area):", "", $extract2);

echo str_replace("sp", "", basename($argv[1], ".ply.log")) . "," . $extract1 . "," . $extract2 . "\n";

?>