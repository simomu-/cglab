#!/bin/sh

INPUT_DIR="../models/SquarePolygonForParam"
LOG_DIR="Log/"

for file in `\find $INPUT_DIR/*.ply  -maxdepth 1 -type f`; do

	LOG=`basename $file`

	spbrpol $file 0.2 1000 | tee $LOG.log

done

mv *.log $LOG_DIR
