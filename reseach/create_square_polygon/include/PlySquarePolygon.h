#ifndef PLY_SQUARE_POLYGON_H
#define PLY_SQUARE_POLYGON_H 
#include <PlyData.h>

class PlySquarePolygon : public PlyData{

public:
	PlySquarePolygon();
	PlySquarePolygon(int fnum);
	~PlySquarePolygon() = default;
	
	void CreateMesh(int fnum);
};

#endif