#include <PlySquarePolygon.h>
#include <cmath>
#include <vector>

PlySquarePolygon::PlySquarePolygon(){

}

PlySquarePolygon::PlySquarePolygon(int fnum){
	CreateMesh(fnum);
}

void PlySquarePolygon::CreateMesh(int fnum){
	std::vector<double> v;
	int div = std::sqrt(fnum / 2) + 1;
	double delta = 1.0 / (double)(div - 1);
	for(int y = 0; y < div; y++){
		for(int x = 0; x < div; x++){
			v.push_back(x * delta);
			v.push_back(y * delta);
			v.push_back(0.0);
		}
	}

	std::vector<int> f;
	for(int i = 0; i < div - 1; i++){
		for(int j = 0; j < div - 1; j++){

			int f0 = i * div + j;
			int f1 = i * div + j + 1;
			int f2 = (i + 1) * div + j + 1;
			int f3 = (i + 1) * div + j;
			f.push_back(f0);
			f.push_back(f1);
			f.push_back(f2);
			f.push_back(f0);
			f.push_back(f2);
			f.push_back(f3); 
		}
	}
	PlyData::SetMesh(v, f);
}