#include <iostream>
#include <PlySquarePolygon.h>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

const std::string outputDirectory = "../models/SquarePolygon/";

int main(int argc, char const *argv[]){

	if(argc < 2){
		std::cerr << "CreateSquarePolygon FACE_COUNT" << std::endl;
		std::cout << "CreateSquarePolygon FACE_COUNT OUTPUT_PLY_NAME" << std::endl;
		return 1;
	}

	int faceCount = std::stoi(argv[1]);

	PlySquarePolygon ply = PlySquarePolygon(faceCount);

	std::string outputFileName;
	if(argc == 3){
		outputFileName = argv[2];
	}else{
		std::ostringstream sout;
		sout << std::setfill('0') << std::setw(6) << ply.GetFaceCount();
		outputFileName = outputDirectory + "sp" + sout.str() + ".ply";
	}
	ply.Write(outputFileName);
	std::cout << "Create Ply File -> " << outputFileName << std::endl;
	std::cout << "Face Count = " << ply.GetFaceCount() << std::endl;
	std::cout << "Vertex Count = " << ply.GetVertexCount() << std::endl;
	
	return 0;
}