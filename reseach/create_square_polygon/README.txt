1*1の正方形ポリゴンモデルをFACE_COUNTだけ分割したものを作る
ポリゴン数が増え、ポリゴンが小さくなったときの平均輝度値を求める実験に使用

コンパイル方法:
	make

使用方法:
	./CreateSquarePolygon FACE_COUNT
	./CreateSquarePolygon FACE_COUNT OUTPUT_PLY_NAME

補足:
	正確にFACE_COUNTだけ分割されるのではなく、
	FACE_COUNTにもっとも近い面数になるように等分割される
	
	OUTPUT_PLY_NAMEを指定しなかった場合、
	models/SquarePolygon/sp{ポリゴン数}.ply
	に保存される

	./CreateSample.sh
	FACE_COUNTを一定範囲内で変えながら
	./CreateSquarePolygonを実行するだけのシェルスクリプト