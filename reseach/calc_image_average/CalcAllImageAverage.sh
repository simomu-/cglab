#!/bin/sh

OUTPUT_NAME=512

for file in `\find InputImage -maxdepth 1 -type f`; do
	FILE_NAME=`echo $file | sed 's/\.[^\.]*$//'`
	BASE_NAME=`basename $FILE_NAME`
	EXTENTION=`echo $file | sed 's/^.*\.\([^\.]*\)$/\1/'`
	# echo $FILE_NAME	
	# echo $BASE_NAME
	# echo $EXTENTION
	if [ $EXTENTION = "bmp" ]; then 
		./CalcImageAverage $file >> $OUTPUT_NAME.txt
	fi
done
