#include <BmpPicture.h>
#include <iostream>
#include <fstream>
#include <string>

BmpPicture::BmpPicture(std::string fileName) : width(0), height(0){

	std::ifstream file( fileName.c_str(), std::ios::in | std::ios::binary );

	if (!file){
		std::cout << "Error" << std::endl;
	}

	int offset = 0;
	file.seekg(10, std::ios::cur);
	file.read((char *)&offset, 4);

	file.seekg(4,std::ios_base::cur);
	file.read((char *)&width, 4);
	file.read((char *)&height, 4);
	// file.seekg(28,std::ios_base::cur);
	file.seekg(offset, std::ios::beg);

	colorDataSize = width * height * 3;
	colorData = new unsigned char[colorDataSize];

	size_t padding = ( width * 3 ) % 4;
	size_t upper_left = (height - 1) * width * 3;

	for (size_t y = 0; y < height; y++){
		size_t line_index = upper_left - y * width * 3;
		for(size_t x = 0; x < width; x++){
			size_t index = line_index + 3 * x;
			file.read((char *)(colorData + index + 2), 1);
			file.read((char *)(colorData + index + 1), 1);
			file.read((char *)(colorData + index + 0), 1);
		}
		file.seekg(padding, std::ios::cur);
	}

}


BmpPicture::~BmpPicture(){
	delete[] colorData;
}

long BmpPicture::GetWidth() const{
	return width;
}

long BmpPicture::GetHeight() const{
	return height;
}

int BmpPicture::GetColorDataSize() const{
	return colorDataSize;
}

unsigned char* BmpPicture::GetColorArray(){
	return colorData;
}

std::array<unsigned char, 3> BmpPicture::GetPixel(int x, int y) const{
	std::array<unsigned char, 3> color;
	int index = 3 * (y * width + x);
	color[0] = colorData[index + 0];
	color[1] = colorData[index + 1];
	color[2] = colorData[index + 2];
	return color;
}

void BmpPicture::SetPixel(int x, int y, std::array<unsigned char, 3> color){
	int index = 3 * (y * width + x);
	colorData[index + 0] = color[0];
	colorData[index + 1] = color[1];
	colorData[index + 2] = color[2];
}

void BmpPicture::ToGrayScale(){
	ToGrayScale(MeanValue);
}

void BmpPicture::ToGrayScale(GlayScaleType type){
	switch(type){
		case MeanValue:
			for(int x = 0; x < GetWidth(); x++){
				for(int y = 0; y < GetHeight(); y++){
					unsigned char c = (GetPixel(x, y)[0] + GetPixel(x, y)[1] + GetPixel(x, y)[2]) / 3;
					std::array<unsigned char, 3> color = {c, c, c};
					SetPixel(x, y, color);
				}
			}
			break;
		case HDTVWeighted:
			for(int x = 0; x < GetWidth(); x++){
				for(int y = 0; y < GetHeight(); y++){
					unsigned char c = 0.2126f * (float)GetPixel(x, y)[0] + 
									  0.7152f * (float)GetPixel(x, y)[1] + 
									  0.0722f * (float)GetPixel(x, y)[2];
					std::array<unsigned char, 3> color = {c};
					SetPixel(x, y, color);
				}
			}
			break;
	}
}




