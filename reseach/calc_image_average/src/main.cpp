#include <BmpPicture.h>
#include <FilePath.h>
#include <iostream>
#include <fstream>
#include <string>
#include <array>
#include <cmath>

const int sampleWidth = 300;
const int sampleHeight = 300;

int main(int argc, char const *argv[]){

	std::array<int, 256> histgram = {0};
	double average = 0;
	double standardDeviation = 0;
	int halfWidth = sampleWidth / 2;
	int halfHeight = sampleHeight / 2;

	if(argc < 2){
		std::cerr << "CalcImageAverage BMP_FILE_NAME" << std::endl;
		return 1;
	}

	BmpPicture bmp = BmpPicture(std::string(argv[1]));

	bmp.ToGrayScale();

	for(int x = bmp.GetWidth() / 2 - halfWidth; x < bmp.GetWidth() / 2 + halfWidth; x++){
		for(int y = bmp.GetHeight() / 2 - halfHeight; y < bmp.GetHeight() / 2 + halfHeight; y++){
			average += bmp.GetPixel(x, y)[0];
		}
	}

	average /= sampleWidth * sampleHeight;

	for(int x = bmp.GetWidth() / 2 - halfWidth; x < bmp.GetWidth() / 2 + halfWidth; x++){
		for(int y = bmp.GetHeight() / 2 - halfHeight; y < bmp.GetHeight() / 2 + halfHeight; y++){
			standardDeviation += (bmp.GetPixel(x, y)[0] - average) * (bmp.GetPixel(x, y)[0] - average);
		}
	}

	standardDeviation /= sampleWidth * sampleHeight;
	standardDeviation = std::sqrt(standardDeviation);

	FilePath path = FilePath(argv[1]);
	// path.ChangeFileName(path.GetFileName());
	// path.ChangeExtension("txt");
	std::cout << path.GetFilePath() << "," << average << "," << standardDeviation << std::endl;
	
	return 0;
}