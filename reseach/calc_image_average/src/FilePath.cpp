#include <FilePath.h>

FilePath::FilePath() : filePath(""){

}

FilePath::FilePath(std::string f) : filePath(f){

}

void FilePath::SetFilePath(std::string f){
	filePath = f;
}

std::string FilePath::GetExtension() const{
	int num = filePath.find_last_of('.');
	std::string s = filePath.substr(num + 1, filePath.size() - num);
	return s;
}

std::string FilePath::GetBaseName() const{
	int extMum = filePath.find_last_of('.');
	std::string s = filePath.substr(0, extMum);
	return s;
}

std::string FilePath::GetFileName() const{
	int pathNum = filePath.find_last_of('/');
	int extMum = filePath.find_last_of('.');
	std::string s = filePath.substr(pathNum + 1, extMum - pathNum - 1);
	return s;
}

std::string FilePath::GetDirectryPath() const{
	int pathNum = filePath.find_last_of('/');
	std::string s = filePath.substr(0, pathNum);
	return s;
}

void FilePath::ChangeFileName(std::string f){
	std::string s = GetDirectryPath() + "/" + f + "." + GetExtension();
	filePath = s;
}

void FilePath::ChangeExtension(std::string e){
	std::string s = GetBaseName() + "." + e;
	filePath = s;
}

