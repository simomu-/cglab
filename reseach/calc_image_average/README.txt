出力画像の中央300*300を切り出し、平均輝度値と標準偏差を算出する

コンパイル方法:
	make

実行方法:
	./CalcImageAverage BMP_FILE_NAME

補足:
	対応画像はBMPのみ
	SPBRにおいて輝度が不安定になる可能性のあるポリゴンモデルの端を計測から外すため
	中央300*300をサンプルとしている

	残してあるログ
		{解像度}a{不透明度}_{補足}.txt
		例 512a2_after.txt => 解像度512不透明度0.2、適当的点削減を使用後

	

その他シェルスクリプト:
	./MakeAllImage.sh
		../models/SquarePolygonにある正方形ポリゴンモデルを全て読み出し、
		順番にspbrpolを実行する
		spbrpolで取得した画像を特定のディレクトリに格納する
		spbrpolはAutoCaptureScreenが有効になっていること

	./CalcAllImageAverage.sh
		MakeAllImage.shで取得した画像を格納したディレクトリに対し、
		その全ての画像の平均輝度値と標準偏差を求め結果を特定のファイルの保存する