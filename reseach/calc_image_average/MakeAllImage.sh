#!/bin/sh

MODEL_DIR=../models/SquarePolygon
OUTPUT_DIR=InputImage/
LOG_DIR=Log/

for file in `\find $MODEL_DIR/*.ply -maxdepth 1 -type f`;do
	FILE_NAME=`basename $file`
	spbrpol $file 0.2 1000 | tee $FILE_NAME.log
done

mv *.bmp $OUTPUT_DIR
mv *.log $LOG_DIR

#cd InputImage
#for file in `\find *.spbr -maxdepth 1 -type f`;do
#	spbr $file
#done
#cd ../


