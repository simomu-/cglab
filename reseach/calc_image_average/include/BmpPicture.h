#ifndef BMP_PICTURE_H
#define BMP_PICTURE_H 

#include <iostream>
#include <fstream>
#include <string>
#include <array>

class BmpPicture{

	enum GlayScaleType{
		MeanValue,
		HDTVWeighted
	};

	long width;
	long height;
	unsigned char* colorData;
	int colorDataSize;

public:
	static BmpPicture ConvertToGrayScale(BmpPicture picture);

	BmpPicture(std::string fileName);
	~BmpPicture();

	long GetWidth() const;
	long GetHeight() const;
	int GetColorDataSize() const;
	unsigned char* GetColorArray();
	std::array<unsigned char, 3> GetPixel(int x, int y) const;
	void SetPixel(int x, int y, std::array<unsigned char, 3> color);
	void ToGrayScale();
	void ToGrayScale(GlayScaleType type);
	
};

#endif

