#ifndef FILE_PATH_H
#define FILE_PATH_H 

#include <string>

class FilePath{

	std::string filePath;

public:
	FilePath();
	FilePath(std::string f);
	~FilePath() = default;

	void SetFilePath(std::string f);
	std::string GetFilePath() const {return filePath;};
	std::string GetExtension() const;
	std::string GetBaseName() const;
	std::string GetFileName() const;
	std::string GetDirectryPath() const;

	void ChangeFileName(std::string f);
	void ChangeExtension(std::string e);
};

#endif