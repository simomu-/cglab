///////////////////////
///// spcomment.h /////
///////////////////////

#if !defined  SPECIAL_COMMENT_HH
#define       SPECIAL_COMMENT_HH

// #/Origin O.x O.y O.z
const char ORIGIN_COMMAND           [] = "#/Origin";

// #/BGColorByteRGB (uByte r) (uByte g) (uByte b)
const char BG_COLOR_BYTE_COMMAND    [] = "#/BGColorByteRGB" ;

// #/BaseVector e1.x e1.y e1.z e2.x e2.y e2.z 
const char BASE_VECTOR_COMMAND      [] = "#/BaseVector";

// #/ColorRGB (double r) (double g) (double b)
const char COLOR_COMMAND            [] = "#/ColorRGB"     ; 

// #/ColorRGBByte (uByte r) (uByte g) (uByte b)
const char COLOR_BYTE_COMMAND       [] = "#/ColorByteRGB" ;

// #/UseKVSDefaultCamera 1/0 (default 0) 
const char USE_KVS_DEFAULT_CAMERA_COMMAND [] = "#/UseKVSDefaultCamera" ;

// #/UseNormals 1 or 0  (default is 1)
const char USE_NORMALS_COMMAND       [] = "#/UseNormals" ;

// #/PointSize (uInt size)
const char POINT_SIZE_COMMAND       [] = "#/PointSize" ;

// #/RepeatLevel (uInt size) (default is 1)
const char REPEAT_LEVEL_COMMAND     [] = "#/RepeatLevel" ;

// #/ImageResolution (uInt size)
const char IMAGE_RESOLUTION_COMMAND [] = "#/ImageResolution" ;

// #/WireframeBox  xmin ymin zmin xmax ymax zmax 
const char WIREFRAME_BOX_COMMAND    [] = "#/WireframeBox" ;

// #/FPS 1 or 0 (default is 0) 
const char FPS_COMMAND              [] = "#/FPS" ;

// #/LOD 1 or 0  (default is 1)
const char LOD_COMMAND              [] = "#/LOD" ;

// #/CameraPosition  x y z  (default is (0,0,12))
const char CAMERA_POSITION_COMMAND  [] = "#/CameraPosition" ;

// #/LookAt          x y z  (default is (0,0,0))
const char LOOK_AT_COMMAND          [] = "#/LookAt" ;

// #/SubpixelLevel   (uInt size)  (default is 1)
const char SUBPIXEL_LEVEL_COMMAND   [] = "#/SubpixelLevel" ;

// #/ParticleZooming 1 or 0  (default is 1)
const char PARTICLE_ZOOMING_COMMAND [] = "#/ParticleZooming" ;

// #/CameraFar 1 or 0 (default is 0)
const char CAMERA_FAR_COMMAND [] = "#/CameraFar" ;

// #/ViewAngle (double angle_deg) (default is 0)
const char VIEW_ANGLE_COMMAND [] = "#/ViewAngle" ;

// #/ObjectZXRotation (double zrot_angle_deg double xrot_angle_deg ) 
//                     (defaults are 0 0)
const char OBJECT_ZX_ROT_COMMAND [] = "#/ObjectZXRotation" ;

// #/BoundingBox xmin ymin zmin  xmax ymax zmax
const char BOUNDING_BOX_COMMAND [] = "#/BoundingBox" ;

// #/ReverseNormals 1 or 0 (default 0)
const char REVERSE_NORMALS_COMMAND [] = "#/ReverseNormals" ;

// #/Shuffle 1 or 0 (default 1)
const char SHUFFLE_COMMAND [] = "#/Shuffle" ;

// #/RepeatLevelZooming 1 or 0  (default is 1)
const char REPEAT_LEVEL_ZOOMING_COMMAND [] = "#/RepeatLevelZooming" ;

// #/MinRepeatLevelInZooming  num (default is 10)
const char MIN_REPEAT_LEVEL_IN_ZOOMING_COMMAND [] = "#/MinRepeatLevelInZooming" ;

// #/UseParticleByParticleColorByteRGB (default is 0)
const char USE_PARTICLE_BY_PARTICLE_COLOR_BYTE_RGB [] = "#/UseParticleByParticleColorByteRGB" ;

// #/LambertShading ka kd (default is 1.0 1.0)
const char LAMBERT_SHADING_COMMAND [] = "#/LambertShading" ;

// #/CameraZoom zoom_factor (default is 1.0)
const char CAMERA_ZOOM_COMMAND [] = "#/CameraZoom" ;  

// #/SmartCameraZoom zoom_factor (default is 1.0)
const char SMART_CAMERA_ZOOM_COMMAND [] = "#/SmartCameraZoom" ;  

#endif
