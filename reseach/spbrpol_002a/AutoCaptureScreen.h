#ifndef AUTO_CAPTURE_SCREEN_H
#define AUTO_CAPTURE_SCREEN_H

#include "FilePath.h"

class AutoCaptureScreen : public kvs::glut::Screen
{

  FilePath file_path;
  bool isAutoCapture;

 public:
  AutoCaptureScreen(kvs::glut::Application* application, std::string name)
    :kvs::glut::Screen(application), file_path(name), isAutoCapture(true){};
  
  void setFileName(std::string name){ file_path.SetFilePath(name);}
  
  void setIsAutoCapture(bool capture){ isAutoCapture = capture;}

  void paintEvent(void)
  {
    kvs::glut::Screen::paintEvent();
    if(isAutoCapture){
      kvs::ColorImage snapshot_image;
      snapshot_image = scene()->camera()->snapshot();
      file_path.SetFilePath(file_path.GetFileName() + ".bmp");
      snapshot_image.write(file_path.GetFilePath());
    }
    exit(0); 
  }
};

#endif
