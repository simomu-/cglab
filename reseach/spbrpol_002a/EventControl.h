///////////////////////////////////////
///// EventControl.h (for KVS 2)  /////
///////////////////////////////////////

#if !defined  SSM__EVENT_CONTROL_INCLUDE
#define       SSM__EVENT_CONTROL_INCLUDE

#include <kvs/InitializeEventListener>
#include <kvs/KeyPressEventListener>
#include <kvs/Key>
#include <kvs/glut/Screen>
#include <kvs/ColorImage>
#include <kvs/Camera>

const kvs::Vector3f DEFAULT_LIGHT_POSITION (12.0, 12.0, 12.0) ;

//------------------------------------------------------------//
class InitializeEvent : public kvs::InitializeEventListener {
//------------------------------------------------------------//

  void update( void )
  {
    // default light position
    kvs::Vector3f P0 = DEFAULT_LIGHT_POSITION ;

    // Set the light position 
    //... The default setting is (0, 0, 12) 
//    screen()->light()->setPosition( P0.x(), P0.y(), P0.z() ); //KVS1
    static_cast<kvs::glut::Screen*>(screen())->scene()->light()->setPosition( P0.x(), P0.y(), P0.z() );//KVS2 type1
  }

};

//------------------------------------------------------------//
class KeyPressEvent : public kvs::KeyPressEventListener
//------------------------------------------------------------//
{
   void update( kvs::KeyEvent* event )
   {
       switch ( event->key() )
       {
       // push l: light control
       // push o: object controls
       // push s: snapshot (BMP)
       // push S: snapshot (PPM)
       // push G: snapshot (PGM)
         case kvs::Key::l: 
           std::cerr << "\n** Light-control mode" << std::endl;

//         screen()->controlTarget() = kvs::glut::Screen::TargetLight;//KVS1 
//         static_cast<kvs::glut::Screen*>( screen() )->scene()->controlTarget() = kvs::Scene::TargetLight; //KVS2 type1
           static_cast<kvs::glut::Screen*>(screen())->setControlTarget( kvs::Scene::TargetLight );//KVS2 type2 

           this->displayMenu();
           break;
         case kvs::Key::o: 
           std::cerr << "\n** Object-control mode" << std::endl;

//         screen()->controlTarget() = kvs::glut::Screen::TargetObject;//KVS1 
//         static_cast<kvs::glut::Screen*>( screen() )->scene()->controlTarget() = kvs::Scene::TargetObject; //KVS2 type1
           static_cast<kvs::glut::Screen*>(screen())->setControlTarget( kvs::Scene::TargetObject );//KVS2 type2

           this->displayMenu();
           break;
         case kvs::Key::s: {
           std::cerr << "\n** Snapshot (BMP) ==> snapshot.bmp " << std::endl;
           kvs::ColorImage snapshot_image; 

//         snapshot_image = screen()->camera()->snapshot();//KVS1
           snapshot_image = static_cast<kvs::glut::Screen*>( screen() )->scene()->camera()->snapshot();//KVS2

	   snapshot_image.write("snapshot.bmp");
           this->displayMenu();
           break;
	 }
         case kvs::Key::S: {
           std::cerr << "\n** Snapshot (PPM)==> snapshot.ppm " << std::endl;
           kvs::ColorImage snapshot_image; 

//         snapshot_image = screen()->camera()->snapshot();//KVS1
	   snapshot_image = static_cast<kvs::glut::Screen*>( screen() )->scene()->camera()->snapshot();//KVS2

	   snapshot_image.write("snapshot.ppm");
           this->displayMenu();
           break;
	 }
         case kvs::Key::G: {
           std::cerr << "\n** Snapshot (PGM) ==> snapshot.pgm " << std::endl;
           kvs::ColorImage snapshot_image; 

//         snapshot_image = screen()->camera()->snapshot();//KVS1
	   snapshot_image = static_cast<kvs::glut::Screen*>( screen() )->scene()->camera()->snapshot();//KVS2

	   snapshot_image.write("snapshot.pgm");
           this->displayMenu();
           break;
	 }
         default: 
           break;
       } // switch


   }

 public:
   void displayMenu (void ) 
   {
     std::cerr << "\nKeyboard menu:" << std::endl;
     std::cerr << "  o-key: object control, l-key: light control" << std::endl;
     std::cerr << "  s-key: snapshot image (BMP)" << std::endl;
     std::cerr << "  S-key: snapshot image (PPM)" << std::endl;
     std::cerr << "  G-key: snapshot image (PGM)" << std::endl;
     std::cerr << "  q-key: quit" << std::endl;
   }

};


#endif
// end of EventControl.h

