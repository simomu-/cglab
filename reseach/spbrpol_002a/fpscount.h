///////////////////////
///// fpscount.h /////
///////////////////////

#if !defined  FPS_COUNT_HH
#define       FPS_COUNT_HH

#include <kvs/glut/Label>
#include <kvs/glut/Screen>
#include <kvs/RendererManager>

#include <sstream>

class Label : public kvs::glut::Label
{
 public:

 Label( kvs::ScreenBase* screen ):
  kvs::glut::Label( screen )
    {
      setMargin( 10 );
    }

  void screenUpdated( void )
  {
    //    const kvs::RendererBase* renderer = screen()->rendererManager()->renderer(); //KVS1
    const kvs::RendererBase* renderer = static_cast<kvs::glut::Screen*>( screen() )->scene()->rendererManager()->renderer();//KVS2

    std::stringstream fps;
    fps << std::setprecision(4) << renderer->timer().fps();
    setText( std::string( "fps: " + fps.str() ).c_str() );
  }
};



#endif


