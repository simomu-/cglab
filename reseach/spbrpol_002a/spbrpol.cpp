///////////////////////
///// spbrpol.cpp /////
///////////////////////

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#define _USE_MATH_DEFINES
#include <cmath>
#include <kvs/RotationMatrix33>
#include <kvs/MersenneTwister>
#include <kvs/Timer>
#include <set>
#include <deque>
#include <list>
#include <limits>
#include <random>
#include "spbrpol.h"
#include "shuffle.h"
#include "spcomment.h"


//----- DEBUG control
//#define DEBUG

//-----
SPBRPol::SPBRPol(char* datafile):
  m_nvertex(0), 
  m_nface(0), 
  m_S_area(0.0), 
  m_min_P_area(0.0),
  m_alpha(1.0), 
  m_haveNormal(false), 
  m_haveColor(false),
  m_centerCoord(0, 0, 0), 
  m_maxCoord(-100000, -1000000, -1000000), 
  m_minCoord(1000000,1000000,1000000), 
  m_vbbMax (1.0, 1.0, 1.0 ), 
  m_vbbMin (-1.0, -1.0, -1.0), 
  m_zoomFactor( DEFAULT_ZOOM_FACTOR ),
  m_flagShading ( true ), 
  m_repeatLevel (DEFAULT_REPEAT_LEVEL ),
  m_imageResolution (DEFAULT_IMAGE_RESOLUTION ),
  m_bgGrayColor (DEFAULT_BG_GRAY_COLOR), 
  m_flagFPS     ( true ),
  m_flagLOD     ( true ),
  m_flagShuffle ( true ),
  m_flagParticleZoom ( true ),
  m_particleDiameter ( 0.0 ),
  m_plyFileName(datafile),
  m_spbrFileName(NULL) 
{
  std::ifstream reader ; 
  reader.open ( datafile );

  ReadHeader(&reader);
  ReadBody(&reader);
}


//-----
SPBRPol::SPBRPol(void):
  m_nvertex(0), 
  m_nface(0), 
  m_S_area(0.0), 
  m_min_P_area(0.0),
  m_alpha(1.0), 
  m_haveNormal(false), 
  m_haveColor(false),
  m_centerCoord(0, 0, 0), 
  m_maxCoord(-100000, -1000000, -1000000), 
  m_minCoord(1000000,1000000,1000000), 
  m_vbbMax (1.0, 1.0, 1.0 ), 
  m_vbbMin (-1.0, -1.0, -1.0), 
  m_zoomFactor( DEFAULT_ZOOM_FACTOR ),
  m_flagShading ( true ), 
  m_repeatLevel (DEFAULT_REPEAT_LEVEL ),
  m_imageResolution (DEFAULT_IMAGE_RESOLUTION ),
  m_bgGrayColor (DEFAULT_BG_GRAY_COLOR), 
  m_flagFPS     ( true ),
  m_flagLOD     ( true ),
  m_flagShuffle ( true ),
  m_flagParticleZoom ( true ),
  m_particleDiameter ( 0.0 ),
  m_plyFileName(NULL),
  m_spbrFileName(NULL),
  m_flagCameraProjectionType(DEFAULT_PROJECTIONTYPE) 
{}


//-----
bool SPBRPol::ReadPly(char* datafile)
{
  std::ifstream reader ; 
  reader.open ( datafile );
  m_plyFileName = datafile;
  if(ReadHeader(&reader)){
    ReadBody(&reader);
  }
  else{
    std::cout<<"ERROR:Can't Read Header!"<<std::endl;
    exit(1);
  }

  return true;
}


//-----
// Read header part of a PLY file
bool SPBRPol::ReadHeader(std::ifstream* reader)
{
  // Check the first line "ply"
  std::string str;
  std::getline(*reader, str);
  if( str.empty() || str != "ply"){
    std::cout<<"ERROR: Not a PLY file"<<std::endl;
    return false;
  }

  // Read each line in the header region
  while(!reader->eof()) {
    // Read one line from the file 
    //   and set the read string to a string stream.
    std::getline( *reader, str, '\n' ); // read one line
    std::stringstream ss1( str );       

    // Get the first word in the read line
    std::string s1;
    ss1 >> s1;

    // Parse the read line
    if(s1 == "end_header") { 
      break;  // end of the header region
    }
    else{
      // Set the read one line to a new string stream
      std::stringstream ss(str);

      // Get the first word
      std::string s;
      ss >> s;

      // Get properties described in the file
      if(s == "element"){
	ss >> s; // Get the second word of the line

	if(s == "vertex"){
	  ss >> m_nvertex; // element vertex num_vertex
	}
	else if(s == "face"){
	  ss >> m_nface;  // element face num_face
	}

      }
      else if(s == "property"){
	ss >> s; // Get the 2nd word (ignored)
	ss >> s; // Get the 3rd word
	if(s == "nx") { // property float nx
	  m_haveNormal = true; 
	}
	else if( s == "red") { // property uchar red
	  m_haveColor = true; 
	}

      }
    }

  } // while

  //EndFn
  return true;

} // ReadHeader()


//-----
// Read vertices, facets, and normal vectors
bool SPBRPol::ReadBody(std::ifstream* reader)
{
  // Initialize bounding-box info
  m_centerCoord.set(0.0, 0.0, 0.0);
  m_minCoord.set( +1.0e+9, +1.0e+9, +1.0e+9 );
  m_maxCoord.set( -1.0e+9, -1.0e+9, -1.0e+9 );

  // Read vertex info
  for( int i=0; i < m_nvertex; i++ ) {
    // Line buffer
    std::string str;

    // Read one line from the file
    std::getline(*reader, str, '\n');

    // Ignore a blank line
    if(str == "") continue; //空行無視 

    // String stream to parse the read line
    std::stringstream ss(str);

    // Get vertex info of the read line
    Vertex v;
    ss >> v.vertex_x; // 1st word is the x coord
    ss >> v.vertex_y; // 2st word is the y coord
    ss >> v.vertex_z; // 3rd word is the z coord

    // Get normla vector info of the read line, if any
    if( m_haveNormal ){
      ss >> v.normal_x; // 4th word is nx
      ss >> v.normal_y; // 5th word is ny
      ss >> v.normal_z; // 6th word is nz
    }
    else { // Set a zero vector, if no normals are written
      v.normal_x = 0.0 ;
      v.normal_y = 0.0 ;
      v.normal_z = 0.0 ;
    }

    // Get color info of the read line, if any
    if( m_haveColor ) {
      //#ifdef USE_PLY_COLOR
      ss >> v.r; // 7th word is nz
      ss >> v.g; // 8th word is nz
      ss >> v.b; // 9th word is nz
      //#endif//USE_PLY_COLOR 
    }
    else{
      v.r = DEFAULT_POINT_COLOR_R;
      v.g = DEFAULT_POINT_COLOR_G;
      v.b = DEFAULT_POINT_COLOR_B;
    }

    // Initialize the number facets connected to the read vertex
    v.faceNum = 0;

    // Vector sum to calc the weight center
    m_centerCoord.set( m_centerCoord.x() + v.vertex_x,
		       m_centerCoord.y() + v.vertex_y,
		       m_centerCoord.z() + v.vertex_z );

    // Update the bounding box info
    // ... Min position   
    if( m_minCoord.x() > v.vertex_x ) { 
      m_minCoord.set( v.vertex_x, m_minCoord.y(), m_minCoord.z() );
    }
    if( m_minCoord.y() > v.vertex_y ) { 
      m_minCoord.set( m_minCoord.x(), v.vertex_y, m_minCoord.z() );
    }
    if( m_minCoord.z() > v.vertex_z ) {
      m_minCoord.set( m_minCoord.x(), m_minCoord.y(), v.vertex_z );
    }
    // ... Max position   
    if( m_maxCoord.x() < v.vertex_x ) {
      m_maxCoord.set( v.vertex_x, m_maxCoord.y(), m_maxCoord.z() );
    }
    if( m_maxCoord.y() < v.vertex_y ) { 
      m_maxCoord.set( m_maxCoord.x(), v.vertex_y, m_maxCoord.z() );
    }
    if( m_maxCoord.z() < v.vertex_z ) {
      m_maxCoord.set( m_maxCoord.x(), m_maxCoord.y(), v.vertex_z );
    }

    // Add the read vertex to the list
    m_verts.push_back(v);

  }// for(i)... vertex info 

  // Calculate weight center of the read vertices
  m_centerCoord = m_centerCoord / m_nvertex;

  // get min polygon area (by shimomura 2017/11/08)
  m_min_P_area = std::numeric_limits<double>::infinity();

	// Read facet info
  for( int i=0; i < m_nface; i++ ) {
		std::string str;
		std::getline(*reader, str, '\n');
		if(str == "") continue; //空行無視 
		std::stringstream ss(str);
		int vertNum;
		ss >> vertNum;
		face *f = new face[vertNum-2];

		ss >> f[0].index1;
		ss >> f[0].index2;
		ss >> f[0].index3;
		//重心を求める
		f[0].center_x = (m_verts[f[0].index1].vertex_x + m_verts[f[0].index2].vertex_x + m_verts[f[0].index3].vertex_x) /3;
		f[0].center_y = (m_verts[f[0].index1].vertex_y + m_verts[f[0].index2].vertex_y + m_verts[f[0].index3].vertex_y) /3;
		f[0].center_z = (m_verts[f[0].index1].vertex_z + m_verts[f[0].index2].vertex_z + m_verts[f[0].index3].vertex_z) /3;
		//面積求める
		kvs::Vector3d vec_0_1(m_verts[f[0].index2].vertex_x - m_verts[f[0].index1].vertex_x,
			m_verts[f[0].index2].vertex_y - m_verts[f[0].index1].vertex_y, 
			m_verts[f[0].index2].vertex_z - m_verts[f[0].index1].vertex_z);
		kvs::Vector3d vec_0_2(m_verts[f[0].index3].vertex_x - m_verts[f[0].index1].vertex_x,
			m_verts[f[0].index3].vertex_y - m_verts[f[0].index1].vertex_y,
			m_verts[f[0].index3].vertex_z - m_verts[f[0].index1].vertex_z);
		kvs::Vector3d vec_0_3(m_verts[f[0].index3].vertex_x - m_verts[f[0].index2].vertex_x,
			m_verts[f[0].index3].vertex_y - m_verts[f[0].index2].vertex_y,
			m_verts[f[0].index3].vertex_z - m_verts[f[0].index2].vertex_z);
		double edge0_1 = vec_0_1.length();
		double edge0_2 = vec_0_2.length();
		double edge0_3 = vec_0_3.length();
		double s_0 = (edge0_1 + edge0_2 + edge0_3)/2;
		f[0].area = sqrt(fabs(s_0*(s_0-edge0_1)*(s_0-edge0_2)*(s_0-edge0_3)));
		m_S_area += f[0].area;

    //get min polygon area (by shimomura 2017/11/08)
    if(f[0].area <= m_min_P_area && f[0].area != 0.0){
      m_min_P_area = f[0].area;
    }


		kvs::Vector3d nol_0 = vec_0_1.cross(vec_0_2);
		//面法線の求める
		nol_0.normalize();
		f[0].normal_x = nol_0.x();
		f[0].normal_y = nol_0.y();
		f[0].normal_z = nol_0.z();
		//頂点法線
		if(!m_haveNormal){
			m_verts[f[0].index1].normal_x += nol_0.x();
			m_verts[f[0].index1].normal_y += nol_0.y();
			m_verts[f[0].index1].normal_z += nol_0.z();
			m_verts[f[0].index1].faceNum++;

			m_verts[f[0].index2].normal_x += nol_0.x();
			m_verts[f[0].index2].normal_y += nol_0.y();
			m_verts[f[0].index2].normal_z += nol_0.z();
			m_verts[f[0].index2].faceNum++;

			m_verts[f[0].index3].normal_x += nol_0.x();
			m_verts[f[0].index3].normal_y += nol_0.y();
			m_verts[f[0].index3].normal_z += nol_0.z();
			m_verts[f[0].index3].faceNum++;
		}

		m_faces.push_back(f[0]);
		//重心の計算が大変なので三角形に分割？
		for(int j=1; j<vertNum-2; j++){
			f[j].index1 = f[0].index1;
			f[j].index2 = f[j-1].index3;
			ss >> f[j].index3;

			f[j].center_x = (m_verts[f[j].index1].vertex_x + m_verts[f[j].index2].vertex_x + m_verts[f[j].index3].vertex_x) /3;
			f[j].center_y = (m_verts[f[j].index1].vertex_y + m_verts[f[j].index2].vertex_y + m_verts[f[j].index3].vertex_y) /3;
			f[j].center_z = (m_verts[f[j].index1].vertex_z + m_verts[f[j].index2].vertex_z + m_verts[f[j].index3].vertex_z) /3;
			//面積を求める
			kvs::Vector3d vec1(m_verts[f[j].index2].vertex_x - m_verts[f[j].index1].vertex_x,
				m_verts[f[j].index2].vertex_y - m_verts[f[j].index1].vertex_y, 
				m_verts[f[j].index2].vertex_z - m_verts[f[j].index1].vertex_z);
			kvs::Vector3d vec2(m_verts[f[j].index3].vertex_x - m_verts[f[j].index1].vertex_x,
				m_verts[f[j].index3].vertex_y - m_verts[f[j].index1].vertex_y,
				m_verts[f[j].index3].vertex_z - m_verts[f[j].index1].vertex_z);
			kvs::Vector3d vec3(m_verts[f[j].index3].vertex_x - m_verts[f[j].index2].vertex_x,
				m_verts[f[j].index3].vertex_y - m_verts[f[j].index2].vertex_y,
				m_verts[f[j].index3].vertex_z - m_verts[f[j].index2].vertex_z);
			double edge1 = vec1.length();
			double edge2 = vec2.length();
			double edge3 = vec3.length();
			double s = (edge1 + edge2 + edge3)/2;
			f[j].area = sqrt(fabs(s*(s-edge1)*(s-edge2)*(s-edge3)));
			kvs::Vector3d nol = vec1.cross(vec2);
			m_S_area += f[j].area;

      //get min polygon area (by shimomura 2017/11/08)
      if(f[j].area <= m_min_P_area && f[j].area != 0.0){
        m_min_P_area = f[j].area;       
      }

			//面法線を求める
			nol.normalize();
			f[j].normal_x = nol.x();
			f[j].normal_y = nol.y();
			f[j].normal_z = nol.z();

			//頂点法線：面法線の平均をとるため加算
			if(!m_haveNormal){
				m_verts[f[j].index1].normal_x += nol.x();
				m_verts[f[j].index1].normal_y += nol.y();
				m_verts[f[j].index1].normal_z += nol.z();
				m_verts[f[j].index1].faceNum++;

				m_verts[f[j].index2].normal_x += nol.x();
				m_verts[f[j].index2].normal_y += nol.y();
				m_verts[f[j].index2].normal_z += nol.z();
				m_verts[f[j].index2].faceNum++;

				m_verts[f[j].index3].normal_x += nol.x();
				m_verts[f[j].index3].normal_y += nol.y();
				m_verts[f[j].index3].normal_z += nol.z();
				m_verts[f[j].index3].faceNum++;
			}
			delete [] f;

		}
  }

  //頂点法線：その頂点が含まれている面法線の平均
  if(!m_haveNormal){
    for( int i=0; i < m_nvertex; i++ ) {
			m_verts[i].normal_x = m_verts[i].normal_x/ m_verts[i].faceNum;
			m_verts[i].normal_y = m_verts[i].normal_y/ m_verts[i].faceNum;
			m_verts[i].normal_z = m_verts[i].normal_z/ m_verts[i].faceNum;
			kvs::Vector3d n_temp(m_verts[i].normal_x, m_verts[i].normal_y, m_verts[i].normal_z);
			n_temp.normalize();
			m_verts[i].normal_x = n_temp.x();
			m_verts[i].normal_y = n_temp.y();
			m_verts[i].normal_z = n_temp.z();
    }// for
  }// if

  //EndFn
  return true;

} // ReadBody()




//-----
void SPBRPol:: WriteSPBRFile ( int  repeat_level , 
                              int  resolution  , 
                              int  num_points  ,  
                              kvs::ValueArray<kvs::Real32>& pointCoords , 
                              kvs::ValueArray<kvs::Real32>& pointNormals ,
                              kvs::ValueArray<kvs::UInt8>&  pointColors )
{
  // Open output stream.
  std::ofstream fout; fout.open ( m_spbrFileName );

  if( !fout.fail() ) {
    std::cerr << "** Generating a SPBR file \"" 
              << m_spbrFileName << "\"..." << std::endl;

    // Commands
    //... Begin header part
    fout << SPBR_ASCII_DATA_COMMAND << std::endl;

    //... comments
    fout << "## input PLY file: " << m_plyFileName << std::endl;
    fout << "## num particles : " << num_points    << std::endl;
    fout << "## Alpha         : " << Alpha()       << std::endl;
    fout << "##"                                   << std::endl;

    //... num points
    fout << NUM_PARTICLES_COMMAND << " " << num_points << std::endl;

    //... repet level
    //    fout << "#/RepeatLevel" << " " << repeat_level << std::endl;
    fout << REPEAT_LEVEL_COMMAND << " " << repeat_level << std::endl;

    //... image resolution
    //    fout << "#/ImageResolution" << " " << resolution << std::endl;
    fout << IMAGE_RESOLUTION_COMMAND << " " << resolution << std::endl;

    //... bounding box
    //    fout << "#/BoundingBox" << " " << MinCoord() << "  " 
    fout << BOUNDING_BOX_COMMAND << " " << MinCoord() << "  " 
         << " " << MaxCoord() << std::endl;

    //... backGround Color
    fout << BG_COLOR_BYTE_COMMAND << " " 
         << BGGrayColor()      << " " 
         << BGGrayColor()      << " " 
         << BGGrayColor()      << std::endl;

    // camera position 
    fout << CAMERA_ZOOM_COMMAND << " " << ZoomFactor() << std::endl;

    // particle zoom (on)
    // particle zoom (on)
    fout << PARTICLE_ZOOM_COMMAND << " " << (int)(IsParticleZoom()) << std::endl;

    //... FPS
    fout << FPS_COMMAND << " " << (int)(IsFPS()) << std::endl;

    //... LOD
    fout << LOD_COMMAND << " " << (int)(IsLOD()) << std::endl;

    //... Shading
    fout << SHADING_COMMAND << " " << (int)(IsShading()) << std::endl;

    // shuffle
    fout << SHUFFLE_COMMAND << " " << IsShuffle() << std::endl;

    // End of preemble
    fout << END_HEADER_COMMAND << std::endl;

    //... particle-data format 
    //     x y z  nx ny nz  R_byte G_byte B_byte
    //  fout << "#/UseParticleByParticleColorByteRGB  1" << std::endl;

    // Data 
    for ( int i = 0 ; i < num_points; i++ ) {
      // coord
      fout << pointCoords [ 3*i     ] << " " ;
      fout << pointCoords [ 3*i + 1 ] << " " ;
      fout << pointCoords [ 3*i + 2 ] << "  " ;

      // normal
      fout << pointNormals[ 3*i     ] << " " ;
      fout << pointNormals[ 3*i + 1 ] << " " ;
      fout << pointNormals[ 3*i + 2 ] << "  " ;

      // Color 
      fout << (int)pointColors [ 3*i     ] << " ";
      fout << (int)pointColors [ 3*i + 1 ] << " ";
      fout << (int)pointColors [ 3*i + 2 ] << std::endl;

    } // for(i)

  } // if( !fout.fail() ) 
  else {

    std::cout<<"File Open Error!!!!"<<std::endl;
  }

  // Close the output stream
  fout.close();

} // WriteSPBRFile ()



//////////////////////////////
/////  Polygon sampling  /////
//////////////////////////////
//---------------------------------------------
// For each polygon in the given polygon mesh, 
// generate uniformly distributed  points 
// with distance "d = sqrt(S/n_sum), where
//   S     = total polygon area,
//   n_sum = total number of particles.
// The n_sum is defined by using 
// the user-defined opacity "alpha" and 
// repeat level "L_R":
//  n_sum = log(1-alpha)/log(1-(s/S))*L_R, 
// where 
//  s = particle_width * particle_width
//    = pixel_3D_width * pixel_3D_width  
// is the particle cross section. 
//---------------------------------------------
////////////////////////////////
//          p2
//            *
//           * *
//   vec21  *   *   vec32 
//  (1->2) *     *  (2->3)
//        *       * 
//       * * * * * * 
//     p1   vec31   p3
//          (1->3)
////////////////////////////////
//---------------------------------------------------------
// Loop structure:
//
//  for ( each polygon i) {
//    //***********************************//
//    // PART 1: Create particles on edges //
//    //***********************************//
//    for (each particle j on edge 21 ) { SAMPLING }
//    for (each particle j on edge 31 ) { SAMPLING }
//    for (each particle j on edge 32 ) { SAMPLING }
//
//    //**********************************************//
//    // PART 2: Create particles inside each polygon //
//    //**********************************************//
//    for (each scanline k ) {
//      for (each particle j ) { SAMPLING }
//  }
//---------------------------------------------------------
//-----
void SPBRPol::ConvertToParticles( double alpha      , 
                              double pixel_width , 
                              int    repeat_level, 
                              int    resolution , 
                              kvs::PointObject* particles )
{	
  // Constants 
  double epsilon     = 1.0e-3 ; // to avoid overlapping sampling points
  double epsilon_cos = 1.0e-4 ; // to treat triangles with zero area

  // Values used in the formula of SPBR
  //..... Particle cross section "s"
  double s_particleCrossSection = pixel_width * pixel_width;

  //..... n_sum: total number of particles (= n * L_R) 
  double n_sum = log(1-alpha)/log(1-(s_particleCrossSection/m_S_area))*repeat_level;

  //.....  inter-particle distance "d" and "2d"
  double d_particleDistance = sqrt(m_S_area/n_sum)    ; // d
  double d2              = 2.0 * d_particleDistance ; // 2*d 

  // Message
  std::cout << "ply vertex num:"  << m_nvertex   << std::endl;
  std::cout << "ply face num:"    << m_nface     << std::endl;
  std::cout << "pixel width:"     << pixel_width << std::endl;
  std::cout << "S(area):"         << m_S_area    << std::endl;
  std::cout << "min P(area):"      << m_min_P_area << std::endl;
  std::cout << "point_dist:"      << d_particleDistance << std::endl;
  std::cout << "n_sum (theoretical):"   << n_sum << std::endl;
  std::cout << "adaptively point reduction:"  << ENABLE_ADAPTIVELY_POINT_REDUCTION << std::endl;

  // Message 
  kvs::Timer timer1;
  timer1.start();

  // list of vertices in the given polygon mesh
  std::deque<Vertex> point_list ;
  
  // delete (by shimomura 2017/11/23)
  // copy: vector<Vertex> m_verts ==> deque<Vertex> point_list
  // for( std::vector<Vertex>::iterator i = m_verts.begin(); 
  //      i != m_verts.end()                               ; 
  //      i++                                                )
  // {
  //   point_list.push_back((*i));
  // } 

  kvs::MersenneTwister mersenne;
  kvs::MersenneTwister posRand;
  

  ////////////////////////////////////
  /////  Polygon-sampling loop  /////
  ///////////////////////////////////
  //---------------------------------------------
  // For each polygon in the given polygon mesh, 
  //  generate uniformly-distributed particles 
  //  with distance "d_particleDistance".
  //---------------------------------------------------------
  //... i is the polygon index
  //
  for( std::vector<face>::iterator i = m_faces.begin(); 
       i != m_faces.end(); 
       i++) 
  {
    // position, normal vector, and color of Vertex 1 of the i-th polygon
    kvs::Vector3d p1Vertex( m_verts[(*i).index1].vertex_x, m_verts[(*i).index1].vertex_y, m_verts[(*i).index1].vertex_z );

    kvs::Vector3d p1Normal( m_verts[(*i).index1].normal_x, m_verts[(*i).index1].normal_y, m_verts[(*i).index1].normal_z );

    kvs::Vector3d p1Color ( m_verts[(*i).index1].r, m_verts[(*i).index1].g, m_verts[(*i).index1].b );

    // position, normal vector, and color of Vertex 2 of the i-th polygon
    kvs::Vector3d p2Vertex( m_verts[(*i).index2].vertex_x, m_verts[(*i).index2].vertex_y, m_verts[(*i).index2].vertex_z );

    kvs::Vector3d p2Normal( m_verts[(*i).index2].normal_x, m_verts[(*i).index2].normal_y, m_verts[(*i).index2].normal_z );

    kvs::Vector3d p2Color ( m_verts[(*i).index2].r, m_verts[(*i).index2].g, m_verts[(*i).index2].b );

    // position, normal vector, and color of Vertex 3 of the i-th polygon
    kvs::Vector3d p3Vertex( m_verts[(*i).index3].vertex_x, m_verts[(*i).index3].vertex_y, m_verts[(*i).index3].vertex_z );

    kvs::Vector3d p3Normal( m_verts[(*i).index3].normal_x, m_verts[(*i).index3].normal_y, m_verts[(*i).index3].normal_z );

    kvs::Vector3d p3Color ( m_verts[(*i).index3].r, m_verts[(*i).index3].g, m_verts[(*i).index3].b );


    // add (by shimomura 2017/11/23)
    // このポリゴンにおけるポリゴンと点の面積比算出
    const kvs::Vector3d vec21( p2Vertex - p1Vertex ); // vector 1-->2
    const kvs::Vector3d vec31( p3Vertex - p1Vertex ); // vector 1-->3
    const double polygon_area = std::sqrt(vec21.length2() * vec31.length2() - std::pow(vec21.dot(vec31), 2.0)) / 2.0;
    const double pol_ptcl_ratio = polygon_area / s_particleCrossSection;

    double point_num = ( n_sum * (polygon_area / m_S_area));
    // add (by shimomura)
    // ポリゴンと点断面積の比が1未満の時の点配置
    if(pol_ptcl_ratio <= 1.0 && ENABLE_ADAPTIVELY_POINT_REDUCTION){

      Vertex v;
      kvs::Vector3d p = (p1Vertex + p2Vertex + p3Vertex) / 3.0;
      kvs::Vector3d n = (p1Normal + p2Normal + p3Normal) / 3.0;
      kvs::Vector3d c = (p1Color + p2Color + p3Color) / 3.0;
      v.vertex_x = p.x();
      v.vertex_y = p.y();
      v.vertex_z = p.z();
      v.normal_x = n.x();
      v.normal_y = n.y();
      v.normal_z = n.z();
      v.r = c.x();
      v.g = c.y();
      v.b = c.z();

      size_t num = (point_num < 1) ? 1 : std::round( point_num);

      double delta = (p - p1Vertex).length();
      if(delta <= (p - p2Vertex).length()){
        delta = (p - p2Vertex).length();
      }
      if(delta <= (p - p3Vertex).length()){
        delta = (p - p3Vertex).length();
      }

      for(size_t j = 0; j < num; j++){
        if(point_num < 1){
          // ポリゴン内の理論点数が1未満の時に乱数て一つだけ点配置
          double r = mersenne();

          if(r <= point_num){
            point_list.push_back(v);
          }

        }else{
          // 点をポリゴンの重心からランダムに微笑距離ずらす
          Vertex p(v);
          double x = (posRand() - 0.5) * 2;
          double y = (posRand() - 0.5) * 2;
          double z = -1 * (p.normal_x * x + p.normal_y * y) / p.normal_z;
          kvs::Vector3d gap(x, y, z);
          gap.normalize();
          double size = ((posRand() - 0.5)*2) * delta;
          double dir = (size >= 0) ? d_particleDistance : -d_particleDistance;
          gap *= (size + dir);
          p.vertex_x += gap.x();
          p.vertex_y += gap.y();
          p.vertex_z += gap.z();

          point_list.push_back(p);
        }
      }
      //これ以降の点生成をキャンセルし、次のポリゴンへ
      continue;
    }



    // 頂点に重複無しで点配置
    if(!m_verts[(*i).index1].deployed && AdaptPoint(pol_ptcl_ratio)){
      m_verts[(*i).index1].deployed = true;
      point_list.push_back(m_verts[(*i).index1]);
    }
    if(!m_verts[(*i).index2].deployed && AdaptPoint(pol_ptcl_ratio)){
      m_verts[(*i).index2].deployed = true;
      point_list.push_back(m_verts[(*i).index2]);
    }    
    if(!m_verts[(*i).index3].deployed && AdaptPoint(pol_ptcl_ratio)){
      m_verts[(*i).index3].deployed = true;
      point_list.push_back(m_verts[(*i).index3]);
    }

    //***********************************//
    // PART 1: Create particles on edges //
    //***********************************//

    //---------------------------------------//
    // Part 1-1  Create particles on edge 21 //
    //---------------------------------------//

    // edge vector of edge 21 and its length
    const double        vec21_length = vec21.length();

    // normalized edge vector for edge 21
    kvs::Vector3d vec21_hat = vec21;  vec21_hat.normalize(); 

    // number of generated particles on edge 21
    int edge21PointNum = (int)( vec21_length / d2 ); 

    // Avoid a particle just on vertex 2. 
    if( fabs( vec21_length - d2*edge21PointNum ) < epsilon*d2 ) {//TANAKA
      edge21PointNum--; 
    }

    // Create particles on edge 21 with distance 2*d 
    //  p1+1*(2d)*vec21_hat, p1+ 2*(2d)*vec21_hat, p1+3*(2d)*vec21_hat, ... 
    //.... j is the particle index on edge 21 
    for( int j = 1; j <= edge21PointNum; j++ ) {//TANAKA 

      //shimomura
      if(!AdaptPoint(pol_ptcl_ratio)){
        continue;
      }

      // the j-th particle position
      Vertex v;
      v.vertex_x = (vec21_hat.x()*d2*j) + m_verts[(*i).index1].vertex_x;
      v.vertex_y = (vec21_hat.y()*d2*j) + m_verts[(*i).index1].vertex_y;
      v.vertex_z = (vec21_hat.z()*d2*j) + m_verts[(*i).index1].vertex_z;

      //----- Phong shading -----//
      // normal-vector interpolation on edge 21 (1 ---> s ---> 2)
      kvs::Vector3d psVertex(v.vertex_x, v.vertex_y, v.vertex_z);//current pos
      kvs::Vector3d vs2( psVertex - p2Vertex );
      kvs::Vector3d v1s( p1Vertex - psVertex );

      kvs::Vector3d normal_s = 
        (1/vec21_length)*( vs2.length()*p1Normal + v1s.length()*p2Normal );

      v.normal_x = normal_s.x();
      v.normal_y = normal_s.y();
      v.normal_z = normal_s.z();

      // color for Phong shading. 
      //.... Use the color read from input PLY file.
      kvs::Vector3d color_s = 
        (1/vec21_length)*( vs2.length()*p1Color + v1s.length()*p2Color );

      v.r = color_s.x();
      v.g = color_s.y();
      v.b = color_s.z();
      //----- end of Phong shading -----//

      // Store the generated particle to the list
      point_list.push_back(v);

    }// for (j): particle generation on edge 21 



    //---------------------------------------//
    // Part 1-2  Create particles on edge 31 //
    //---------------------------------------//

    // edge vectors of edge 31 and its length
    const double        vec31_length = vec31.length();

    // normalized edge vector for edge 31
    kvs::Vector3d vec31_hat = vec31;  vec31_hat.normalize(); 

    // number of generated particles on edge 31
    int edge31PointNum = (int)( vec31_length / d2 );

    // Avoid a particle just on vertex 3. 
    if( fabs( vec31_length - d2*edge31PointNum ) < epsilon*d2 ) {//TANAKA
      edge31PointNum--;
    }

    // Create particles on edge 31 with distance 2*d 
    //  p1+1*(2d)*vec31_hat, p1+ 2*(2d)*vec31_hat, p1+3*(2d)*vec31_hat, ... 
    //.... j is the particle index on edge 31 
    for( int j = 1; j <= edge31PointNum; j++ ) {//TANAKA 
      // the j-th particle position

      if(!AdaptPoint(pol_ptcl_ratio)){
        continue;
      }

      Vertex v;
      v.vertex_x = (vec31_hat.x()*d2*j) + m_verts[(*i).index1].vertex_x;
      v.vertex_y = (vec31_hat.y()*d2*j) + m_verts[(*i).index1].vertex_y;
      v.vertex_z = (vec31_hat.z()*d2*j) + m_verts[(*i).index1].vertex_z;

      //----- Phong shading -----//
      // normal-vector interpolation on edge 31 (1 ---> s ---> 3)
      kvs::Vector3d psVertex(v.vertex_x, v.vertex_y, v.vertex_z);//current pos
      kvs::Vector3d vs3( psVertex - p3Vertex );
      kvs::Vector3d v1s( p1Vertex - psVertex );

      kvs::Vector3d normal_s = 
        (1/vec31_length)*( vs3.length()*p1Normal + v1s.length()*p3Normal );

      v.normal_x = normal_s.x();
      v.normal_y = normal_s.y();
      v.normal_z = normal_s.z();

      // color for Phong shading. 
      //.... Use the color read from input PLY file.
      kvs::Vector3d color_s = 
        (1/vec31_length)*( vs3.length()*p1Color + v1s.length()*p3Color );

      v.r = color_s.x();
      v.g = color_s.y();
      v.b = color_s.z();
      //----- end of Phong shading -----//

      // Store the generated particle to the list
      point_list.push_back(v);

    } // for (j): particle generation on edge 31 



    //---------------------------------------//
    // Part 1-3  Create particles on edge 32 //
    //---------------------------------------//

    // edge vectors of edge 32 and its length
    const kvs::Vector3d vec32( p3Vertex - p2Vertex ); // vector 2-->3
    const double        vec32_length = vec32.length();

    // normalized edge vector for edge 32
    kvs::Vector3d vec32_hat = vec32;  vec32_hat.normalize(); 

    // number of generated particles on edge 32
    int edge32PointNum = (int)( vec32_length / d2 );

    // Avoid a particle just on vertex 3. 
    if( fabs( vec32_length - d2*edge32PointNum ) < epsilon*d2 ) {//TANAKA
      edge32PointNum--;
    }

    // Create particles on edge 32 with distance 2*d 
    //  p2+1*(2d)*vec32_hat, p2+ 2*(2d)*vec32_hat, p2+3*(2d)*vec32_hat, ... 
    //.... j is the particle index on edge 32 
    for( int j = 1; j <= edge32PointNum; j++ ) {//TANAKA 
      // the j-th particle position

      if(!AdaptPoint(pol_ptcl_ratio)){
        continue;
      }

      Vertex v;
      v.vertex_x = (vec32_hat.x()*d2*j) + m_verts[(*i).index2].vertex_x;
      v.vertex_y = (vec32_hat.y()*d2*j) + m_verts[(*i).index2].vertex_y;
      v.vertex_z = (vec32_hat.z()*d2*j) + m_verts[(*i).index2].vertex_z;

      //----- Phong shading -----//
      // normal-vector interpolation on edge 32 (2 ---> s ---> 3)
      kvs::Vector3d psVertex(v.vertex_x, v.vertex_y, v.vertex_z);//current pos
      kvs::Vector3d vs3( psVertex - p3Vertex );
      kvs::Vector3d v2s( p2Vertex - psVertex );

      kvs::Vector3d normal_s = 
        (1/vec32_length)*( vs3.length()*p2Normal + v2s.length()*p3Normal );

      v.normal_x = normal_s.x();
      v.normal_y = normal_s.y();
      v.normal_z = normal_s.z();

      // color for Phong shading. 
      //.... Use the color read from input PLY file.
      kvs::Vector3d color_s = 
        (1/vec32_length)*( vs3.length()*p2Color + v2s.length()*p3Color );

      v.r = color_s.x();
      v.g = color_s.y();
      v.b = color_s.z();
      //----- end of Phong shading -----//

      // Store the generated particle to the list
      point_list.push_back(v);

    }// for (j): particle generation on edge 32 



    //**********************************************//
    // PART 2: Create particles inside each polygon //
    //**********************************************//

    // particle distance along edge 31
    double d_31 = 0.0; // initialization 

    // number of scanlines
    int num_scanlines = 0 ; // initialized to "skip sampling"

    // Starting position of the first scanline (k=1).
    kvs::Vector3d p_cross_31 ;

    // polygon normal
    kvs::Vector3d polygon_normal((*i).normal_x, (*i).normal_y, (*i).normal_z);
    polygon_normal.normalize();

    // moving direction of scan line (perpendicular to edge 21)
    kvs::Vector3d vec_m = vec21.cross( polygon_normal );  vec_m.normalize();

    // Check direction of the scanline movement:
    //  The movement should be in the direction to inside of 
    //  the polygon.
    if( vec31_hat.dot( vec_m ) < 0 ) { vec_m *= (-1.0); }

    // Calc the number of scanlines
    const double cos_factor = fabs( vec31_hat.dot( vec_m  ) );

    if( cos_factor > epsilon_cos ) { 
      // calc particle distance along edge 31
      d_31 = d_particleDistance / cos_factor ;

      // Calc the number of scanlines
      num_scanlines = vec31_length / d_31 ;

      // Avoid a scanline passing through vertex 3.
      if( fabs( vec31_length - d_31 * num_scanlines ) < epsilon*d2 ) {//TANAKA
        num_scanlines--;
      }

      // Starting position of the first scanline (k=1).
      p_cross_31 = p1Vertex + d_31 * vec31_hat ;

    } 

    // Generate sampling points on each scanline.
    //.... k is the scanline index.  
    for( int k = 1; k<= num_scanlines; k++ ) { // TANAKA

      // beginning point of the current scanline
      kvs::Vector3d p_scanline_begin = (p_cross_31 - p1Vertex)*k + p1Vertex;

      // scanline length //TANAKA
      //.... Use the formula of the homologous triangle  
      double scanline_length = ( 1.0 - (d_31/vec31_length)*k ) * vec21_length;

      // ending point of the current scanline // TANAKA
      kvs::Vector3d p_scanline_end = p_scanline_begin + vec21_hat * scanline_length;

      // Calc the number of sampling points on the current scanline.
      int particleNumOnScanline = (int)(scanline_length / d_particleDistance);

      // Avoid a sampling point just on edge 32
    if( fabs( scanline_length - d_particleDistance * particleNumOnScanline ) < epsilon*d_particleDistance ) 
      {//TANAKA
        particleNumOnScanline--;
      }

      // Generate sampling points on the k-the scanline
      // .... j is a particle index on the current scanline.
      for( int j=1; j<= particleNumOnScanline ; j++ ) {//TANAKA 

        if(!AdaptPoint(pol_ptcl_ratio)){
          continue;
        }

	// current sampling position 
        kvs::Vector3d p = p_scanline_begin + vec21_hat*d_particleDistance*j;

        // Set the current sampling position to a Vertex object
        Vertex v;
        v.vertex_x = p.x();
        v.vertex_y = p.y();
        v.vertex_z = p.z();


        //----- Phong shading -----//TANAKA

        // interpolation of normal vectors on edge 31
        double vec_b3_length = ( p_scanline_begin - p3Vertex ).length();
        double vec_1b_length = ( p1Vertex - p_scanline_begin ).length();

        kvs::Vector3d normal_begin = 
          (1/vec31_length)*(vec_b3_length*p1Normal + vec_1b_length*p3Normal);

        // interpolation of normal vectors on edge 32
        double vec_e3_length = ( p_scanline_end - p3Vertex ).length();
        double vec_2e_length = ( p2Vertex - p_scanline_end ).length();

        kvs::Vector3d normal_end = 
          (1/vec32_length)*(vec_e3_length*p2Normal + vec_2e_length*p3Normal);

        // interpolation of normal vectors on the current scanline
        double vec_pb_length = ( p - p_scanline_begin ).length();
        double vec_ep_length = ( p_scanline_end - p   ).length();

        kvs::Vector3d normal_p = 
          (1/scanline_length)*(vec_ep_length*normal_begin + vec_pb_length*normal_end);

        // set the interpolated normal vector to the vertex object
        v.normal_x = normal_p.x();
        v.normal_y = normal_p.y();
        v.normal_z = normal_p.z(); // TANAKA (BUG in Shimokubo's code)

	// interpolation of color 
        kvs::Vector3d color_begin = 
          (1/vec31_length)*(vec_b3_length*p1Color + vec_1b_length*p3Color);
        kvs::Vector3d color_end = 
          (1/vec32_length)*(vec_e3_length*p2Color + vec_2e_length*p3Color);
        kvs::Vector3d color_p = 
          (1/scanline_length)*(vec_ep_length*color_begin + vec_pb_length*color_end);

        // set the interpolated color to the vertex object
        v.r = color_p.x();
        v.g = color_p.y();
        v.b = color_p.z(); 

        //----- end of Phong shading -----//

        point_list.push_back(v);

      } // for(j) (for each particle)

    } // for (k) (for each scanline)

  } // for (i) (for each polygon)

  //////////////////////////////////////////
  /////  End of polygon-sampling loop  /////
  //////////////////////////////////////////



  ////////////////////////
  /////  Final step  /////
  ////////////////////////
  //-------------------------------------------------------
  // Below, we create ValuedArray objects 
  //  for the created particles.
  //  Then we set them back to the argument, "pointObject.
  // We also clean up the temporary "point_list" object.
  // Optionally we write the result to a SPBR-format file, 
  // calling the "WriteSPBRFile ()" method
  //-------------------------------------------------------

  // Message 
  timer1.stop();
  std::cout << "Num of created particles: "<< point_list.size() <<std::endl;
  std::cout << "Particle-creation time (sec): " << timer1.sec() <<std::endl;

  // Message 
  std::cout << "** Start particle shuffling" << std::endl;
  kvs::Timer timer2;
  timer2.start();

  // Particle shuffling 
  RandomGenerator rnd;
  std::random_shuffle( point_list.begin(), point_list.end(), rnd );

  // Message 
  timer2.stop();
  std::cout << "** Shuffling completed" << std::endl;
  std::cout << "Shuffling time (sec): " << timer2.sec()<< std::endl;

  // Message
  std::cout << "** Start creating ValuedArray objects" << std::endl;
  kvs::Timer timer3;
  timer3.start();

  // memory allocation for the ValuedArray objects.
  kvs::ValueArray<kvs::Real32> pointCoords ;
  kvs::ValueArray<kvs::Real32> pointNormals;
  kvs::ValueArray<kvs::UInt8>  pointColors ;
  pointCoords.allocate  ( point_list.size()*3 );
  pointNormals.allocate ( point_list.size()*3 );
  pointColors.allocate  ( point_list.size()*3 );

  // Message 
  std::cout<<"Number of created particles: "<< point_list.size() <<std::endl;

  // Set the created particles to the ValuedArray objects
  int j=0;
  for( std::deque<Vertex>::iterator i = point_list.begin(); 
       i != point_list.end(); 
       i++                                                 ) 
  {
    // x-coord 
    pointCoords[j] = (*i).vertex_x;
    pointNormals[j] = (*i).normal_x;
    pointColors[j] = (*i).r;

    j++;

    // y-coord 
    pointCoords[j] = (*i).vertex_y;
    pointNormals[j] = (*i).normal_y;
    pointColors[j] = (*i).g;

    j++;

    // z-coord 
    pointCoords[j] = (*i).vertex_z;
    pointNormals[j] = (*i).normal_z;
    pointColors[j] = (*i).b;

    j++;

  } // for (i)


  // Message 
  std::cout << "** ValuedArray objects created" << std::endl;
  timer3.stop();
  std::cout<<"Time to create the ValuedArray objects: "<< timer3.sec() << std::endl;

  // Write the particle data into a SPBR-format file
  if( OutputSPBRFileName() != NULL ) {
    WriteSPBRFile( repeat_level, resolution, point_list.size() , 
                  pointCoords, pointNormals, pointColors);
  } 

  // Set the created particles 
  //  back to the argument, "particles", 
  //  which is a pointObject type. 
  particles->setCoords(pointCoords);
  particles->setNormals(pointNormals);
  particles->setColors(pointColors);
  particles->updateMinMaxCoords();

  // Clear temporary particle list
  point_list.clear();

}// ConvertToParticles() 

// AdaptPoint
// 引数の面積比の時に点を採用するかどうかを返す関数
// 点採用の基準は補正曲線と乱数(論文参照)
// pol_ptcl_ratio ポリゴン面積と点断面積の比
bool SPBRPol::AdaptPoint(double pol_ptcl_ratio){

  static kvs::MersenneTwister rand;

  if(!ENABLE_ADAPTIVELY_POINT_REDUCTION){
    return true;
  }

  if(pol_ptcl_ratio > MIN_POLYGON_AREA_THRESHOLD){
    return true;
  }

  // 1.05668x^-0.0163891 (ratio 1.0 ~ 15.0)
  if(1.0 < pol_ptcl_ratio && pol_ptcl_ratio <= MIN_POLYGON_AREA_THRESHOLD){
    double ratio = 1.0 / (1.05668*pow(pol_ptcl_ratio, -0.0163891));
    double t = rand();
    return (t <= ratio);
  }else if (pol_ptcl_ratio <= 1.0){
    return true;
  }

  return false;

}// AdaptPoint

// end of spbrpol.cpp
