<< How to comile, test and  install PBRPOL >>

1. Edit "INSTALL_DIR" in Makefile.
  * The default installation directory is 
    ~/local/bin.

2. Compile and test the program 
  $ make clean
  $ make 
  $./spbrpol ./PLY_DATA/pig.ply

3. Intall the program to INSTALL_DIR.
  $ make install

Note: 
 "make autoinstall" is equivalent to  "make; make install"

<<USAGE>>
 (1) spbrpol  input.ply
 (2) spbrpol  input.ply  alpha
 (3) spbrpol  input.ply  alpha  L_R
 (4) spbrpol  input.ply  alpha  L_R  output.spbr

<<Customization with the environmental variables>>
 setenv SPBRPOL_BG_GRAY_COLOR num [default:0]  
 setenv SPBRPOL_FPS 1/0 [default:0]  
 setenv SPBRPOL_IMAGE_RESOLUTION num [default:512]  
 setenv SPBRPOL_LOD 1/0 [default:1]  
 setenv SPBRPOL_SHADING 1/0 [default:1]  
 setenv SPBRPOL_PARTICLE_ZOOM 1/0 [default:1]  
 setenv SPBRPOL_CAMERA_ZOOM value [default:1.0]  
