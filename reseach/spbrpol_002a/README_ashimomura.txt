spbrpolの基本的な使い方は
./DOC/README.txt
を参照

適当的点削減の設定
	spbr_customize.hの中で設定

	const bool ENABLE_ADAPTIVELY_POINT_REDUCTION = true;
		適応的点削減を有効

	const double MIN_POLYGON_AREA_THRESHOLD = 15.0;
		面積比がこの値より小さくなったときに適応的点削減開始


AutoCaptureScreenの設定
	main.cppでscreenを宣言している箇所で変更

	kvs::glut::Screen screen( &app );			//通常
	// AutoCaptureScreen screen(&app, argv[1]);	//自動でスクリーンショットを
												//撮って終了する
	// screen.setIsAutoCapture(false);			//自動でスクリーンショットを撮るが、
												//自動終了させたくない時

Linux機でmakeしたときにstd::round()辺りでエラーを吐くかもしれない
そのときはMakefile.kvsの中の
%.o: %.cpp %.h
の部分にコンパイル時オプションとして-std=C++11を加え、
以降のコンパイルはkvsmakeを用いること（makeをするとMakefile.kvsが上書きされるため）