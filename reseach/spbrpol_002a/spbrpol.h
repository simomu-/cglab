/////////////////////
///// spbrpol.h /////
/////////////////////

#ifndef __SPBRPOL__
#define __SPBRPOL__

#include <fstream>
#include <vector>
#include "vertex.h"
#include "face.h"
#include <kvs/Vector3>
#include <kvs/PointObject>
#include <kvs/ValueArray>
#include <kvs/Camera>
#include <cstdlib>
#include <ctime>

#include "spbr_customize.h"

//-----------//
class SPBRPol {
//-----------//
private:
  // surface geometry data
  std::vector<Vertex> m_verts; //vertex positions
  std::vector<face>   m_faces; //facets
  int m_nvertex ; //number of vertices
  int m_nface   ; // number of facets written in PLY file.
                  //  Note: The number of polygons may change 
                  //        through the polygon decomposition.
  double m_S_area;  // surface area
  double m_min_P_area; //min polygon area (by shimomura 2017/11/08)

  // surface properties
  double m_alpha; //alpha (opasity) value
  bool   m_haveNormal;//Does input PLY file have normal-vector info?
  bool   m_haveColor ;//Does input PLY file have color info?

  // max-min coordinates of input polygon mesh
  kvs::Vector3d m_centerCoord;
  kvs::Vector3d m_maxCoord;
  kvs::Vector3d m_minCoord;

  // "View baounding box" to calculate camera parameter.
  kvs::Vector3d m_vbbMax;
  kvs::Vector3d m_vbbMin;

  // Rendering parameters
  double m_zoomFactor ;
  bool   m_flagShading ;
  unsigned int m_repeatLevel;
  unsigned int m_imageResolution;

  // Other paramters
  unsigned int m_bgGrayColor ;
  bool   m_flagFPS     ;
  bool   m_flagLOD     ;
  bool   m_flagShuffle ;
  bool   m_flagParticleZoom ;
  double m_particleDiameter ;

  // File I/O
  char* m_plyFileName;
  char* m_spbrFileName;

  // Projection Type (Perspective = 0, Orthogonal = 1, Frustum =2 )
  kvs::Camera::ProjectionType m_flagCameraProjectionType;

  // private methods (File I/O)
  bool ReadHeader(std::ifstream* reader);
  bool ReadBody(std::ifstream* reader);

public:
  // Constructors and destructor
  SPBRPol ( char* datafile );
  SPBRPol ( void );

  ~SPBRPol(){ m_verts.clear(); m_faces.clear(); }

  // Access methods
  double Alpha  ( void ) { return m_alpha  ; }
  void   SetAlpha  ( double alpha ) 
  { 
    m_alpha = alpha ; if( m_alpha > MAX_ALPHA ) { m_alpha = MAX_ALPHA; }
  }

  double Nvertex( void ) { return m_nvertex; }
  double Nface  ( void ) { return m_nface  ; }
  double Area   ( void ) { return m_S_area ; }
  void   SetViewBoundingBox ( const kvs::Vector3d& min, 
                              const kvs::Vector3d& max ) 
         { m_vbbMin = min;  m_vbbMax = max; } 
  void   SetOutputSPBRFileName ( char* spbr_file )
         { m_spbrFileName = spbr_file;  } 
  const char* OutputSPBRFileName ( void ) { return m_spbrFileName ; }

  // Read PLY file 
  bool ReadPly(char* datafile);

  // Does the input PLY file have color info?
  bool HaveColorFromFile(void){ return m_haveColor;};

  // Generate Point data
  void ConvertToParticles( double  alpha       , 
                           double  pixel_width  , 
                           int     repeat_level , 
                           int     resolution  , 
                           kvs::PointObject* particles );
	
  // Center and  Bounding box of the polygon object
  kvs::Vector3d ObjectCenter(void){ return m_centerCoord; } 
  kvs::Vector3d MaxCoord    (void){ return m_maxCoord   ; }	
  kvs::Vector3d MinCoord    (void){ return m_minCoord   ; }

  // Rendering parameters
  void   SetZoomFactor ( double factor ) { m_zoomFactor = factor ;}
  double ZoomFactor    ( void ) { return m_zoomFactor ; }

  void   SetFlagShading ( bool flag ) { m_flagShading = flag; }
  bool   IsShading      ( void ) { return m_flagShading ; }

  void   SetRepeatLevel ( unsigned int repeat_level ) 
  {  m_repeatLevel = repeat_level; 
     if ( m_repeatLevel < 1 ){ m_repeatLevel= 1; } 
  }
  unsigned int  RepeatLevel (void) { return m_repeatLevel; }

  void   SetImageResolution ( unsigned int resoln ) 
  {  m_imageResolution = resoln; 
     if ( m_imageResolution < 1 ){ m_imageResolution = 1; } 
  }
  unsigned int  ImageResolution (void) { return m_imageResolution; }

  // Other parameters
  void         SetBGGrayColor ( unsigned int gray_scale ) { m_bgGrayColor  = gray_scale ;}
  unsigned int BGGrayColor    ( void ) { return m_bgGrayColor; }

  void   SetFlagFPS    ( bool flag ) { m_flagFPS = flag; }
  bool   IsFPS         ( void ) { return m_flagFPS ; }

  void   SetFlagLOD    ( bool flag ) { m_flagLOD = flag; }
  bool   IsLOD         ( void ) { return m_flagLOD ; }

  void   SetFlagShuffle ( bool flag ) { m_flagShuffle = flag; }
  bool   IsShuffle      ( void ) { return m_flagShuffle ; }

  void   SetFlagParticleZoom ( bool flag ) { m_flagParticleZoom = flag; }
  bool   IsParticleZoom      ( void ) { return m_flagParticleZoom ; }

  void   SetParticleDiameter ( double diameter ) { m_particleDiameter = diameter ;}
  double ParticleDiameter ( void ) { return m_particleDiameter ; }

  void setProjectionType(kvs::Camera::ProjectionType type){ m_flagCameraProjectionType = type; }
  kvs::Camera::ProjectionType CameraProjectionType() { return m_flagCameraProjectionType; }

 private:
  // Write SPBR format file in ConvertToParticles() 
  void WriteSPBRFile ( int  repeat_level , 
                       int  resolution  , 
                       int  num_points  ,  
                       kvs::ValueArray<kvs::Real32>& pointCoords , 
                       kvs::ValueArray<kvs::Real32>& pointNormals ,
                       kvs::ValueArray<kvs::UInt8>&  pointColors );
  // shimomura
  bool AdaptPoint(double pol_ptcl_ratio);
};//SPBRPol

#endif
// end of spbrpol.h
