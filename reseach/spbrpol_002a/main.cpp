////////////////////
///// main.cpp /////
////////////////////

#include <kvs/ParticleBasedRenderer>
#include <kvs/glut/Application>
#include <kvs/glut/Screen>
#include <kvs/RotationMatrix33>
#include <kvs/CellByCellParticleGenerator>
#include <kvs/Vector3>
#include <kvs/PointObject>
#include <kvs/ObjectManager>

#include <cstring>
#include <cstdlib>

#include "CameraInfo.h"
#include "EventControl.h"
#include "spbrpol.h"
#include "fpscount.h"
#include "AutoCaptureScreen.h"
#include<iostream>
#include<fstream>
#include<sstream>
#define _USE_MATH_DEFINES
#include<cmath>

#include "spbr_customize.h"
#include "version.h"


//-----------------------------------------------//


// MAIN function
int main(int argc, char** argv)
{
  //----- Local Variables -----//

  // local variables (1) 
  double alpha        = DEFAULT_ALPHA ; // see spbr_customize.h
  int    repeat_level = DEFAULT_REPEAT_LEVEL; // see spbr_customize.h
  unsigned int bg_gray_color = DEFAULT_BG_GRAY_COLOR;// see spbr_customize.h 
  int image_resolution = DEFAULT_IMAGE_RESOLUTION ; // see spbr_customize.h 
  char* env = NULL ;
  double camera_distance = DEFAULT_CAMERA_DISTANCE ; 
  double zoom_factor     = DEFAULT_ZOOM_FACTOR     ;

  // local variables (2) 
  InitializeEvent  init;
  KeyPressEvent    key;
  SPBRPol spbrpol; // polygon-sampling engine

  //----- Message -----//
  std::cout << VERSION      << std::endl;
  std::cout << VERSION_DATE << "\n" << std::endl;


  //----- Argument control -----//
  //... (1) USAGE
  if (argc == 1 || argc > 5 ) {
    std::cout<<"USAGE:  " << std::endl;
    std::cout<<" (1) spbrpol  input.ply" << std::endl;
    std::cout<<" (2) spbrpol  input.ply  alpha" << std::endl;
    std::cout<<" (3) spbrpol  input.ply  alpha  L_R" << std::endl;
    std::cout<<" (4) spbrpol  input.ply  alpha  L_R  output.spbr" << std::endl;
    std::cout << std::endl;
    std::cout<<"Default parameters:  " << std::endl;
    std::cout<<" alpha = " << DEFAULT_ALPHA << std::endl;
    std::cout<<" L_R   = " << DEFAULT_REPEAT_LEVEL << std::endl;
    std::cout << std::endl;
    std::cout<<"Additional customization with environmental variables:  " << std::endl;
    std::cout<<" setenv SPBRPOL_BG_GRAY_COLOR num [default:0]  " << std::endl;
    std::cout<<" setenv SPBRPOL_FPS 1/0 [default:0]  " << std::endl;
    std::cout<<" setenv SPBRPOL_IMAGE_RESOLUTION num [default:512]  " << std::endl;
    std::cout<<" setenv SPBRPOL_LOD 1/0 [default:1]  " << std::endl;
    std::cout<<" setenv SPBRPOL_SHADING 1/0 [default:1]  " << std::endl;    
    std::cout<<" setenv SPBRPOL_PARTICLE_ZOOM 1/0 [default:1]  " << std::endl;    
    std::cout<<" setenv SPBRPOL_CAMERA_ZOOM value [default:1.0]  " << std::endl;    
    exit(1);
  }

  //... (2) Check if input == output
  if( argc >= 5 && !strcmp(argv[1], argv[4] ) ) { 
    std::cerr <<"ERROR: input = output " << std::endl;
    exit(2);
  }

  //... (3) Set alpha by arg[2]
  if( argc >= 3 ) {
    sscanf( argv[2], "%lg", &alpha );

    // error recovery from irrelevant user-input value
    alpha  = fabs( alpha ) ; 
    if( alpha > MAX_ALPHA ) { 
      alpha = MAX_ALPHA; // see spbr_customize.h
        std::cerr << "*** Alpha is truncated to "; 
        std::cerr << MAX_ALPHA  << std::endl;
    }
  } 
  spbrpol.SetAlpha( alpha ); 

  //... (4) Set repeat level by arg[3]
  if( argc >= 4 ) { 
    sscanf( argv[3], "%d", &repeat_level );
  } 
  spbrpol.SetRepeatLevel( repeat_level ); 

  //... (5) Set output SPBR file name by arg[4]
  // ... If arg[4] is given, created particles are saved 
  // ... to the file.
  if( argc >= 5 ) { 
    spbrpol.SetOutputSPBRFileName ( argv[4] );
  } 


  //----- Additional parameter setups -----//

  //... (1) FPS displaying control
  bool flag_display_fps = false ; // default
  env = getenv( "SPBRPOL_FPS" );
  if( env != NULL && !strcmp( env, "1" ) ) {
    flag_display_fps = true ; 
  }
  spbrpol.SetFlagFPS ( flag_display_fps ) ;

  //... (2) Background gray scale control 
  env = getenv( "SPBRPOL_BG_GRAY_COLOR" );
  if( env != NULL ) {
    sscanf( env, "%d", &bg_gray_color );
    if ( bg_gray_color > 255 ) 
    { bg_gray_color = 255; } 
    spbrpol.SetBGGrayColor ( bg_gray_color ) ;
  } // if (env)

  //...  (3) Shading
  spbrpol.SetFlagShading ( false ) ; // shading ON (initialization)
  // env = getenv( "SPBRPOL_SHADING" );
  // if( env != NULL && !strcmp( env, "1" ) ) {
  //   spbrpol.SetFlagShading ( true ) ; // shading ON
  // } else if( env != NULL && !strcmp( env, "0" ) ) {
  //   spbrpol.SetFlagShading ( false ) ; // shading OFF
  // } 

  //...  (4) Particle zoom
  spbrpol.SetFlagParticleZoom ( false ) ; // particle zoom ON (initialization)
  // env = getenv( "SPBRPOL_PARTICLE_ZOOM" );
  // if( env != NULL && !strcmp( env, "1" ) ) {
  //   spbrpol.SetFlagParticleZoom ( true ) ; // particle zoom ON
  // } else if( env != NULL && !strcmp( env, "0" ) ) {
  //   spbrpol.SetFlagParticleZoom ( false ) ; // particle zoom OFF
  // } 

  //...  (5)  LOD control
  spbrpol.SetFlagLOD( true ); // LOD ON (initialization)
  env = getenv( "SPBRPOL_LOD" );
  if( env != NULL && !strcmp( env, "1" ) ) {
    spbrpol.SetFlagLOD( true ); // LOD ON
  } else if( env != NULL && !strcmp( env, "0" ) ) {
    spbrpol.SetFlagLOD( false );
  } 

  //... (6) camera parameters
  env = getenv( "SPBRPOL_CAMERA_ZOOM" );
  if( env != NULL ) {
    // local variables 

    // Get zoom factor from the environmental variable
    sscanf( env, "%lg", &zoom_factor );
    zoom_factor  = fabs( zoom_factor ) ;
    if ( zoom_factor < MIN_ZOOM_FACTOR ) { 
      std::cerr << "*** Zoom factor is truncated to the min value "; 
      std::cerr << MIN_ZOOM_FACTOR << std::endl; 
      zoom_factor = MIN_ZOOM_FACTOR ; 
    }
    if ( zoom_factor > MAX_ZOOM_FACTOR ) { 
      std::cout << "*** Zoom factor is truncated to the max value " ;
      std::cout << MAX_ZOOM_FACTOR << std::endl; 
      zoom_factor = MAX_ZOOM_FACTOR ; 
    }

    // Save the zoom factor
    spbrpol.SetZoomFactor ( zoom_factor );

  } // if (env)


  //----- Read input PLY file -----//
  //--------------------//
  spbrpol.ReadPly(argv[1]);
  //--------------------//


  //----- Set up screen -----//

  //... (1) Instantialte application 
  kvs::glut::Application app(argc, argv);

  //... (2) Create Screen
  kvs::glut::Screen screen( &app );
  // AutoCaptureScreen screen(&app, argv[1]);
  // screen.setIsAutoCapture(false);

  //... (3) Set image resolution to the screen
  env = getenv( "SPBRPOL_IMAGE_RESOLUTION" );
  if( env != NULL ) {
    sscanf( env, "%d", &image_resolution );
    image_resolution = abs( image_resolution );
  } // if (env)
  spbrpol.SetImageResolution( image_resolution );
  screen.setGeometry( 0, 0, spbrpol.ImageResolution(), spbrpol.ImageResolution() );



  //----- Calc particle diameter -----//

  //... (1) Calc bounding box
  kvs::Vector3d min = spbrpol.MinCoord();
  kvs::Vector3d max = spbrpol.MaxCoord();

  std::cout << "BB_MIN: (" << min << ")" << std::endl;
  std::cout << "BB_MAX: (" << max << ")" << std::endl;

  double xwidth = fabs( max.x()-min.x() );
  double ywidth = fabs( max.y()-min.y() );
  double zwidth = fabs( max.z()-min.z() );

  double xmin = - 0.5 * xwidth ; 
  double ymin = - 0.5 * ywidth ;
  double zmin = - 0.5 * zwidth ;

  double xmax = + 0.5 * xwidth ; 
  double ymax = + 0.5 * ywidth ;
  double zmax = + 0.5 * zwidth ;

  //... (2) Store the bounding box 
  kvs::Vector3d  vbbMin( xmin, ymin, zmin );  
  kvs::Vector3d  vbbMax( xmax, ymax, zmax );
  spbrpol.SetViewBoundingBox ( vbbMin, vbbMax );

  //... (3) Calc particle diameter
  env = getenv( "SPBRPOL_CAMERA_ZOOM" );
  if( env != NULL ) {
    camera_distance = DEFAULT_CAMERA_DISTANCE / spbrpol.ZoomFactor() ;
    kvs::Vector3f camera_position ( 0.0, 0.0, camera_distance ) ;
    screen.scene()->camera()->setPosition( camera_position );
  }
  CameraInfo cameraInfo ;

  //... (4) Set camera type (shimomura)
  if( spbrpol.CameraProjectionType() == kvs::Camera::Orthogonal ) {
    screen.scene()->camera()->setProjectionType ( kvs::Camera::Orthogonal );
    std::cout << "** Orthogonal camera is set." << std::endl;
  } 
  if( spbrpol.CameraProjectionType() == kvs::Camera::Perspective ) {
    screen.scene()->camera()->setProjectionType ( kvs::Camera::Perspective );
    std::cout << "** Perspective camera is set." << std::endl;
  } 

  cameraInfo.set( screen.scene()->camera(), xmin, ymin, zmin, xmax, ymax, zmax ) ; 
  double particle_diameter = cameraInfo.pixelWidth( screen.scene()->camera() );

  spbrpol.SetParticleDiameter ( particle_diameter * DIAMETER_RATE );


  //----- Display main parameters -----//
  std::cout << "ALPHA       : " << spbrpol.Alpha()       << std::endl;
  std::cout << "REPEAT_LEVEL: " << spbrpol.RepeatLevel() << std::endl;
  std::cout << "RESOLUTION  : " << spbrpol.ImageResolution() << std::endl;
  std::cout << "particle diameter : " << spbrpol.ParticleDiameter() << std::endl;


  //----- Polygon sampling along scan lines -----//
  kvs::PointObject* object = new kvs::PointObject(); 
  std:: cout<<"** Start converting polygons to particles "<< std::endl;
  spbrpol.ConvertToParticles( spbrpol.Alpha(), spbrpol.ParticleDiameter(), spbrpol.RepeatLevel(), spbrpol.ImageResolution(), object );
	

  //----- Create a renderer -----//
  kvs::glsl::ParticleBasedRenderer* renderer = new kvs::glsl::ParticleBasedRenderer(); 
  //---------------------------------------------------------------------------------//


  //----- Set SPBR rendering parameters to the KVS renderer-----//
  //... (1) repeat level
  renderer->setRepetitionLevel ( spbrpol.RepeatLevel() );

  //... (2) Shading on/off
  // env = getenv( "SPBRPOL_SHADING" );
  // if( env != NULL && !strcmp( env, "1" ) ) {
  //   std::cout << "SHADING: ON" << std::endl;
  //   renderer->enableShading(); // shading ON
  // } else if( env != NULL && !strcmp( env, "0" ) ) {
  //   std::cout << "SHADING: OFF" << std::endl;
  //   renderer->disableShading(); // shading OFF
  // } else {
  //   std::cout << "SHADING: ON" << std::endl;
  //   renderer->enableShading(); // default (ON)
  // }

  if(spbrpol.IsShading()){
    renderer->enableZooming();
    std::cout << "SHADING: ON" << std::endl;

  }else{
    renderer->disableShading();
    std::cout << "SHADING: OFF" << std::endl;
  }

  //... (3) Particle zoom on/off
  // env = getenv( "SPBRPOL_PARTICLE_ZOOM" );
  // if( env != NULL && !strcmp( env, "1" ) ) {
  //   std::cout << "PARTICLE ZOOM: ON" << std::endl;
  //   renderer->enableZooming();
  // } else if( env != NULL && !strcmp( env, "0" ) ) {
  //   std::cout << "PARTICLE ZOOM: OFF" << std::endl;
  //   renderer->disableZooming();
  // } else {
  //   std::cout << "PARTICLE ZOOM: ON" << std::endl;
  //   renderer->enableZooming();
  // }

  if(spbrpol.IsParticleZoom()){
    renderer->enableZooming();
    std::cout << "PARTICLE ZOOM: ON" << std::endl;
  }else{
    renderer->disableZooming();
    std::cout << "PARTICLE ZOOM: OFF" << std::endl;
  }

  //... (4) LOD on/off
  env = getenv( "SPBRPOL_LOD" );
  if( env != NULL && !strcmp( env, "1" ) ) {
    std::cout << "LOD: ON" << std::endl;
    renderer->enableLODControl(); // LOD ON
  } else if( env != NULL && !strcmp( env, "0" ) ) {
    std::cout << "LOD: OFF" << std::endl;
    renderer->disableLODControl(); // LOD OFF
  } else {
    std::cout << "LOD: ON" << std::endl;
    renderer->enableLODControl(); // default (ON)
  }


  //----- Create a screen and register objects and the renderers -----//
  screen.registerObject( object, renderer );


  //----- Create a view window -----//
  //... (1) background color
  screen.setBackgroundColor( kvs::RGBColor ( bg_gray_color, 
                                             bg_gray_color, 
                                             bg_gray_color) );

  //... (2) window title
  char renderer_name[128];
  strcpy( renderer_name, "SPBRPOL (Rits Transparent Polygon Renderer)  " ) ;
  strcat( renderer_name, VERSION ) ;
  screen.setTitle( renderer_name );

  //... (3) event control
  screen.addEvent( &init );
  screen.addEvent( &key );

  //... (4) show the screen
  screen.show();
  key.displayMenu();

  //... (5) show FPS 
  Label label( &screen );
  kvs::RGBColor textcolor( 255 - bg_gray_color, 
                           255 - bg_gray_color, 
                           255 - bg_gray_color );
  if( flag_display_fps ) {
    label.setTextColor(textcolor);
    label.show();
  }

  // ... (6) object rotate (shimomura)
  // screen.scene()->objectManager()->rotate(kvs::ZRotationMatrix33<float>(1.0f));
  // screen.scene()->objectManager()->rotate(kvs::XRotationMatrix33(-60.0f));
  // screen.scene()->objectManager()->rotate(kvs::XRotationMatrix33(90.0f));

  //----- Start the main loop -----//
  return( app.run() );
  // return 0;

}// main()

// end of main.cpp
