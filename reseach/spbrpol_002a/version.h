/////////////////////
///// version.h /////
/////////////////////

#ifndef VERSION_HH
#define VERSION_HH

const char VERSION     [] = "***  SPBRPOL ver.002e ***";
const char VERSION_DATE[] = "              2017/11/23";

#endif
// end of version.h
