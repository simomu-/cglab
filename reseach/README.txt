それぞれの使い方等は各ディレクトリ下のREADME.txtを参照

特に再現用の実験なんかをするときは
各実験の自動化のためのシェルスクリプトや
spbrpolのAutoCaptureScreenに関することは
読んだ方がいいかもしれない

dual_depth_peeling/
	nvidiaによるDual Depth Peeling法のサンプル
	論文で速度比較に用いたもの


DepthPeelingForKVS/
	KVSの製作者によるDepthPeeling法のサンプル
	ポスター発表において比較図に用いたもの


spbrpol_002a/
	spbrpol、ポリゴンからspbrを行うやつ
	spbr_customize.hで適応的点削減のON/OFFを切り替えられる


create_square_polygon/
	いろんな分割数の正方形ポリゴンモデルを作るやつ


create_piled_polygon/
	正方形ポリゴンメッシュを任意枚数並べたモデルを作るやつ


calc_image_average/
	正方形ポリゴンモデルを用いた平均輝度値の計測実験


calc_ratio_param/
	面積比と生成点数のデータをいっぱい用意するやつ


high_polygon_test/
	高精細ポリゴンモデルのテスト用
	pig、teapot、bunny等


least_squares_method/
	最小二乗法による補正曲線を求めるやつ


models/
	実験に使用したポリゴンモデルがいっぱい


やり残したこと、今後
	補正曲線の模索

	三角形の重心から僅かにずらした位置に配置するやつ(手法の二つ目)の廃止
		長谷川先生曰く、三角形を再帰的に重心分割し
		そこに配置すれば三角形内に収まる

	そもそもスキャンライン上で点を置くことの廃止
		三角ポリゴン内に指定の点数を一様にランダムに
		配置する手法を確立できればほぼ完璧


おまけ（研究室関連のソースコード全部）
	https://bitbucket.org/simomu-/cglab/overview
