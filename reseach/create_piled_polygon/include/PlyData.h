#ifndef PLY_DATA_H
#define PLY_DATA_H 

#include <vector>
#include <string>

class PlyData{
protected:
	std::vector<double> vertexes;
	std::vector<double> normals;
	std::vector<unsigned char> colors;
	std::vector<int> faceIndexes;

	bool useVertexColor;
	std::string comment;

public:
	PlyData();
	PlyData(std::vector<double> v, std::vector<int> f);
	PlyData(std::vector<double> v, std::vector<int> f, std::vector<double> n);
	PlyData(std::vector<double> v, std::vector<int> f, std::vector<double> n, std::vector<unsigned char> c);
	~PlyData() = default;

	int GetFaceCount() const{ return faceIndexes.size() / 3; };
	int GetVertexCount() const{ return vertexes.size() / 3; };
	bool GetUseVertexColor() const{ return useVertexColor;};

	void SetMesh(std::vector<double> v, std::vector<int> f);
	void SetMesh(std::vector<double> v, std::vector<int> f, std::vector<double> n);
	void SetMesh(std::vector<double> v, std::vector<int> f, std::vector<double> n, std::vector<unsigned char> c);
	void SetComment(std::string s);
	std::string CreateHeader(int vnum, int fnum);
	void Write(std::string fname);

};

#endif