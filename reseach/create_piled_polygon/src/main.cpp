#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <PlyData.h>

const double polygonInterval = 20.0;

int main(int argc, char const *argv[]){

	if(argc < 3){
		std::cerr << "CreatePiledPolygon PILED_COUNT " << std::endl;
		std::cerr << "CreatePiledPolygon PILED_COUNT FACE_COUNT_PER_1_SQUARE" << std::endl;
		std::cerr << "CreatePiledPolygon PILED_COUNT FACE_COUNT_PER_1_SQUARE OUTPUT_FILE_NAME" << std::endl;
		return 1;
	}

	int piledCount = std::stoi(argv[1]);
	int faceNum = std::stoi(argv[2]);
	std::vector<double> vertexes;
	std::vector<int> faceIndexes;

	int div = std::sqrt(faceNum / 2) + 1;
	double delta = 1.0 / (double)(div - 1);
	for(int i = 0; i < piledCount; i++){
		for(int y = 0; y < div; y++){
			for(int x = 0; x < div; x++){
				vertexes.push_back(x * delta);
				vertexes.push_back(y * delta);
				vertexes.push_back((double)1.0 / piledCount * i);
			}
		}

		for(int u = 0; u < div - 1; u++){
			for(int v = 0; v < div - 1; v++){
				int f0 = (div * div * i) + u * div + v;
				int f1 = (div * div * i) + u * div + v + 1;
				int f2 = (div * div * i) + (u + 1) * div + v + 1;
				int f3 = (div * div * i) + (u + 1) * div + v;
				faceIndexes.push_back(f0);
				faceIndexes.push_back(f1);
				faceIndexes.push_back(f2);
				faceIndexes.push_back(f0);
				faceIndexes.push_back(f2);
				faceIndexes.push_back(f3); 
			}
		}		
	}


	PlyData ply = PlyData(vertexes, faceIndexes);

	std::string outputFileName;
	if(argc == 4){
		outputFileName = argv[3];
	}else{
		outputFileName = "piled" + std::to_string(piledCount) + "_" + std::to_string(ply.GetFaceCount() / piledCount) + ".ply";
	}
	ply.Write(outputFileName);
	std::cout << "Create Ply File -> " << outputFileName << std::endl;
	std::cout << "Face Count = " << ply.GetFaceCount() << std::endl;
	std::cout << "Vertex Count = " << ply.GetVertexCount() << std::endl;

	return 0;
}
