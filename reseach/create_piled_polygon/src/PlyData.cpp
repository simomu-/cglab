#include <PlyData.h>
#include <fstream>

PlyData::PlyData() : useVertexColor(false), comment(){

}

PlyData::PlyData(std::vector<double> v, std::vector<int> f) : 
					useVertexColor(false), comment(){
	SetMesh(v, f);
}

PlyData::PlyData(std::vector<double> v, std::vector<int> f, std::vector<double> n) : 
					useVertexColor(false), comment(){
	SetMesh(v, f, n);
}

PlyData::PlyData(std::vector<double> v, std::vector<int> f, std::vector<double> n, std::vector<unsigned char> c) :
					useVertexColor(false), comment(){
	SetMesh(v, f, n, c);
}

void PlyData::SetMesh(std::vector<double> v, std::vector<int> f){
	vertexes = v;
	faceIndexes = f;
	useVertexColor = false;
}

void PlyData::SetMesh(std::vector<double> v, std::vector<int> f, std::vector<double> n){
	vertexes = v;
	faceIndexes = f;
	normals = n;
	useVertexColor = false;
}


void PlyData::SetMesh(std::vector<double> v, std::vector<int> f, std::vector<double> n, std::vector<unsigned char> c){
	vertexes = v;
	faceIndexes = f;
	normals = n;
	colors = c;
	useVertexColor = true;
}

void PlyData::SetComment(std::string s){
	comment = s;
}


std::string PlyData::CreateHeader(int vnum, int fnum){
	std::string h;
	h += "ply\n";
	h += "format ascii 1.0\n";
	if(!comment.empty()){
		h += "comment " + comment + "\n";
	}
	h += "element vertex " + std::to_string(vnum) + "\n";
	h += "property float x\n";
	h += "property float y\n";
	h += "property float z\n";
	h += "property float nx\n";
	h += "property float ny\n";
	h += "property float nz\n";
	if(useVertexColor){
		h += "property uchar red\n";
		h += "property uchar green\n";
		h += "property uchar blue\n";
	}
	h += "element face " + std::to_string(fnum) + "\n";
	h += "property list uchar uint vertex_indices\n";
	h += "end_header\n";
	return h;
}

void PlyData::Write(std::string fname){

	std::ofstream fout;

	fout.open(fname.c_str());
	fout << CreateHeader(vertexes.size() / 3, faceIndexes.size() / 3);
	for(int i = 0; i < vertexes.size(); i += 3){
		fout << vertexes[i + 0] << " " << vertexes[i + 1] << " " << vertexes[i + 2];
		fout << " " << 0.0 << " " << 0.0 << " " << 1.0 ; 
		if(useVertexColor){
			fout << " " << +colors[i + 0] << " " << +colors[i + 1] << " " << +colors[i + 2];
		}
		fout << std::endl;
	}

	for(int i = 0; i < faceIndexes.size(); i += 3){
		fout << 3 << " " 
			 << faceIndexes[i + 0] << " " 
			 << faceIndexes[i + 1] << " " 
			 << faceIndexes[i + 2] << std::endl; 
	}

	fout.close();
}