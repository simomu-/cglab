正方形ポリゴンモデルを入力枚数分だけ並べたモデルを作るやつ

DepthPeeling法とSPBRの速度比較実験を行うための
ポリゴンモデルを制作するのに使用

コンパイル方法:
	make
実行方法:
	./CreatePiledPolygon PILED_COUNT
	./CreatePiledPolygon PILED_COUNT FACE_COUNT_PER_1_SQUARE
	./CreatePiledPolygon PILED_COUNT FACE_COUNT_PER_1_SQUARE OUTPUT_FILE_NAME

補足:
	PILED_COUNT 正方形ポリゴンを並べる枚数
	FACE_COUNT_PER_1_SQUARE 1枚の正方形ポリゴンの分割数 無指定で2
	出力ファイル名無指定の場合
	piled{並べた正方形ポリゴン枚数}_{1枚の正方形ポリゴンの分割数}.ply 

