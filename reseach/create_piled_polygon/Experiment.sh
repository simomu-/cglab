#!/bin/sh

# for file in `\find ../models/SquarePolygon -maxdepth 1 -type f`; do
# 	echo $file
# 	SPBR_FILE_NAME=`basename $file`
#     SPBR_FILE_NAME=`echo $SPBR_FILE_NAME | sed 's/\.[^\.]*$//'`
#     echo $SPBR_FILE_NAME.spbr
#     spbrpol $file 0.3 500 ./InputImage/$SPBR_FILE_NAME.spbr
# done

DIVISION=1000

for i in {1..10}
do
	# ./CreatePiledPolygon $i ../models/PiledPolygon/piled$i.ply
	FILE="${i}_${DIVISION}"
	./CreatePiledPolygon $i $DIVISION piled$FILE.ply
	spbrpol piled$FILE.ply 0.3 500 piled$FILE.spbr
done


