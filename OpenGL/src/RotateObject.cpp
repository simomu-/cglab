#include "RotateObject.h"

RotateObject::RotateObject(std::string objFileName) : obj(objFileName){
	
}

void RotateObject::Draw(){
	obj.Draw();
}

void RotateObject::Update(){
	obj.Rotate(0.0, 1.0, 0.0);
}