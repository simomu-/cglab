#pragma once
#include "Objects/ObjectBase.h"
#include "Objects/PolygonObject.h"
#include "Objects/SquarePolygonObject.h"
#include <string>

class RotateObject : public ObjectBase{

	// PolygonObject obj;
	SquarePolygonObject obj;

public:
	RotateObject(std::string objFileName);
	~RotateObject() = default;
	
	void Draw();
	void Update();
};