#include "Title.h"
#include "../Utility/SceneManager.h"
#include "../Utility/InputManager.h"
#include "Light.h"
#include "GlidPlane.h"
#include "CustomCamera.h"
#include "PolygonObject.h"
#include "Player.h"
#include "EnemyManager.h"

Title::Title() : canControll(false){

}

void Title::Update(){

	if(!canControll && InputManager::GetInstance().IsReturnKey){
		return;
	}else{
		canControll = true;
	}

	if(InputManager::GetInstance().IsReturnKey){
		moveNextScene();
	}
}

void Title::moveNextScene(){

	Light* light = new Light(0);
	light->SetColor(1.0, 1.0, 1.0);
	light->SetPosition(5.0, 5.0, 5.0);

	Player* player = new Player();
	player->SetPosition(0.0f, 0.0f, -50.0f);

	CustomCamera* followCamera = new CustomCamera(*player);
	followCamera->SetRadius(20.0f);
	followCamera->SetPosition(1.0, 10.0, -20.0);

	GlidPlane* glidPlane = new GlidPlane(512, 5);
	glidPlane->SetPosition(0.0, -1.0, 0.0);

	EnemyManager* enemyManager = new EnemyManager();

	SceneManager::GetInstance().AddNextSceneObject( followCamera);
	SceneManager::GetInstance().AddNextSceneObject( light);
	SceneManager::GetInstance().AddNextSceneObject( glidPlane);
	SceneManager::GetInstance().AddNextSceneObject( player);
	SceneManager::GetInstance().AddNextSceneObject( enemyManager);

	SceneManager::GetInstance().SceneChange();
}