#pragma once
#include "PolygonObject.h"
#include "../Utility/SphereCollider.h"

class Player : public PolygonObject{
private:
	float speed;
	float rotateSpeed;
	float reloadTime;
	float currentTime;
	float aliveTime;
	float health;
	bool isReloading;
	SphereCollider collider;

public:
	Player();
	~Player();
	void Draw();
	void Update();
	void LateUpdate();
	void Delete();

	void MoveTitleScene();
};