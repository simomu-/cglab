#pragma once
#include "ObjectBase.h"

class Title : public ObjectBase{

	bool canControll;

public:
	Title();
	~Title() = default;

	void Update();

	void moveNextScene();
	
};