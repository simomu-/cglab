#include <GL/glut.h>
#include "Cube.h"

Cube::Cube() : size(1){
}

void Cube::Draw(){

	glPushMatrix();
	glTranslated(position_x, position_y, position_z);
	glRotated(rotation_x, 1.0, 0.0, 0.0);
	glRotated(rotation_y, 0.0, 1.0, 0.0);
	glRotated(rotation_z, 0.0, 0.0, 1.0);
	glTranslated(-position_x, -position_y, -position_z);

	glTranslated(position_x, position_y, position_z);

	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colorArray.data());

	glutSolidCube(size);
	glPopMatrix();

}

void Cube::SetColor(float r, float g, float b){

	colorArray = {r, g, b, 1.0};

}