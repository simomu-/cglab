#pragma once
#include "ObjectBase.h"
#include "Camera.h"

class FollowCamera : public Camera
{
protected:
	ObjectBase& targetObject;
	float radius;
public:
	FollowCamera(ObjectBase &target);
	void SetTargetObject(ObjectBase &target);
	void SetRadius(float r);
	void Update();
	void Draw();
	
};