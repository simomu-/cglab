#include <GL/glut.h>
#include "Light.h"
#include <array>
#include <iostream>

Light::Light(int index) : lightIndex(index){
}

void Light::SetColor(float r, float g, float b){
	colorR = r;
	colorG = b;
	colorB = b;
	colorArray = {colorR, colorG, colorB, 1.0};
	std::array<float,4> cArray = {colorR, colorG, colorB, 1.0};
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, colorArray.data());
	// glLightfv(GL_LIGHT0, GL_SPECULAR, cArray.data());
	glLightfv(GL_LIGHT0, GL_AMBIENT, colorArray.data());
;}

void Light::Draw(){
	glPushMatrix();
	positionArray[3] = 0.0f;
	glLightfv(GL_LIGHT0, GL_POSITION, positionArray.data());
	glPopMatrix();	
}