#pragma once
#include "PolygonObject.h"
#include <vector>
#include <array>
#include <string>

struct SquareFace{

	int faceIndex0;
	int faceIndex1;
	int faceIndex2;
	int faceIndex3;

	float diffuseMaterialR;
	float diffuseMaterialG;
	float diffuseMaterialB;

	std::array<float, 4> diffuseMaterialArray;

	SquareFace(int f0, int f1, int f2, int f3) : faceIndex0(f0), 
												 faceIndex1(f1), 
												 faceIndex2(f2), 
												 faceIndex3(f3){
	}

	void SetDiffuseMaterial(float r, float g, float b){
		diffuseMaterialArray[0] = r;
		diffuseMaterialArray[1] = g;
		diffuseMaterialArray[2] = b;
		diffuseMaterialArray[3] = 1.0f;
	}

	float* GetDiffuseMaterialArray(){
		return diffuseMaterialArray.data();
	}
	
};

class SquarePolygonObject : public PolygonObject {

	std::vector<SquareFace> squareFaces;

	void LoadSquareFace(std::string objFileName);
	void SetSquareFaces(char dataLine[], float r, float g, float b);

public:
	SquarePolygonObject();
	SquarePolygonObject(std::string objFileName);
	~SquarePolygonObject() = default;

	void Draw();

};