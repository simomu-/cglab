#include "Explosion.h"
#include "../Utility/SceneManager.h"
#include <GL/glut.h>
#include <cmath>

Explosion::Explosion(float s) : size(s), 
								cullentSize(0.0f), 
								lifeTime(0.5f){
	diffuseArray = {0.0f, 0.0f, 0.0f, 0.0f};
	emissionArray = {1.0f, 0.0f, 0.0f, 0.0f};
	deltaSize = size / lifeTime;
	deltaColor = 1.0f / lifeTime;
}

void Explosion::Draw(){
	glPushMatrix();
	glTranslatef(position_x, position_y, position_z);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseArray.data());
	// glMaterialfv(GL_FRONT, GL_DIFFUSE, emissionArray.data());
	glMaterialfv(GL_FRONT, GL_EMISSION, emissionArray.data());
	glutSolidSphere(cullentSize, 10, 10);

	float c[] = {0.0, 0.0, 0.0, 0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, c);

	glPopMatrix();
}

void Explosion::Update(){

	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();

	lifeTime -= deltaTime;
	emissionArray[1] += deltaColor * deltaTime;
	cullentSize = size * (1.0f - pow(1.0f/100.0f,emissionArray[1]));

	if(lifeTime < 0){
		lifeTime = 1.0f;
		isDelete = true;
	}
}