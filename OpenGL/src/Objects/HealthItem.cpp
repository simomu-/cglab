#include "HealthItem.h"
#include "PolygonObject.h"
#include "../Utility/SceneManager.h"

HealthItem::HealthItem() : collider(2.0f, "HealthItem"), PolygonObject("Resource/HealthItem.obj"){

}

HealthItem::~HealthItem(){
	collider.Delete();
}

void HealthItem::Draw(){
	PolygonObject::Draw();
	collider.DrawForDebug();
}

void HealthItem::Update(){
	collider.SetPosition(position_x, position_y, position_z);
	Rotate(0.0f, 90.0f * SceneManager::GetInstance().GetUpdateDeltaTime(), 0.0f);
}

void HealthItem::LateUpdate(){
	if(SceneManager::GetInstance().IsHit(&collider,"Player")){
		isDelete = true;
	}
}

void HealthItem::Delete(){
	// collider.Delete();
	PolygonObject::Delete();
}
