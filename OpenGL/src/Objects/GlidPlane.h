#pragma once
#include "ObjectBase.h"

class GlidPlane : public ObjectBase{
	
	int size;
	float glidSize;

public:
	GlidPlane();
	GlidPlane(int s, float gSize);
	void Draw();
};