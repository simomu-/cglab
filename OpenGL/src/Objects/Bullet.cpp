#include "Bullet.h"
#include "ObjectBase.h"
#include "Explosion.h"
#include "../Utility/SceneManager.h"
#include <GL/glut.h>
#include <iostream>

Bullet::Bullet(float speed) : speed(speed),size(0.5), collider(size), lifeTime(1.0){
	diffuseArray = {1.0, 0.0, 0.0, 0.0};
	emissionArray = {1.0, 0.0, 0.0, 0.0};
}

Bullet::~Bullet(){
	// std::cout << "Delete Bullet" << std::endl;
	collider.Delete();
}

void Bullet::Draw(){
	glPushMatrix();
	glTranslatef(position_x, position_y, position_z);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseArray.data());
	glMaterialfv(GL_FRONT, GL_EMISSION, emissionArray.data());
	glutSolidSphere(size, 10, 10);

	float c[] = {0.0, 0.0, 0.0, 0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, c);

	glPopMatrix();

	collider.DrawForDebug();
}

void Bullet::Update(){

	lifeTime -= SceneManager::GetInstance().GetUpdateDeltaTime();
	if(lifeTime < 0){
		lifeTime = 1.0f;
		isDelete = true;

	}
	MoveForward(speed);
	collider.SetPosition(position_x, position_y, position_z);


}

void Bullet::LateUpdate(){
	if(SceneManager::GetInstance().IsHit(&collider)){
		if(!SceneManager::GetInstance().IsHit(&collider, "HealthItem")){
			MoveForward(-speed);
			
			Explosion* exp = new Explosion(size * 2.5);
			exp->SetPosition(position_x, position_y, position_z);
			SceneManager::GetInstance().AddObject(exp);
			isDelete = true;
		}
	}
}

void Bullet::Delete(){
	// collider.Delete();
	ObjectBase::Delete();
}