#pragma once
#include "ObjectBase.h"
#include <array>

class Camera : public ObjectBase{

	std::array<float, 3> targetPosition;

public:
	Camera();
	void Draw();
	void SetTargetPosition(float* pos);
};