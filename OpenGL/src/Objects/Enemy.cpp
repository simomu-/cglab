#include "Enemy.h"
#include "../Utility/SceneManager.h"
#include "Explosion.h"
#include "Bullet.h"
#include "HealthItem.h"
#include "EnemyManager.h"
#include <iostream>
#include <random>

Enemy::Enemy() : 
	speed(0.25), 
	rotateSpeed(1.5), 
	currentTime(0),
	rotateTime(1.0f),
	rotateInterval(3.0f),
	collider(2.5),
	life(100.0f),
	isRotate(false),
	PolygonObject(""){

	shootInterval = 0.3f;
	beginRotateTime = 0.0f;
	nextRotateTime  = 0.0f;
	nextShootTime   = 0.0f;
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::uniform_int_distribution<> rand(0, 99);
	if(rand(mt) <= 50){
		LoadObjFile("Resource/Enemy.obj");
	}else{
		LoadObjFile("Resource/Player2.obj");
		speed = 0.5f;
		rotateSpeed = 1.5f;
		rotateInterval = 2.0f;
		shootInterval = 0.2f;
	}
}

Enemy::~Enemy(){
	collider.Delete();
}

void Enemy::Draw(){
	PolygonObject::Draw();
	collider.DrawForDebug();
}

void Enemy::Update(){
	
	std::random_device rnd;
	std::mt19937 mt(rnd());

	currentTime += SceneManager::GetInstance().GetUpdateDeltaTime();
	collider.SetPosition(position_x, position_y, position_z);
	MoveForward(speed);

	if(currentTime >= nextRotateTime && !isRotate){
		isRotate = true;
		std::uniform_int_distribution<> randr(0, 20);
		rotateTime = randr(mt) / 10.0f;
		beginRotateTime = currentTime;
	}

	if(isRotate){
		Rotate(0.0f , rotateSpeed, 0.0f);
		if(currentTime - beginRotateTime >= rotateTime){
			isRotate = false;
			nextRotateTime = currentTime + rotateInterval;
		}
	}

	if(currentTime >= nextShootTime){
		Bullet* bullet = new Bullet(3);
		float forward_x = sin(M_PI / 180.0 * rotation_y);
		float forward_z = cos(M_PI / 180.0 * rotation_y);

		float x = forward_x * 3.0f;
		float z = forward_z * 3.0f;

		bullet->SetPosition(position_x + x, position_y, position_z + z);
		bullet->SetRotation(rotation_y, rotation_y, rotation_z);
		SceneManager::GetInstance().AddObject(bullet);
		nextShootTime += shootInterval;
	}

}

void Enemy::LateUpdate(){
	if(SceneManager::GetInstance().IsHit(&collider)){
		life -= 33.4f;
		if(life <= 0){
			isDelete = true;
			Explosion* exp = new Explosion(4.5f);
			exp->SetPosition(position_x, position_y, position_z);
			SceneManager::GetInstance().AddObject(exp);	
			
			std::random_device rnd;
			std::mt19937 mt(rnd());
			std::uniform_int_distribution<> rand(0, 100);
			if(rand(mt) <= 10){
				HealthItem* health = new HealthItem();
				health->SetPosition(position_x, position_y, position_z);
				SceneManager::GetInstance().AddObject(health);
			}

			EnemyManager::DecreaseEnemyCount();

		}
	}
}

void Enemy::Delete(){
	PolygonObject::Delete();
}