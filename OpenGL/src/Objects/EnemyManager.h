#pragma once
#include "ObjectBase.h"

class EnemyManager : public ObjectBase{
private:
	float enemySpawnInterval;
	float currentTime;
	float stageSize;
	int maxEnemyCount;
	static int currentEnemyCount;

public:
	EnemyManager();

	void Update();
	static void DecreaseEnemyCount();
	
};