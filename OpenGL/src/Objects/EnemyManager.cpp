#include "EnemyManager.h"
#include "Enemy.h"
#include "../Utility/SceneManager.h"
#include <random>
#include <iostream>

int EnemyManager::currentEnemyCount;

EnemyManager::EnemyManager() : enemySpawnInterval(2.5),
							   currentTime(0),
							   stageSize(128),
							   maxEnemyCount(10){

}

void EnemyManager::Update(){
	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();
	currentTime += deltaTime;
	if(currentTime >= enemySpawnInterval && currentEnemyCount < maxEnemyCount){
		currentTime = 0;
		Enemy* enemy = new Enemy();

		std::random_device rnd;
		std::mt19937 mt(rnd());
		std::uniform_int_distribution<> randp(-stageSize / 2, stageSize / 2);
		std::uniform_int_distribution<> randr(0, 365);

		float x = randp(mt);
		float z = randp(mt);

		enemy->SetPosition(x, 0.0f, z);
		enemy->SetRotation(0.0, randr(mt), 0.0);

		SceneManager::GetInstance().AddObject(enemy);
		std::cout << "Enemy Spawn " << x << ","<< z << std::endl;
		currentEnemyCount++;
	}

}

void EnemyManager::DecreaseEnemyCount(){
	currentEnemyCount--;
}