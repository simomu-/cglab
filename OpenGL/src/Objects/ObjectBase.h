#pragma once
#include <array>

class ObjectBase{

protected:
	bool isDelete;

	float position_x;
	float position_y;
	float position_z;

	std::array<float,4> positionArray;

	float rotation_x;
	float rotation_y;
	float rotation_z;
	
public:
	ObjectBase();
	virtual ~ObjectBase();
	float PositionX() const;
	float PositionY() const;
	float PositionZ() const;
	float* GetPositionArray();

	void SetPosition(float x, float y, float z);
	void Translate(float x, float y, float z);

	float RotationX() const;
	float RotationY() const;
	float RotationZ() const;
	
	void Rotate(float x, float y, float z);
	void SetRotation(float x, float y, float z);
	void MoveForward(float v);

	virtual void Update();
	virtual void LateUpdate();
	virtual void Draw();
	virtual void CheckDelete();
	virtual void Delete();
};