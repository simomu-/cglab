#pragma once
#include "FollowCamera.h"
#include "ObjectBase.h"

class CustomCamera : public FollowCamera{
private:
	float currentTime;
public:
	CustomCamera(ObjectBase &target);
	void Update();
	
};