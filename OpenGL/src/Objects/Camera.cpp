#include "Camera.h"
#include <GL/glut.h>
#include <iostream>

Camera::Camera(){};

void Camera::Draw(){

	gluLookAt(position_x, position_y, position_z, 
			  targetPosition[0], targetPosition[1], targetPosition[2],
			  0.0, 1.0, 0.0);

}

void Camera::SetTargetPosition(float* pos){
	targetPosition[0] = pos[0];
	targetPosition[1] = pos[1];
	targetPosition[2] = pos[2];
}