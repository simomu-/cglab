#pragma once
#include "ObjectBase.h"
#include "../Utility/BmpPicture.h"
#include <string>
#include <GL/glut.h>

class Image : public ObjectBase{
protected:
	BmpPicture picture;
	GLuint texture;
	int rasterPositionX;
	int rasterPositionY;
	float pixelZoom;

public:
	Image(std::string fileName);
	~Image();

	void Draw();
	void SetRasterPosition(float x, float y);
	void SetPixelZoom(float zoom);
	
};