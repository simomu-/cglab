#include "Player.h"
#include "../Utility/SceneManager.h"
#include "../Utility/InputManager.h"
#include "../Utility/UIManager.h"
#include "Bullet.h"
#include "Explosion.h"
#include "Title.h"
#include "Light.h"
#include "PolygonObject.h"
#include "GlidPlane.h"
#include "Camera.h"
#include "Image.h"
#include <iostream>
	
Player::Player() : speed(0.5), 
				   rotateSpeed(1.5), 
				   isReloading(false), 
				   collider(2.5f, "Player"), 
				   health(100.0f),
				   PolygonObject("Resource/Player1.obj"){
	reloadTime = 0.25;
	currentTime = 0.0;
	aliveTime = 0.0;
}

Player::~Player(){
	collider.Delete();
}

void Player::Draw(){
	PolygonObject::Draw();
	collider.DrawForDebug();
}

void Player::Update(){

	static bool isActive = false;

	if(!isVisible){
		if(InputManager::GetInstance().IsReturnKey){
			MoveTitleScene();
		}
		return;
	}

	UIManager::GetInstance().SetCurrentTime(aliveTime);

	if(currentTime <= 2.0 && !isActive){
		MoveForward(speed);
		currentTime += SceneManager::GetInstance().GetUpdateDeltaTime();
		return;
	}else{
		isActive = true;
	}

	aliveTime += SceneManager::GetInstance().GetUpdateDeltaTime();

	MoveForward(speed);
	if(InputManager::GetInstance().IsLeftButton){
		Rotate(0.0, rotateSpeed, 0.0);
	}
	if(InputManager::GetInstance().IsRightButton){
		Rotate(0.0, -rotateSpeed, 0.0);
	}

	if(InputManager::GetInstance().IsUpButton){
		speed = 0.75f;
	}else{
		if(InputManager::GetInstance().IsDownButton){
			speed = 0.25f;
		}else{
			speed = 0.5f;
		}
	}

	collider.SetPosition(position_x, position_y, position_z);

	if(InputManager::GetInstance().IsShootButton && !isReloading){
		Bullet* bullet = new Bullet(3);
		float forward_x = sin(M_PI / 180.0 * rotation_y);//* cos(M_PI / 180.0 * rotation_x);
		float forward_z = cos(M_PI / 180.0 * rotation_y);

		float x = forward_x * 2.5f;
		float z = forward_z * 2.5f;

		bullet->SetPosition(position_x + x, position_y, position_z + z);
		bullet->SetRotation(rotation_y, rotation_y, rotation_z);
		SceneManager::GetInstance().AddObject(bullet);
		isReloading = true;
	}

	if(isReloading){
		currentTime += SceneManager::GetInstance().GetUpdateDeltaTime();
		if(currentTime >= reloadTime){
			currentTime = 0;
			isReloading = false;
		}
	}
}

void Player::LateUpdate(){

	if(SceneManager::GetInstance().IsHit(&collider, "HealthItem")){
		health += 20.0f;
		if(health >= 100.0f){
			health = 100.0f;
		}
	}else{
		if(SceneManager::GetInstance().IsHit(&collider)){
			if(health > 0){
				health -= 12.5f;
				if(health <= 0){
					health = 0.0f;
					isVisible = false;
					Explosion* exp = new Explosion(4.5f);
					exp->SetPosition(position_x, position_y, position_z);
					SceneManager::GetInstance().AddObject(exp);

					Image* image = new Image("Resource/GameOver.bmp");
					image->SetPixelZoom(5);
					image->SetRasterPosition(1270 / 2, 720 / 4);
					SceneManager::GetInstance().AddImageObject(image);
					Image* image2 = new Image("Resource/GameOver2.bmp");
					image2->SetPixelZoom(2.5);
					image2->SetRasterPosition(1270 / 2, 720 * 3.0/4.0);
					SceneManager::GetInstance().AddImageObject(image2);
				}
			}
		}
	}
	UIManager::GetInstance().SetHealth(health);

}

void Player::Delete(){
	PolygonObject::Delete();
}

void Player::MoveTitleScene(){
	Light* light = new Light(0);
	light->SetColor(1.0, 1.0, 1.0);
	light->SetPosition(5.0, 5.0, 5.0);

	GlidPlane* glidPlane = new GlidPlane(512, 5);
	glidPlane->SetPosition(0.0, -1.0, 0.0);

	PolygonObject* titleObject = new PolygonObject("Resource/Player1.obj");
	titleObject->SetPosition(0.0, 0.0, 0.0);

	Camera* camera = new Camera();
	camera->SetPosition(10.0, 5.0, 10.0);
	camera->SetTargetPosition(titleObject->GetPositionArray());

	Title* title = new Title();

	Image* image1 = new Image("Resource/Title.bmp");
	image1->SetPixelZoom(5);
	image1->SetRasterPosition(1270 / 2, 720 / 8);

	Image* image2 = new Image("Resource/Title2.bmp");
	image2->SetPixelZoom(2.5);
	image2->SetRasterPosition(1270 / 2, 720 * 3.0/4.0);

	SceneManager::GetInstance().AddNextSceneObject( light);
	SceneManager::GetInstance().AddNextSceneObject( camera);
	SceneManager::GetInstance().AddNextSceneObject( glidPlane);
	SceneManager::GetInstance().AddNextSceneObject( titleObject);
	SceneManager::GetInstance().AddNextSceneObject( title);

	SceneManager::GetInstance().AddNextSceneImageObject	( image1);
	SceneManager::GetInstance().AddNextSceneImageObject( image2);

	SceneManager::GetInstance().SceneChange();
}