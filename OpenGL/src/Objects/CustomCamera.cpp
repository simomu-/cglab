#include "CustomCamera.h"
#include "FollowCamera.h"
#include "ObjectBase.h"
#include "../Utility/SceneManager.h"
#include <GL/glut.h>

CustomCamera::CustomCamera(ObjectBase &target) : currentTime(0) , FollowCamera(target){

}

void CustomCamera::Update(){
	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();
	currentTime += deltaTime;
	if(currentTime <= 2.0f){
		gluLookAt(position_x, position_y, position_z, 
		  targetObject.PositionX(), targetObject.PositionY(), targetObject.PositionZ(),
		  0.0, 1.0, 0.0);
		Translate(-0.5 * deltaTime, 0.0f, 0.0f);
	}else{
		FollowCamera::Update();
	}
}