#include "SphereCollider.h"
#include "SceneManager.h"
#include <iostream>
#include <string>
#include <GL/glut.h>

SphereCollider::SphereCollider(float r) : radius(r){
	SceneManager::GetInstance().AddCollider(this);
}

SphereCollider::SphereCollider(float r, std::string name) : radius(r), Collider(name){
	SceneManager::GetInstance().AddCollider(this);
}

SphereCollider::~SphereCollider(){

}

void SphereCollider::GetSphereColliderInfo(float &x, float &y, float &z, float &r){
	x = position_x;
	y = position_y;
	z = position_z;
	r = radius;
}

void SphereCollider::SetPosition(float x, float y, float z){
	position_x = x;
	position_y = y;
	position_z = z;

}

void SphereCollider::Delete(){
	// std::cout << "delete Collider" << std::endl;
	SceneManager::GetInstance().RemoveCollider(this);
}

void SphereCollider::DrawForDebug(){
	if(isDebug){
		Collider::DrawForDebug();
		glPushMatrix();
		glDisable(GL_LIGHTING);
		glColor3f(0.0f, 1.0f, 0.0f);
		glTranslatef(position_x, position_y, position_z);
		glutWireSphere(radius, 20, 20);
		glColor3f(0.0f, 0.0f, 0.0f);
		glEnable(GL_LIGHTING);
		glPopMatrix();
	}
}