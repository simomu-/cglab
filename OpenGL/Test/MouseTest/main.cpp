#include <GL/glut.h>
#include <stdio.h>
#define MAXPOINTS 100

GLint point[MAXPOINTS][2];
int pointnum = 0;

void Display(){
	printf("Display\n");

	int i = 0;
	glClear(GL_COLOR_BUFFER_BIT);

	if(pointnum > 1){
		glColor3d(0,0,0);
		glBegin(GL_LINE);
		for(int i = 0; i < pointnum; i++){
			printf("Make Vertex\n");
			glVertex2iv(point[i]);
		}
		glEnd();
	}

	glFlush();
}

void Resize(int w,int h){
	glViewport(0,0,w,h);
	glLoadIdentity();

	glOrtho(-0.5, (GLdouble)w - 0.5, (GLdouble)h - 0.5, -0.5, -1.0, 1.0);
}

void Mouse(int button, int state, int x, int y){
	
	switch(button){
		case GLUT_LEFT_BUTTON:
			printf("Left Button\n");
			point[pointnum][0] = x;
			point[pointnum][1] = y;
			if(state == GLUT_UP){
				glColor3d(0,0,0);
				printf("Button Up\n");
				glBegin(GL_LINES);
				glVertex2iv(point[pointnum - 1]);
				glVertex2iv(point[pointnum]);
				glEnd();
				glFlush();
			}
			if(pointnum < MAXPOINTS - 1){
				pointnum++;
			}
			break;
		
	}
	for(int i = 0; i < pointnum; i++){
		printf("%d , %d\n", point[i][0],point[i][1]);
	}
}

void Init(){
	glClearColor(1,1,1,1.0);
}

int main(int argc, char *argv[]){

	glutInitWindowPosition(100,100);
	glutInitWindowSize(320,240);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(Display);
	glutMouseFunc(Mouse);
	glutReshapeFunc(Resize);
	Init();
	glutMainLoop();
	
	return 0;
}