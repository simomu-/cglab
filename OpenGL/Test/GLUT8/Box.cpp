#include <GL/glut.h>
#include "Box.h"

Box::Box() : red({0.8f, 0.2f, 0.2f, 1.0f}),
			 vertex{
				{ 0.0, 0.0, 0.0 },
				{ 1.0, 0.0, 0.0 },
				{ 1.0, 1.0, 0.0 },
				{ 0.0, 1.0, 0.0 },
				{ 0.0, 0.0, 1.0 },
				{ 1.0, 0.0, 1.0 },
				{ 1.0, 1.0, 1.0 },
				{ 0.0, 1.0, 1.0 }},

			 face{
				{ 0, 1, 2, 3 },
				{ 1, 5, 6, 2 },
				{ 5, 4, 7, 6 },
				{ 4, 0, 3, 7 },
				{ 4, 5, 1, 0 },
				{ 3, 2, 6, 7 }},

			 normal{
				{ 0.0, 0.0,-1.0 },
				{ 1.0, 0.0, 0.0 },
				{ 0.0, 0.0, 1.0 },
				{-1.0, 0.0, 0.0 },
				{ 0.0,-1.0, 0.0 },
				{ 0.0, 1.0, 0.0 }} {

}

void Box::Draw(){
	// glColor3d(0,0,0);
	// glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, red);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, red.data());
	glPushMatrix();
	glTranslated(-0.5, 0.0, 0.0);
	glRotated((double)r / 4, 0.0, 1.0, 0.0);
	glBegin(GL_QUADS);
	for (int i = 0; i < 6; ++i) {
		glNormal3dv(normal[i]);
		for (int j = 0; j < 4; ++j){
			glVertex3dv(vertex[face[i][j]]);
		}
	}
	glPopMatrix();

	glEnd();
	if (++r >= 360 * 4){
		r = 0;
	}
}
