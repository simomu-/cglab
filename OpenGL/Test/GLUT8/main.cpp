#include <iostream>
#include <stdlib.h>
#include <GL/glut.h>
#include "Box.h"
#include "Light.h"

Box box;
Box box2;
Box box3;
Light light;

void Idle(){
	glutPostRedisplay();
}

void Display(){

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	light.Draw();

	glPushMatrix();
	box.Draw();
	glPopMatrix();

	glPushMatrix();
	glTranslated(2.0, 0.0, 0.0);
	box2.Draw();
	// box3.Draw();
	glPopMatrix();
	
	glPushMatrix();
	glTranslated(-2.0, 0.0, 0.0);
	box3.Draw();
	glPushMatrix();

	glutSwapBuffers();
	// glFlush();		
}

void Risize(int w, int h){
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-2.0, 2.0, -2.0, 2.0, -2.0, 2.0);

	gluPerspective(30.0, (double)w / (double)h, 1.0, 100.0);
	// glTranslated(0.0, 0.0, -5.0);

	gluLookAt(3.0, 4.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	glMatrixMode(GL_MODELVIEW);
}

void Init(){
	glClearColor(1.0,1.0,1.0,1.0);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glCullFace(GL_FRONT);

	light.Init();
}

void Keyboard(unsigned char key, int x, int y){

	switch(key){
		case 'q':
			exit(0);
		break;
	}

}


int main(int argc, char *argv[]){

	std::cout << "OpenGL : " << glGetString(GL_VERSION) << std::endl;
	glutInit(&argc,argv);
	glutInitWindowPosition(0,0);
	glutInitWindowSize(640,480);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(Display);
	glutReshapeFunc(Risize);
	glutKeyboardFunc(Keyboard);
	glutIdleFunc(Idle);
	Init();
	glutMainLoop();

	return 0;

}