
class Light{
	GLfloat light0pos[4] = { 0.0, 3.0, 5.0, 1.0 };
	GLfloat light1pos[4] = { 5.0, 3.0, 0.0, 1.0 };

	GLfloat green[4] = { 0.0, 1.0, 0.0, 1.0 };
public:
	Light(){};
	~Light(){};
	void Init();
	void Draw();
};