#pragma once
#include <array>

class Box{

	GLdouble vertex[8][3];

	int face[6][4];

	GLdouble normal[6][3];

	std::array<GLfloat, 4> red;

	int r = 0;


public:
	// Box() : red({0.8f, 0.2f, 0.2f, 1.0f}) {};
	Box();
	~Box(){};
	void Draw();
	
};