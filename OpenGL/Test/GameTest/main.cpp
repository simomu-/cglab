#include <GL/glut.h>
#include <iostream>
#include "Objects/Title.h"
#include "Objects/Light.h"
#include "Objects/PolygonObject.h"
#include "Objects/GlidPlane.h"
#include "Objects/Camera.h"
#include "Objects/Image.h"
#include "Utility/SceneManager.h"
#include "Utility/InputManager.h"
#include "Utility/UIManager.h"

// const float deltaTime = 0.02;

void Update(int value){

	static int counter;
	static float currentTime;

	counter++;

	if(counter % (int) (1 / SceneManager::GetInstance().GetUpdateDeltaTime()) == 0){
		// std::cout << "Update" << std::endl;
	}

	SceneManager::GetInstance().UpdateAllObjects();
	SceneManager::GetInstance().LateUpdateAllObjects();
	SceneManager::GetInstance().CheckDeleteAllObjects();

	glutPostRedisplay();
	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();
	currentTime += deltaTime;
	// UIManager::GetInstance().SetCurrentTime(currentTime);
	glutTimerFunc((int)(deltaTime * 1000.0), Update, 0);
}

void Display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	SceneManager::GetInstance().DrawAllObjects();
	UIManager::GetInstance().Draw();

	glutSwapBuffers();

}

void Idle(){
	glutPostRedisplay();
}

void KeyboardDown(unsigned char keyboard, int x, int y){
	// printf("%d\n", keyboard);
	switch(keyboard){
		case 'q':
		case 'Q':
		case '\033':
			exit(0);
			break;
		case 'a':
			InputManager::GetInstance().IsLeftButton = true;
			break;
		case 'd':
			InputManager::GetInstance().IsRightButton = true;
			break;
		case 'w':
			InputManager::GetInstance().IsUpButton = true;
			break;
		case 's':
			InputManager::GetInstance().IsDownButton = true;
			break;
		case 'z':
			InputManager::GetInstance().IsShootButton = true;
			break;
		case 13:
			InputManager::GetInstance().IsReturnKey = true;
			break;
		case '0':
			InputManager::GetInstance().IsDebugKey = true;
			break;
	}
}

void KeyboardUp(unsigned char keyboard, int x, int y){
	switch(keyboard){
		case 'a':
			InputManager::GetInstance().IsLeftButton = false;
			break;
		case 'd':
			InputManager::GetInstance().IsRightButton = false;
			break;
		case 'w':
			InputManager::GetInstance().IsUpButton = false;
			break;
		case 's':
			InputManager::GetInstance().IsDownButton = false;
			break;
		case 'z':
			InputManager::GetInstance().IsShootButton = false;
			break;
		case 13:
			InputManager::GetInstance().IsReturnKey = false;
			break;
		case '0':
			InputManager::GetInstance().IsDebugKey = false;
			break;
	}
}

void SpecialKeyboardDown(int keyboard, int x, int y){
	switch(keyboard){
		case GLUT_KEY_LEFT:
			InputManager::GetInstance().IsLeftButton = true;
			break;
		case GLUT_KEY_RIGHT:
			InputManager::GetInstance().IsRightButton = true;
			break;
		case GLUT_KEY_UP:
			InputManager::GetInstance().IsUpButton = true;
			break;
		case GLUT_KEY_DOWN:
			InputManager::GetInstance().IsDownButton = true;
			break;
	}
}

void SpecialKeyboardUp(int keyboard, int x, int y){
	switch(keyboard){
		case GLUT_KEY_LEFT:
			InputManager::GetInstance().IsLeftButton = false;
			break;
		case GLUT_KEY_RIGHT:
			InputManager::GetInstance().IsRightButton = false;
			break;
		case GLUT_KEY_UP:
			InputManager::GetInstance().IsUpButton = false;
			break;
		case GLUT_KEY_DOWN:
			InputManager::GetInstance().IsDownButton = false;
			break;
	}
}

void Init(){
	glClearColor(1.0, 1.0, 1.0, 1.0);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_LIGHTING);

	glEnable(GL_FOG);
	glFogi(GL_FOG_MODE , GL_LINEAR);
	glFogi(GL_FOG_START , 70);
	glFogi(GL_FOG_END , 100);
	float color[] = {1.0, 1.0, 1.0};
	glFogfv(GL_FOG_COLOR, color);

	Light* light = new Light(0);
	light->SetColor(1.0, 1.0, 1.0);
	light->SetPosition(5.0, 5.0, 5.0);

	GlidPlane* glidPlane = new GlidPlane(512, 5);
	glidPlane->SetPosition(0.0, -1.0, 0.0);

	PolygonObject* titleObject = new PolygonObject("Resource/Player1.obj");
	titleObject->SetPosition(0.0, 0.0, 0.0);

	Camera* camera = new Camera();
	camera->SetPosition(10.0, 5.0, 10.0);
	camera->SetTargetPosition(titleObject->GetPositionArray());
	
	Title* title = new Title();

	// Image* image = new Image("Resource/TestImage.bmp");
	Image* image1 = new Image("Resource/Title.bmp");
	image1->SetPixelZoom(5);
	image1->SetRasterPosition(1270 / 2, 720 / 8);

	Image* image2 = new Image("Resource/Title2.bmp");
	image2->SetPixelZoom(2.5);
	image2->SetRasterPosition(1270 / 2, 720 * 3.0/4.0);


	SceneManager::GetInstance().AddObject( light);
	SceneManager::GetInstance().AddObject( camera);
	SceneManager::GetInstance().AddObject( glidPlane);
	SceneManager::GetInstance().AddObject( titleObject);
	SceneManager::GetInstance().AddObject( title);

	SceneManager::GetInstance().AddImageObject( image1);
	SceneManager::GetInstance().AddImageObject( image2);
	// title->moveNextScene();


}

void Resize(int w, int h){
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (double)w / (double)h, 1.0, 100.0);
}

int main(int argc, char *argv[]){
	
	glutInit(&argc,argv);
	glutInitWindowSize(1270,720);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(Display);
	glutReshapeFunc(Resize);
	glutKeyboardFunc(KeyboardDown);
	glutKeyboardUpFunc(KeyboardUp);
	glutSpecialFunc(SpecialKeyboardDown);
	glutSpecialUpFunc(SpecialKeyboardUp);
	glutIgnoreKeyRepeat(GL_TRUE);

	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();
	glutTimerFunc((int)(deltaTime * 1000.0), Update, 0);

	Init();
	glutMainLoop();

	return 0;
}

