#include <GL/glut.h>
#include <iostream>
#include <vector>
#include "ObjectBase.h"
#include "PolygonObject.h"
#include "Light.h"
#include "GlidPlane.h"
#include "Camera.h"
#include "SceneManager.h"


const float deltaTime = 0.02f;
PolygonObject* polygonObject;

void Update(int value){

	static int counter;
	counter++;

	polygonObject->Rotate(0.0, -1.0, 0.0);

	glutPostRedisplay();
	glutTimerFunc((int)(deltaTime * 1000.0), Update, 0);
}

void Draw(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_NORMALIZE);

	SceneManager::GetInstance().DrawAllObjects();

	glutSwapBuffers();
}

void Resize(int w, int h){
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (double)w / (double)h, 1.0, 100.0);
}

void Init(){
	glClearColor(1.0, 1.0, 1.0, 1.0);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_LIGHTING);

	Camera* camera = new Camera();
	camera->SetPosition(7, 5, 7);
	float pos[] = {0.0, 0.0, 0.0, 0.0};
	camera->SetTargetPosition(pos);

	polygonObject->SetPosition(0.0, 1.0, 0.0);
	polygonObject->SetRotation(0.0, 180.0, 0.0);

	Light* light = new Light(0);
	light->SetColor(1.0, 1.0, 1.0);
	light->SetPosition(10.0, 10.0, 10.0);

	GlidPlane* glid = new GlidPlane(32);

	SceneManager::GetInstance().AddObject(camera);
	SceneManager::GetInstance().AddObject(polygonObject);
	SceneManager::GetInstance().AddObject(light);
	SceneManager::GetInstance().AddObject(glid);
}

int main(int argc, char *argv[]){
	
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(1280,720);
	glutCreateWindow(argv[0]);
	glutInitWindowPosition(0, 0);
	glutDisplayFunc(Draw);
	glutReshapeFunc(Resize);

	glutTimerFunc((int)(deltaTime * 1000.0), Update, 0);

	if(argc > 0){
		polygonObject = new PolygonObject(argv[1]);
	}else{
		std::cout << "ファイル名未指定" << std::endl;
		return 1;
	}

	Init();
	glutMainLoop();
	return 0;
}