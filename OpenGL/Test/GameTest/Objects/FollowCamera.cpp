#include "FollowCamera.h"
#include "Camera.h"
#include "ObjectBase.h"
#include <GL/glut.h>
#include <cmath>

FollowCamera::FollowCamera(ObjectBase &target) : targetObject(target){

}

void FollowCamera::SetTargetObject(ObjectBase &target){
	targetObject = target;
	SetTargetPosition(target.GetPositionArray());
}

void FollowCamera::SetRadius(float r){
	radius = r;
}

void FollowCamera::Update(){

	Camera::Update();
	float forward_x = sin(M_PI / 180.0 * targetObject.RotationY());//* cos(M_PI / 180.0 * rotation_x);
	float forward_z = cos(M_PI / 180.0 * targetObject.RotationY());

	float x = forward_x * radius;
	float z = forward_z * radius;

	SetPosition(targetObject.PositionX() - x ,position_y ,targetObject.PositionZ() - z);

}

void FollowCamera::Draw(){
	SetTargetPosition(targetObject.GetPositionArray());
	Camera::Draw();
}