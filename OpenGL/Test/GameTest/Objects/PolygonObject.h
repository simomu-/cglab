#pragma once
#include "ObjectBase.h"
#include <vector>
#include <array>
#include <string>
#include <cmath>

struct Face{

	int faceIndex0;
	int faceIndex1;
	int faceIndex2;

	float diffuseMaterialR;
	float diffuseMaterialG;
	float diffuseMaterialB;

	std::array<float , 4> materialArray;

	Face(int f0, int f1, int f2) : faceIndex0(f0) , faceIndex1(f1) , faceIndex2(f2) {
	}

	void SetMaterial(float r, float g, float b){
		diffuseMaterialR = r;
		diffuseMaterialG = g;
		diffuseMaterialB = b;
		materialArray[0] = r;
		materialArray[1] = g;
		materialArray[2] = b;
		materialArray[3] = 1.0f;
	}

	float* GetMaterialArray(){
		return materialArray.data();
	}

};


class PolygonObject : public ObjectBase{

	std::string mtlFileName;

	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<Face> faces;
	std::vector<int> faceNormals;

	void SetVertex(char dataLine[]);
	void SetFaces(char dataLine[], float r, float g, float b);
	void SetMaterial(std::string materialName, float* r, float* g, float* b);

protected:
	bool isVisible;

public:
	PolygonObject();
	PolygonObject(std::string objFileName);
	void LoadObjFile(std::string objFileName);
	void Draw();
	void Update();

	static float* GetNormalOfPolygon(float v0[], float v1[], float v2[]);
};
