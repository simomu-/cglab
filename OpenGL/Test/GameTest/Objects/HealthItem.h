#pragma once
#include "PolygonObject.h"
#include "../Utility/SphereCollider.h"

class HealthItem : public PolygonObject{

	SphereCollider collider;

public:
	HealthItem();
	~HealthItem();

	void Draw();
	void Update();	
	void LateUpdate();
	void Delete();
};