#pragma once
#include "PolygonObject.h"
#include "../Utility/SphereCollider.h"
#include <random>

class Enemy : public PolygonObject{
private:
	float speed;
	float rotateSpeed;
	float rotateTime;
	float rotateInterval;
	float currentTime;
	float life;
	float shootInterval;
	bool isRotate;

	float beginRotateTime;
	float nextRotateTime;
	float nextShootTime;
	SphereCollider collider;

public:
	Enemy();
	~Enemy();
	void Draw();
	void Update();
	void LateUpdate();
	void Delete();
};