#include "PolygonObject.h"
#include <GL/glut.h>
#include <fstream>
#include <iostream>
#include <string>

PolygonObject::PolygonObject(std::string objFileName) : isVisible(true){
	if(!objFileName.empty()){
		LoadObjFile(objFileName);
	}
}

void PolygonObject::LoadObjFile(std::string objFileName){

	vertices.clear();
	normals.clear();
	faces.clear();
	faceNormals.clear();

	std::ifstream file;
	char buf[256];
	std::string materialName;
	float r = 0.8;
	float g = 0.8;
	float b = 0.8;

	std::cout << "File Name " << objFileName << std::endl;
	file.open(objFileName.c_str());
	if(file.fail()){
		std::cout << objFileName << " can not open" << std::endl;
		exit(0);
	}
	while(!file.eof()){
		file.getline(buf,sizeof(buf));
		if(buf[0] == 'v'){
			SetVertex(buf);
		}
		if(buf[0] == 'f'){
			SetFaces(buf, r, g, b);
		}
		if(buf[0] == 'u'){;
			materialName = buf + 7;
			SetMaterial(materialName, &r, &g, &b);
		}
		if(buf[0] == 'm'){
			mtlFileName = "Resource/";
			mtlFileName += buf + 7;
		}
	}
	file.close();
	std::cout << "vertices " << vertices.size() / 3 << std::endl;
	std::cout << "faces " << faces.size() << std::endl;

}

void PolygonObject::SetVertex(char dataLine[]){
	
	float x;
	float y;
	float z;
	sscanf(dataLine + 2, "%f%f%f",&x,&y,&z);

	switch(dataLine[1]){
		case ' ':
			vertices.push_back(x);
			vertices.push_back(y);
			vertices.push_back(z);
			break;
		case 'v':
			normals.push_back(x);
			normals.push_back(y);
			normals.push_back(z);
			break;
	}
}

void PolygonObject::SetFaces(char dataLine[],float r, float g, float b){
	int v0;
	int v1;
	int v2;
	int n0;
	int n1;
	int n2;
	sscanf(dataLine + 2, "%d//%d %d//%d %d//%d ", &v0, &n0, &v1, &n1, &v2, &n2);
	Face f(v0, v1, v2);
	f.SetMaterial(r, g, b);
	faces.push_back(f);
}

void PolygonObject::SetMaterial(std::string materialName, float* r, float* g, float* b){
	
	std::ifstream file;
	char buf[256];

	file.open(mtlFileName.c_str());
	if(file.fail()){
		std::cout << mtlFileName << " can not open" << std::endl;
		exit(0);
	}
	while(!file.eof()){
		file.getline(buf,sizeof(buf));
		if((buf + 7) == materialName){
			file.getline(buf,sizeof(buf));
			file.getline(buf,sizeof(buf));
			file.getline(buf,sizeof(buf));
			sscanf(buf + 3, "%f %f %f", r, g, b);
			break;
		}
	}
}

void PolygonObject::Draw(){
	
	glPushMatrix();

	glTranslated(position_x, position_y, position_z);
	glRotated(rotation_x, 1.0, 0.0, 0.0);
	glRotated(rotation_y, 0.0, 1.0, 0.0);
	glRotated(rotation_z, 0.0, 0.0, 1.0);
	glTranslated(-position_x, -position_y, -position_z);

	glTranslated(position_x, position_y, position_z);

	if(isVisible){
		glBegin(GL_TRIANGLES);
		for(int i = 0; i < faces.size(); i++){

			int f0 = faces[i].faceIndex0 - 1;
			int f1 = faces[i].faceIndex1 - 1;
			int f2 = faces[i].faceIndex2 - 1;

			float v0[] = {vertices[f0 * 3], vertices[f0 * 3 + 1], vertices[f0 * 3 + 2]};
			float v1[] = {vertices[f1 * 3], vertices[f1 * 3 + 1], vertices[f1 * 3 + 2]};
			float v2[] = {vertices[f2 * 3], vertices[f2 * 3 + 1], vertices[f2 * 3 + 2]};

			float* n = GetNormalOfPolygon(v0, v1, v2);

			glMaterialfv(GL_FRONT, GL_DIFFUSE, faces[i].GetMaterialArray());
			glNormal3fv(n);
			glVertex3fv(v0);
			glVertex3fv(v1);
			glVertex3fv(v2);

		}
		glEnd();
	}

	glPopMatrix();

}

void PolygonObject::Update(){

}

float* PolygonObject::GetNormalOfPolygon(float v0[], float v1[], float v2[]){

		float va[3] = {v1[0] - v0[0], v1[1] - v0[1], v1[2] - v0[2]};
		float vb[3] = {v2[0] - v0[0], v2[1] - v0[1], v2[2] - v0[2]};

		float* normal = new float[3];
		normal[0] = va[1] * vb[2] - va[2] * vb[1];
		normal[1] = va[2] * vb[0] - va[0] * vb[2];
		normal[2] = va[0] * vb[1] - va[1] * vb[0];

		float length = sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
		for(int i = 0; i < 3; i++){
			normal[i] /= length;
		}

		return normal;

}