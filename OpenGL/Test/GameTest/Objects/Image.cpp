#include "Image.h"
#include "../Utility/BmpPicture.h"
#include <GL/glut.h>
#include <iostream>

Image::Image(std::string fileName) : rasterPositionX(0), 
									 rasterPositionY(0), 
									 pixelZoom(1),
									 picture(fileName){
	// glEnable( GL_TEXTURE_2D );
	// glGenTextures( 1, &texture );
	// glBindTexture( GL_TEXTURE_2D, texture );
	// glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	// glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	// glTexImage2D( GL_TEXTURE_2D, 0, 3, picture.GetWitdth(), picture.GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, picture.GetColorArray() );
	// glBindTexture( GL_TEXTURE_2D, 0 );
}

Image::~Image(){

}

void Image::Draw(){
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 1270, 720, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glPixelZoom(pixelZoom, pixelZoom);
	glRasterPos2f(rasterPositionX, rasterPositionY);
	glDrawPixels(picture.GetWitdth(),picture.GetHeight(),GL_RGB,GL_UNSIGNED_BYTE,picture.GetColorArray());
	glEnable(GL_DEPTH_TEST);
	glRasterPos2f(0, 0);

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

}

void Image::SetRasterPosition(float x, float y){
	rasterPositionX = x - (picture.GetWitdth() * pixelZoom)/ 2.0;
	rasterPositionY = y + (picture.GetHeight() * pixelZoom) / 2.0;
}

void Image::SetPixelZoom(float zoom){
	pixelZoom = zoom;
}