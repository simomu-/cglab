#include "ObjectBase.h"
#include "../Utility/SceneManager.h"
#include <cmath>
#include <iostream>

ObjectBase::ObjectBase() :  position_x(0),position_y(0),position_z(0),
							rotation_x(0),rotation_y(0),rotation_z(0){
	positionArray = {position_x, position_y, position_z, 1.0};
	isDelete = false;
};

ObjectBase::~ObjectBase(){
}

float ObjectBase::PositionX() const{
	return position_x;
}

float ObjectBase::PositionY() const{
	return position_y;
}

float ObjectBase::PositionZ() const{
	return position_z;
}

float* ObjectBase::GetPositionArray(){
	float* array = new float[3];
	array[0] = position_x;
	array[1] = position_y;
	array[2] = position_z;
	return array;
}

void ObjectBase::SetPosition(float x, float y, float z){
	position_x = x;
	position_y = y;
	position_z = z;
	positionArray[0] = position_x;
	positionArray[1] = position_y;
	positionArray[3] = position_z;
}

void ObjectBase::Translate(float x, float y, float z){
	position_x += x;
	position_y += y;
	position_z += z;
	positionArray[0] = position_x;
	positionArray[1] = position_y;
	positionArray[3] = position_z;
}

void ObjectBase::MoveForward(float v){	
	float forward_x = sin(M_PI / 180.0 * rotation_y);//* cos(M_PI / 180.0 * rotation_x);
	float forward_y = sin(M_PI / 180.0 * rotation_y); //* sin(M_PI / 180.0 * rotation_x);
	float forward_z = cos(M_PI / 180.0 * rotation_y);

	float x = forward_x * v;
	float z = forward_z * v;

	Translate(x, 0, z);
}

float ObjectBase::RotationX() const{
	return rotation_x;
}

float ObjectBase::RotationY() const{
	return rotation_y;
}

float ObjectBase::RotationZ() const{
	return rotation_z;
}

void ObjectBase::SetRotation(float x, float y, float z){
	rotation_x = x;
	rotation_y = y;
	rotation_z = z;
}

void ObjectBase::Rotate(float x, float y, float z){
	rotation_x += x;
	rotation_y += y;
	rotation_z += z;
}

void ObjectBase::Update(){};
void ObjectBase::LateUpdate(){};
void ObjectBase::Draw(){};
void ObjectBase::CheckDelete(){
	if(isDelete){
		Delete();
	}
}
void ObjectBase::Delete(){
	SceneManager::GetInstance().RemoveObject(this);
}