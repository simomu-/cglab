#pragma once
#include "ObjectBase.h"
#include <array>

class Cube : public ObjectBase{
	float size;
	std::array<float,4> colorArray;
public:
	Cube();
	void SetColor(float r, float g, float b);
	void Draw();
	
};