#pragma once
#include "ObjectBase.h"
#include "../Utility/SphereCollider.h"
#include <array>

class Bullet : public ObjectBase{
private:
	float speed;
	float size;
	float lifeTime;
	std::array<float, 4> diffuseArray;
	std::array<float, 4> emissionArray;

	SphereCollider collider;

public:
	Bullet();
	Bullet(float speed);
	~Bullet();

	void Draw();
	void Update();
	void LateUpdate();
	void Delete();
};