#pragma once

class InputManager{
private:
	InputManager();
	~InputManager() = default;
	
public:
	InputManager(const InputManager&) = delete;
	InputManager& operator=(const InputManager&) = delete;
    InputManager(InputManager&&) = delete;
    InputManager& operator=(InputManager&&) = delete;

    static InputManager& GetInstance() {
        static InputManager inst;
        return inst;
    }

	bool IsRightButton;
	bool IsLeftButton;
	bool IsUpButton;
	bool IsDownButton; 

	bool IsShootButton;

	bool IsReturnKey;

	bool IsDebugKey;
};