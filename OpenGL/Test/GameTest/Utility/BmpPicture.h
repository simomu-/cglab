#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

class BmpPicture{

	long width;
	long height;
	char* colorData;
	int colorDataSize;

public:
	BmpPicture(std::string fileName);
	~BmpPicture();

	long GetWitdth() const;
	long GetHeight() const;
	int GetColorDataSize() const;
	char* GetColorArray();
	
};