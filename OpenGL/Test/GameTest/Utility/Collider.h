#pragma once
#include <string>

class Collider{
protected:
	std::string nameTag;

	static bool isDebug;

public:
	Collider();
	Collider(std::string name);
	~Collider() = default;

	virtual void DrawForDebug();

	std::string GetNameTag();
};