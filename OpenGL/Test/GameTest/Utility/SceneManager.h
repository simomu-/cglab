#pragma once
#include "../Objects/ObjectBase.h"
#include "../Objects/Image.h"
#include "SphereCollider.h"
#include <vector>
#include <string>

class SceneManager{
private:
	SceneManager();
	~SceneManager() = default;
	std::vector<ObjectBase*> sceneObjects;
	std::vector<Image*> imageObjects;
	std::vector<ObjectBase*> nextSceneObjects;
	std::vector<Image*> nextSceneImageObjects;
	std::vector<SphereCollider*> colliderObjects;
	const float deltaTime;
	
public:
	SceneManager(const SceneManager&) = delete;
	SceneManager& operator=(const SceneManager&) = delete;
    SceneManager(SceneManager&&) = delete;
    SceneManager& operator=(SceneManager&&) = delete;

    static SceneManager& GetInstance() {
        static SceneManager inst;
        return inst;
    }

	void AddObject(ObjectBase *object);
	void AddImageObject(Image *image);
	void AddNextSceneObject(ObjectBase *object);
	void AddNextSceneImageObject(Image *image);

	void RemoveObject(ObjectBase *object);
	void RemoveImageObject(Image *image);
	void DrawAllObjects();
	void UpdateAllObjects();
	void LateUpdateAllObjects();
	void CheckDeleteAllObjects();

	void AddCollider(SphereCollider* collider);
	void RemoveCollider(SphereCollider* collider);

	bool IsHit(SphereCollider* collider) const;
	bool IsHit(SphereCollider* collider, std::string name) const;

	void SceneChange();

	float GetUpdateDeltaTime() const;
};