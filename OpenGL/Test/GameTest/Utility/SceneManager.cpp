#include "SceneManager.h"
#include "../Objects/ObjectBase.h"
#include "UIManager.h"
#include <algorithm>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

SceneManager::SceneManager() : deltaTime(0.02){
};

void SceneManager::AddObject(ObjectBase *object){
	sceneObjects.push_back(object);
}

void SceneManager::AddImageObject(Image *image){
	imageObjects.push_back(image);
}

void SceneManager::AddNextSceneObject(ObjectBase *object){
	nextSceneObjects.push_back(object);
}

void SceneManager::AddNextSceneImageObject(Image *image){
	nextSceneImageObjects.push_back(image);
}

void SceneManager::RemoveObject(ObjectBase *object){

    auto itr = std::find(sceneObjects.begin(), sceneObjects.end(), object);
	delete (*itr);
	sceneObjects.erase(itr);
	sceneObjects.shrink_to_fit();
}

void SceneManager::RemoveImageObject(Image *image){

    auto itr = std::find(imageObjects.begin(), imageObjects.end(), image);
	delete (*itr);
	imageObjects.erase(itr);
	imageObjects.shrink_to_fit();
}

void SceneManager::DrawAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->Draw();
	}
	for(int i = 0; i < imageObjects.size(); i++){
		imageObjects[i]->Draw();
	}
}

void SceneManager::UpdateAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->Update();
	}

	for(int i = 0; i < imageObjects.size(); i++){
		imageObjects[i]->Update();
	}
}

void SceneManager::LateUpdateAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->LateUpdate();
	}
	for(int i = 0; i < imageObjects.size(); i++){
		imageObjects[i]->LateUpdate();
	}
}

void SceneManager::CheckDeleteAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->CheckDelete();
	}

	for(int i = 0; i < imageObjects.size(); i++){
		imageObjects[i]->CheckDelete();
	}
}

void SceneManager::AddCollider(SphereCollider* collider){
	colliderObjects.push_back(collider);
}

void SceneManager::RemoveCollider(SphereCollider* collider){
    auto itr = std::find(colliderObjects.begin(), colliderObjects.end(), collider);
	// delete (*itr); 
	colliderObjects.erase(itr);
	colliderObjects.shrink_to_fit();
}

bool SceneManager::IsHit(SphereCollider* collider) const{

	float x0, y0, z0, r0;
	collider->GetSphereColliderInfo(x0, y0, z0, r0);

	for(int i = 0; i < colliderObjects.size(); i++){
		if(collider != colliderObjects[i]){
			float x1, y1, z1, r1;
			colliderObjects[i]->GetSphereColliderInfo(x1, y1, z1, r1);
			float d = sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0) + (z1 - z0) * (z1 - z0));
			if(d <= (r0 + r1)){
				return true;
			}
		}
	}

	return false;
}

bool SceneManager::IsHit(SphereCollider* collider, std::string name) const{
	
	float x0, y0, z0, r0;
	collider->GetSphereColliderInfo(x0, y0, z0, r0);

	for(int i = 0; i < colliderObjects.size(); i++){
		if(collider != colliderObjects[i]){
			float x1, y1, z1, r1;
			colliderObjects[i]->GetSphereColliderInfo(x1, y1, z1, r1);
			float d = sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0) + (z1 - z0) * (z1 - z0));
			if(d <= (r0 + r1) && name == colliderObjects[i]->GetNameTag()){
				return true;
			}
		}
	}

	return false;
}

void SceneManager::SceneChange(){
	UIManager::GetInstance().ClearAllText();
	for(int i = 0; i < sceneObjects.size(); i++){
		delete sceneObjects[i];
	}
	sceneObjects.clear();
	sceneObjects.shrink_to_fit();
	sceneObjects = nextSceneObjects;
	nextSceneObjects.clear();
	nextSceneObjects.shrink_to_fit();

	for(int i = 0; i < imageObjects.size(); i++){
		delete imageObjects[i];
	}
	imageObjects.clear();
	imageObjects.shrink_to_fit();
	imageObjects = nextSceneImageObjects;
	nextSceneImageObjects.clear();
	nextSceneObjects.shrink_to_fit();
}

float SceneManager::GetUpdateDeltaTime() const{
	return deltaTime;
}