#pragma once
#include <string>

class UIManager{
private:
	std::string currentTimeText;
    std::string currentHealthText;

	UIManager();
	~UIManager() = default;

	void DrawString(std::string str, int w, int h, int x0, int y0);
	
public:
	UIManager(const UIManager&) = delete;
	UIManager& operator=(const UIManager&) = delete;
    UIManager(UIManager&&) = delete;
    UIManager& operator=(UIManager&&) = delete;

    static UIManager& GetInstance() {
        static UIManager inst;
        return inst;
    }

    void SetHealth(float h);
    void SetCurrentTime(float time);
    void ClearAllText();

    void Draw();

};