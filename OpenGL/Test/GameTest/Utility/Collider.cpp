#include "Collider.h"
#include <string>

Collider::Collider() : nameTag(""){
	
}

Collider::Collider(std::string name): nameTag(name){

}

bool Collider::isDebug(false);

void Collider::DrawForDebug(){}

std::string Collider::GetNameTag(){
	return nameTag;
}