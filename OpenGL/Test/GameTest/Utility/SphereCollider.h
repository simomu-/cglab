#pragma once
#include "Collider.h"
#include <string>

class SphereCollider : public Collider{
private:
	float position_x;
	float position_y;
	float position_z;
	float radius;
public:
	SphereCollider(float r);
	SphereCollider(float r, std::string name);
	~SphereCollider();

	void Delete();
	void GetSphereColliderInfo(float &x, float &y, float &z, float &r);
	void SetPosition(float x, float y, float z);	

	void DrawForDebug();
};