//#pragma once
#include <GL/glut.h>
#include <stdio.h>
#include "Display.h"

void Init(){
	glClearColor(0.0,0.0,0.0,1.0);
}

void Mouce(int button, int state, int x, int y){

	if(state == GLUT_DOWN){
		switch(button){
			case GLUT_LEFT_BUTTON:
				printf("Left\n");
				break;
			case GLUT_MIDDLE_BUTTON:
				printf("Middle\n");
				break;
			case GLUT_RIGHT_BUTTON:
				printf("Right\n");
				break;
		}
	}
}

void Keybord(){
	
}

void Resize(int w, int h){
	/* ウィンドウ全体をビューポートにする */
	glViewport(0, 0, w, h);

	/* 変換行列の初期化 */
	glLoadIdentity();

	/* スクリーン上の表示領域をビューポートの大きさに比例させる */
	glOrtho(-w / 200.0, w / 200.0, -h / 200.0, h / 200.0, -1.0, 1.0);
}

int main(int argc, char* argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(Display);
	glutReshapeFunc(Resize);
	glutMouseFunc(Mouce);
	Init();
	glutMainLoop();
	return 0;
}