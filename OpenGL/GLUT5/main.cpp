//#pragma once
#include <GL/glut.h>
#include "Display.h"

void Init(){
	glClearColor(0.0,0.0,0.0,1.0);
}

int main(int argc, char* argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(Display);
	Init();
	glutMainLoop();
	return 0;
}