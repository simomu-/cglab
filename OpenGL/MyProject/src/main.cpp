#include <GL/glut.h>
#include <iostream>
#include <Manager/SceneManager.h>
#include <Manager/InputManager.h>
#include <Manager/UIManager.h>
#include <Utility/DisplaySetting.h>
#include <Initialize.h>

void Update(int value){

	static int counter;
	static float currentTime;

	counter++;

	if(counter % (int) (1 / SceneManager::GetInstance().GetUpdateDeltaTime()) == 0){
		// std::cout << "Update" << std::endl;
	}

	SceneManager::GetInstance().UpdateAllObjects();
	SceneManager::GetInstance().UpdateAllObjects2D();
	SceneManager::GetInstance().LateUpdateAllObjects();
	SceneManager::GetInstance().LateUpdateAllObjects2D();
	SceneManager::GetInstance().CheckDeleteAllObjects();

	glutPostRedisplay();
	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();
	currentTime += deltaTime;
	// UIManager::GetInstance().SetCurrentTime(currentTime);
	glutTimerFunc((int)(deltaTime * 1000.0), Update, 0);
}

void Display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float aspect = (float)DisplaySetting::GetWidth() / (float)DisplaySetting::GetHeight();
	gluPerspective(DisplaySetting::GetFieldOfView(), aspect, 1.0, 100.0);
	glViewport(0, 0, DisplaySetting::GetWidth(), DisplaySetting::GetHeight());
	glMatrixMode(GL_MODELVIEW);
	SceneManager::GetInstance().DrawAllObjects();

	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// gluOrtho2D(0, DisplaySetting::GetWidth(), DisplaySetting::GetHeight(), 0);
	glOrtho(0, DisplaySetting::GetWidth(), DisplaySetting::GetHeight(), 0, 0, 10000);
	glViewport(0, 0, DisplaySetting::GetWidth(), DisplaySetting::GetHeight());
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	SceneManager::GetInstance().DrawAllObjects2D();

	// UIManager::GetInstance().Draw();

	glutSwapBuffers();

}

void Idle(){
	glutPostRedisplay();
}

void KeyboardDown(unsigned char keyboard, int x, int y){
	// printf("%d\n", keyboard);
	switch(keyboard){
		case 'q':
		case 'Q':
		case '\033':
			exit(0);
			break;
		case 'a':
			InputManager::GetInstance().IsLeftButton = true;
			break;
		case 'd':
			InputManager::GetInstance().IsRightButton = true;
			break;
		case 'w':
			InputManager::GetInstance().IsUpButton = true;
			break;
		case 's':
			InputManager::GetInstance().IsDownButton = true;
			break;
		case 'z':
			InputManager::GetInstance().IsShootButton = true;
			break;
		case 13:
			InputManager::GetInstance().IsReturnKey = true;
			break;
		case '0':
			InputManager::GetInstance().IsDebugKey = true;
			break;
	}
}

void KeyboardUp(unsigned char keyboard, int x, int y){
	switch(keyboard){
		case 'a':
			InputManager::GetInstance().IsLeftButton = false;
			break;
		case 'd':
			InputManager::GetInstance().IsRightButton = false;
			break;
		case 'w':
			InputManager::GetInstance().IsUpButton = false;
			break;
		case 's':
			InputManager::GetInstance().IsDownButton = false;
			break;
		case 'z':
			InputManager::GetInstance().IsShootButton = false;
			break;
		case 13:
			InputManager::GetInstance().IsReturnKey = false;
			break;
		case '0':
			InputManager::GetInstance().IsDebugKey = false;
			break;
	}
}

void SpecialKeyboardDown(int keyboard, int x, int y){
	switch(keyboard){
		case GLUT_KEY_LEFT:
			InputManager::GetInstance().IsLeftButton = true;
			break;
		case GLUT_KEY_RIGHT:
			InputManager::GetInstance().IsRightButton = true;
			break;
		case GLUT_KEY_UP:
			InputManager::GetInstance().IsUpButton = true;
			break;
		case GLUT_KEY_DOWN:
			InputManager::GetInstance().IsDownButton = true;
			break;
	}
}

void SpecialKeyboardUp(int keyboard, int x, int y){
	switch(keyboard){
		case GLUT_KEY_LEFT:
			InputManager::GetInstance().IsLeftButton = false;
			break;
		case GLUT_KEY_RIGHT:
			InputManager::GetInstance().IsRightButton = false;
			break;
		case GLUT_KEY_UP:
			InputManager::GetInstance().IsUpButton = false;
			break;
		case GLUT_KEY_DOWN:
			InputManager::GetInstance().IsDownButton = false;
			break;
	}
}

void Init(){
	glClearColor(0.4, 0.4, 0.4, 1.0);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_LIGHTING);

	glEnable(GL_FOG);
	glFogi(GL_FOG_MODE , GL_LINEAR);
	glFogi(GL_FOG_START , 70);
	glFogi(GL_FOG_END , 100);
	float color[] = {1.0, 1.0, 1.0};
	glFogfv(GL_FOG_COLOR, color);

	Initialize();

}

void Resize(int w, int h){
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(DisplaySetting::GetFieldOfView(), (double)w / (double)h, 1.0, 100.0);
}

int main(int argc, char *argv[]){
	
	glutInit(&argc,argv);
	glutInitWindowSize(DisplaySetting::GetWidth(),DisplaySetting::GetHeight());
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(Display);
	// glutReshapeFunc(Resize);
	glutKeyboardFunc(KeyboardDown);
	glutKeyboardUpFunc(KeyboardUp);
	glutSpecialFunc(SpecialKeyboardDown);
	glutSpecialUpFunc(SpecialKeyboardUp);
	glutIgnoreKeyRepeat(GL_TRUE);

	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();
	glutTimerFunc((int)(deltaTime * 1000.0), Update, 0);

	Init();
	glutMainLoop();

	return 0;
}

