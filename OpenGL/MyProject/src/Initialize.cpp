#include <Manager/SceneManager.h>
#include <Objects/Light.h>
#include <Objects/Camera.h>
#include <Objects/GlidPlane.h>
#include <TestObject.h>
#include <TestObject2D.h>

void Initialize(){

	Camera* camera = new Camera();
	camera->SetPosition(0, 30, -30);
	camera->SetRotation(45, 0, 0);

	Light* light = new Light(0);
	light->SetColor(1.0, 1.0, 1.0);
	light->SetPosition(1, 30, 1);

	GlidPlane* plane = new GlidPlane(256,1);

	TestObject* test = new TestObject();
	SceneManager::GetInstance().AddObject(camera);
	SceneManager::GetInstance().AddObject(light);
	SceneManager::GetInstance().AddObject(plane);
	SceneManager::GetInstance().AddObject(test);
}