#include <Utility/BmpPicture.h>
#include <iostream>
#include <fstream>
#include <string>

BmpPicture::BmpPicture(std::string fileName) : width(0), height(0){

	std::ifstream file( fileName.c_str(), std::ios::in | std::ios::binary );

	if (!file){
		std::cout << "Error" << std::endl;
	}

	file.seekg(18,std::ios_base::cur);
	file.read((char *)&width, 4);
	file.read((char *)&height, 4);
	file.seekg(28,std::ios_base::cur);

	colorDataSize = width * height * 3;
	colorData = new char[colorDataSize];
	file.read(colorData, colorDataSize);

	for(int i = 0; i < colorDataSize; i += 3){
		char tmp = colorData[i];
		colorData[i] = colorData[i + 2];
		colorData[i + 2] = tmp;
	}
}


BmpPicture::~BmpPicture(){
	delete[] colorData;
}

long BmpPicture::GetWidth() const{
	return width;
}

long BmpPicture::GetHeight() const{
	return height;
}

int BmpPicture::GetColorDataSize() const{
	return colorDataSize;
}

char* BmpPicture::GetColorArray(){
	return colorData;
}
