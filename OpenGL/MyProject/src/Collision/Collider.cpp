#include <Collision/Collider.h>
#include <string>

Collider::Collider() : nameTag(""){
	
}

Collider::Collider(std::string name): nameTag(name){

}

bool Collider::isDebug(true);

void Collider::SetPosition(float x, float y, float z){
	position.Set(x, y, z);
}

void Collider::SetPosition(Vector3 v){
	position = v;
}

Vector3 Collider::GetPosition() const{
	return position;
}

void Collider::DrawForDebug(){}

ColliderType Collider::GetColliderType() const{ 
}

std::string Collider::GetNameTag(){
	return nameTag;
}