#include <Collision/CapsuleAndCapsule.h>
#include <algorithm>

bool CapsuleAndCapsule::IsCollision(Collider* c1, Collider* c2){
	CapsuleCollider* collider1 = static_cast<CapsuleCollider*>(c1);
	CapsuleCollider* collider2 = static_cast<CapsuleCollider*>(c2);
	float d = GetSegmentSegmentDistance(*collider1, *collider2);

	float r = collider1->GetRadius() + collider2->GetRadius();

	if((r * r) >= d){
		return true;
	}
	return false;
}

float CapsuleAndCapsule::GetPointLineDistance(Vector3 p, CapsuleCollider l){
	float len = l.GetDirection().Length();
	float t = l.GetDirection().Dot(p - l.GetPosition()) / len;
	Vector3 h;
	h = l.GetPosition() + l.GetDirection() * t;
	Vector3 result = (h - p);
	return result.Length();
	// return (h - p).Length();
}

float CapsuleAndCapsule::GetPointSegmentDistance(Vector3 p, CapsuleCollider c){
	Vector3 v1 = p - c.GetStartPoint();
	Vector3 v2 = c.GetEndPoint() - c.GetStartPoint();
	float v1v2 = v1.Dot(v2);
	if(v1v2 <= 0.0f){
		return v1.Length2();
	}
	float v2v2 = v2.Length2();
	if(v2v2 <= v1v2){
		Vector3 v3 = p - c.GetEndPoint();
		return v3.Length2();
	}

	return v1.Length2() - v1v2*v1v2 / v2v2;

}

float CapsuleAndCapsule::GetSegmentSegmentDistance(CapsuleCollider c1, CapsuleCollider c2){
	Vector3 v1 = c1.GetEndPoint() - c1.GetStartPoint();
	Vector3 v2 = c2.GetEndPoint() - c2.GetStartPoint();
	Vector3 n = v1.Cross(v2);
	if(n.Length2()){
		Vector3 v12 = c2.GetStartPoint() - c1.GetStartPoint();
		float nv12 = n.Dot(v12);
		Vector3 vd = n * (nv12 / n.Length2());
		Vector3 q1 = c2.GetStartPoint() - vd;
		Vector3 q2 = c2.GetEndPoint() - vd;
		Vector3 p1q1 = q1 - c1.GetStartPoint();
		Vector3 p1q2 = q2 - c1.GetStartPoint();
		Vector3 r1 = v1.Cross(p1q1);
		Vector3 r2 = v1.Cross(p1q2);

		if(r1.Dot(r2) < 0){
			Vector3 v3 = q2 - q1;
			Vector3 q1p1 = c1.GetStartPoint() - q1;
			Vector3 q1p2 = c1.GetEndPoint() - q1;
			r1 = v3.Cross(q1p1);
			r2 = v3.Cross(q1p2);
			if(r1.Dot(r2) < 0){
				return nv12 * nv12 / n.Length2();
			}
		}

	}

	return std::min( 
				std::min(GetPointSegmentDistance(c1.GetStartPoint(), c2), 
						GetPointSegmentDistance(c1.GetEndPoint(), c2)), 
				std::min(GetPointSegmentDistance(c2.GetStartPoint(), c1), 
						GetPointSegmentDistance(c2.GetEndPoint(), c1))
			);
}




