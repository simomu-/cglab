#include <Collision/CylinderCollider.h>

CylinderCollider::CylinderCollider(float radius, float length, Vector3 dir) : 
									radius(radius), length(length){
	direction = dir;
}

void CylinderCollider::Delete(){

}

void CylinderCollider::GetCylinderColliderInfo(float &r, float &l, Vector3 &dir){
	r = radius;
	l = length;
	dir = direction;
}

void CylinderCollider::SetPosition(float x, float y, float z){
	position.Set(x, y, z);
}

void CylinderCollider::SetDirection(Vector3 dir){
	direction = dir;
}

void CylinderCollider::DrawForDebug(){

}
