#include <Collision/FlexibleCollision.h>
#include <Collision/SphereAndSphere.h>
#include <Collision/CapsuleAndCapsule.h>
#include <Collision/SphereAndCapsule.h>
#include <iostream>

FlexibleCollision::FlexibleCollision(){	
	collisionArray[SPHERE][SPHERE]		= new SphereAndSphere();
	collisionArray[SPHERE][CAPSULE]		= new SphereAndCapsule();
	collisionArray[CAPSULE][SPHERE]		= new SphereAndCapsule();
	collisionArray[CAPSULE][CAPSULE]	= new CapsuleAndCapsule();
}

FlexibleCollision::~FlexibleCollision(){
	for(int i = 0; i < COLLIDER_TYPE_COUNT; i++){
		for(int j = 0; j < COLLIDER_TYPE_COUNT; j++){
			delete collisionArray[i][j];
		}
	}
}

bool FlexibleCollision::IsCollision(Collider* c1, Collider* c2){
	return collisionArray[c1->GetColliderType()][c2->GetColliderType()]->IsCollision(c1, c2);
}