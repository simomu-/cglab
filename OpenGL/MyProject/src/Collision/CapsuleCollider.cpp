#include <Collision/CapsuleCollider.h>
#include <Manager/SceneManager.h>
#include <GL/glut.h>
#include <iostream>

CapsuleCollider::CapsuleCollider(float r, float l) : radius(r), length(l){
	colliderType = CAPSULE;
	SceneManager::GetInstance().AddCollider(this);
}

Vector3 CapsuleCollider::GetDirection() const{
	return direction;
}

Vector3 CapsuleCollider::GetStartPoint() const{
	Vector3 p = position + direction * (length / -2.0);
	return p;
}

Vector3 CapsuleCollider::GetEndPoint() const{
	Vector3 p = position + direction * (length / 2.0);
	return p;
}

float CapsuleCollider::GetRadius() const{
	return radius;
}

void CapsuleCollider::GetColliderInfo(float& r, float& l, Vector3& d){
	r = radius;
	l = length;
	d = direction;
}

ColliderType CapsuleCollider::GetColliderType() const{
	return colliderType;
}

void CapsuleCollider::SetDirection(Vector3 d){
	direction = d;
}

void CapsuleCollider::Delete(){
	SceneManager::GetInstance().RemoveCollider(this);
}

void CapsuleCollider::DrawForDebug(){
	if(isDebug){
		Collider::DrawForDebug();
		glPushMatrix();
		glDisable(GL_LIGHTING);
		glColor3f(0.0f, 1.0f, 0.0f);
		glTranslatef(GetStartPoint().x(), GetStartPoint().y(), GetStartPoint().z());
		glutWireSphere(radius, 20, 20);
		glPopMatrix();
		
		glPushMatrix();
		glTranslatef(GetEndPoint().x(), GetEndPoint().y(), GetEndPoint().z());
		glutWireSphere(radius, 20, 20);
		glColor3f(0.0f, 0.0f, 0.0f);
		glEnable(GL_LIGHTING);
		glPopMatrix();
	}
}