#include <Collision/SphereCollider.h>
#include <Manager/SceneManager.h>
#include <iostream>
#include <string>
#include <GL/glut.h>

SphereCollider::SphereCollider(float r) : radius(r){
	colliderType = SPHERE;
	SceneManager::GetInstance().AddCollider(this);
}

SphereCollider::SphereCollider(float r, std::string name) : radius(r), Collider(name){
	colliderType = SPHERE;
	SceneManager::GetInstance().AddCollider(this);
}

SphereCollider::~SphereCollider(){

}

float SphereCollider::GetRadius() const{
	return radius;
}

void SphereCollider::GetSphereColliderInfo(float &x, float &y, float &z, float &r){
	x = position.x();
	y = position.y();
	z = position.z();
	r = radius;
}

void SphereCollider::Delete(){
	// std::cout << "delete Collider" << std::endl;
	SceneManager::GetInstance().RemoveCollider(this);
}

ColliderType SphereCollider::GetColliderType() const{
	return colliderType;
}

void SphereCollider::DrawForDebug(){
	if(isDebug){
		Collider::DrawForDebug();
		glPushMatrix();
		glDisable(GL_LIGHTING);
		glColor3f(0.0f, 1.0f, 0.0f);
		glTranslatef(position.x(), position.y(), position.z());
		glutWireSphere(radius, 20, 20);
		glColor3f(0.0f, 0.0f, 0.0f);
		glEnable(GL_LIGHTING);
		glPopMatrix();
	}
}