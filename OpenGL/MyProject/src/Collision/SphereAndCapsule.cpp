#include <Collision/SphereAndCapsule.h>
#include <iostream>
#include <cmath>

bool SphereAndCapsule::IsCollision(Collider* c1, Collider* c2){
	SphereCollider* collider1;
	CapsuleCollider* collider2;

	if(dynamic_cast<SphereCollider*>(c1)){
		collider1 = dynamic_cast<SphereCollider*>(c1);
		collider2 = dynamic_cast<CapsuleCollider*>(c2);
	}else{
		collider1 = dynamic_cast<SphereCollider*>(c2);
		collider2 = dynamic_cast<CapsuleCollider*>(c1);
	}

	float d = GetPointSegmentDistance(collider1->GetPosition(), *collider2);
	float r = collider1->GetRadius() + collider2->GetRadius();

	if((r * r) >= d){
		return true;
	}
	return false;
}

float SphereAndCapsule::GetPointSegmentDistance(Vector3 p, CapsuleCollider c){
	Vector3 v1 = p - c.GetStartPoint();
	Vector3 v2 = c.GetEndPoint() - c.GetStartPoint();
	float v1v2 = v1.Dot(v2);
	if(v1v2 <= 0.0f){
		return v1.Length2();
	}
	float v2v2 = v2.Length2();
	if(v2v2 <= v1v2){
		Vector3 v3 = p - c.GetEndPoint();
		return v3.Length2();
	}

	return v1.Length2() - v1v2*v1v2 / v2v2;

}