#include <Collision/SphereAndSphere.h>
#include <Collision/SphereCollider.h>
#include <cmath>

bool SphereAndSphere::IsCollision(Collider* c1,Collider* c2){
	SphereCollider* collider1 = static_cast<SphereCollider*>(c1);
	SphereCollider* collider2 = static_cast<SphereCollider*>(c2);
	float x0, y0, z0, r0;
	collider1->GetSphereColliderInfo(x0, y0, z0, r0);
	float x1, y1, z1, r1;
	collider2->GetSphereColliderInfo(x1, y1, z1, r1);

	float d = sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0) + (z1 - z0) * (z1 - z0));
	if(d <= (r0 + r1)){
		return true;
	}
	return false;
}