#include <Easing/Easing.h>
#include <cmath>

namespace easing{

	float clamp( float n, float lower, float upper ){
	    n = ( n > lower ) * n + !( n > lower ) * lower;
	    return ( n < upper ) * n + !( n < upper ) * upper;
	}

	float clamp01(float n){
		return clamp(n, 0.0, 1.0);
	}

	float sine(float time){
		float t = clamp01(time);
		return 1.0 - cos(M_PI / 2.0 * t);
	}

	float quad(float time){
		float t = clamp01(time);
		return t * t;
	}

	float cubic(float time){
		float t = clamp01(time);
		return t*t*t;
	}

	float quart(float time){
		float t = clamp01(time);
		return t*t*t*t;
	}

	float expo(float time){
		float t = clamp01(time);
		return pow(2, 10 * (t - 1.0));
	}

	float circ(float time){
		float t = clamp01(time);
		return 1.0 - sqrt(1.0 - t * t);
	}

	float back(float time){
		float t = clamp01(time);
		float s = 1.70158f;
		return t * t * ((s + 1) * t - s);
	}

	float elastic(float time){
		float t = clamp01(time);
		if(t == 0){
			return 0;
		}
		if(t == 1){
			return 1;
		}

		float postFix = pow(2, 10 * (t-=1));
		return -(postFix*sin((t - 0.075) * (2*M_PI) / 0.3333333f));
	}

	float bounceIn(float time){
		float t = clamp01(time);
		return 1 - bounceOut(1 - t);
	}

	float bounceOut(float time){
		float t = clamp01(time);
	    
	    // float rvsTime = 1.0 - t;
	    // if (rvsTime < (1.0 / 2.75)) {
	    // 	return 1.0 - 7.5625 * rvsTime * rvsTime;
	    // }
	    // else if (rvsTime < (2.0 / 2.75)) {
		   //  float postFix = rvsTime - (1.5f / 2.75f);
		   //  return -7.5625 * rvsTime * postFix + 0.25;
	    // }
	    // else if (rvsTime < (2.5 / 2.75)) {
	    // 	float postFix = rvsTime - (2.25f / 2.75f);
	    // 	return -7.5625 * rvsTime * postFix + 0.0625;
	    // }
	    // else {
	    // 	float postFix = rvsTime - (2.625f / 2.75f);
	    // 	return -7.5625 * rvsTime * postFix + 0.015625;
	    // }

		// t = 1.0 - t;
		if (t < (1/2.75f)) {
			return (7.5625f * t * t);
		} else if (t < (2/2.75f)) {
			float postFix = t -= (1.5f / 2.75f);
			return 7.5625f * postFix * t + 0.75f;
		} else if (t < (2.5/2.75)) {
			float postFix = t-=(2.25f/2.75f);
			return 7.5625f * postFix * t + 0.9375f;
		} else {
			float postFix = t -= (2.625f / 2.75f);
			return 7.5625f * postFix * t + 0.984375f;
		}
		}
}
