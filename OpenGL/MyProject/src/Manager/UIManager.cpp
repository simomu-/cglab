#include <Manager/UIManager.h>
#include <sstream>
#include <iomanip>
#include <string>
#include <GL/glut.h>
#include <cmath>

UIManager::UIManager() : currentTimeText(""), currentHealthText("") {

}

void UIManager::DrawString(std::string str, int w, int h, int x0, int y0){
	glDisable(GL_LIGHTING);
	// 平行投影にする
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, w, h, 0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	// 画面上にテキスト描画
	glRasterPos2f(x0, y0);
	int size = (int)str.size();
	for(int i = 0; i < size; ++i){
	    char ic = str[i];
	    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ic);
	}

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

}

void UIManager::SetHealth(float h){
	std::ostringstream text;
	text << std::fixed << std::setprecision(1) << h;
	currentHealthText = text.str();
}

void UIManager::SetCurrentTime(float time){
	currentTimeText.clear();

	int millisecond = (int)floor((time - floor(time)) * 100);
	std::ostringstream text;
	text << std::setfill('0') << std::setw(2) <<  std::to_string((int)time / 60);
	currentTimeText += text.str() + ":";
	text.str("");
	text << std::setfill('0') << std::setw(2) << std::to_string((int)time % 60);
	currentTimeText += text.str() + ":";
	text.str("");
	text << std::setw(2) << std::setfill('0') << std::to_string(millisecond);
	currentTimeText += text.str();

	// currentTimeText = std::to_string((int)time / 60) + ":" + 
	// 				  std::to_string((int)time % 60) + ":" + 
	// 				  std::to_string(millisecond);
}

void UIManager::ClearAllText(){
	currentTimeText = "";
	currentHealthText = "";
}

void UIManager::Draw(){

	glColor3f(0.0, 0.6f, 0.0f);
	DrawString(currentTimeText,1270 , 720, 1280 / 2 - currentTimeText.size()*12 /2, 100);
	DrawString(currentHealthText, 1270, 720, 100, 100);
	glColor3f(0.0f, 0.0f, 0.0f);

}