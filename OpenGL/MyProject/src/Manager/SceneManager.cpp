#include <Manager/SceneManager.h>
#include <Objects/ObjectBase.h>
#include <Manager/UIManager.h>
#include <Collision/FlexibleCollision.h>
#include <algorithm>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>

SceneManager::SceneManager() : deltaTime(0.02){
};

void SceneManager::AddObject(ObjectBase *object){
	sceneObjects.push_back(object);
}

void SceneManager::AddObject2D(ObjectBase2D *object){
	sceneObjects2D.push_back(object);
	sort(sceneObjects2D.begin(), sceneObjects2D.end(), ObjectBase2D::Compare());
}

void SceneManager::AddNextSceneObject(ObjectBase *object){
	nextSceneObjects.push_back(object);
}

void SceneManager::AddNextSceneObject2D(ObjectBase2D *object){
	nectSceneObjects2D.push_back(object);
	sort(sceneObjects2D.begin(), sceneObjects2D.end(), ObjectBase2D::Compare());
}

void SceneManager::RemoveObject(ObjectBase *object){

    auto itr = std::find(sceneObjects.begin(), sceneObjects.end(), object);
	delete (*itr);
	sceneObjects.erase(itr);
	sceneObjects.shrink_to_fit();
}

void SceneManager::RemoveObject2D(ObjectBase2D* object){

    auto itr = std::find(sceneObjects2D.begin(), sceneObjects2D.end(), object);
	delete (*itr);
	sceneObjects2D.erase(itr);
	sceneObjects2D.shrink_to_fit();
}

void SceneManager::DrawAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->Draw();
	}
}

void SceneManager::DrawAllObjects2D(){
	for(int i = 0; i < sceneObjects2D.size(); i++){
		sceneObjects2D[i]->Draw();
	}	
}

void SceneManager::UpdateAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->Update();
	}
}

void SceneManager::UpdateAllObjects2D(){
	for(int i = 0; i < sceneObjects2D.size(); i++){
		sceneObjects2D[i]->Update();
	}
}

void SceneManager::LateUpdateAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->LateUpdate();
	}
}

void SceneManager::LateUpdateAllObjects2D(){
	for(int i = 0; i < sceneObjects2D.size(); i++){
		sceneObjects2D[i]->LateUpdate();
	}
}

void SceneManager::CheckDeleteAllObjects(){
	for(int i = 0; i < sceneObjects.size(); i++){
		sceneObjects[i]->CheckDelete();
	}
}

void SceneManager::CheckDeleteAddObjects2D(){
	for(int i = 0; i < sceneObjects2D.size(); i++){
		sceneObjects2D[i]->CheckDelete();
	}
}

void SceneManager::AddCollider(Collider* collider){
	colliderObjects.push_back(collider);
}

void SceneManager::RemoveCollider(Collider* collider){
    auto itr = std::find(colliderObjects.begin(), colliderObjects.end(), collider);
	// delete (*itr); 
	colliderObjects.erase(itr);
	colliderObjects.shrink_to_fit();
}

bool SceneManager::IsHit(Collider* collider) const{
	FlexibleCollision collision;
	bool hit = false;
	for(int i = 0; i < colliderObjects.size(); i++){
		if(collider == colliderObjects[i]){
			continue;
		}
		
		if(collision.IsCollision(collider, colliderObjects[i])){
			return true;
		}
	}
	return false;
}

bool SceneManager::IsHit(Collider* collider, std::string name) const{
	
	return false;
}

void SceneManager::SceneChange(){
	UIManager::GetInstance().ClearAllText();
	for(int i = 0; i < sceneObjects.size(); i++){
		delete sceneObjects[i];
	}
	sceneObjects.clear();
	sceneObjects.shrink_to_fit();
	sceneObjects = nextSceneObjects;
	nextSceneObjects.clear();
	nextSceneObjects.shrink_to_fit();

	for(int i = 0; i < sceneObjects2D.size(); i++){
		delete sceneObjects2D[i];
	}
	sceneObjects2D.clear();
	sceneObjects2D.shrink_to_fit();
	sceneObjects2D = nectSceneObjects2D;
	nectSceneObjects2D.clear();
	nectSceneObjects2D.shrink_to_fit();
}

float SceneManager::GetUpdateDeltaTime() const{
	return deltaTime;
}