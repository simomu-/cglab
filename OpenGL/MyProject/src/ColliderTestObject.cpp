#include <ColliderTestObject.h>
#include <Manager/SceneManager.h>
#include <iostream>

ColliderTestObject::ColliderTestObject() : collider(2.0f) , PolygonObject("Resource/kutiku.obj"){
}

void ColliderTestObject::Draw(){
	PolygonObject::Draw();
	collider.DrawForDebug();
}

void ColliderTestObject::Update(){
	collider.SetPosition(position);
	// collider.SetDirection(right);
}

void ColliderTestObject::LateUpdate(){
	if(SceneManager::GetInstance().IsHit(&collider)){
		std::cout << "Hit" << std::endl;
	}else{
		std::cout << std::endl;
	}
}