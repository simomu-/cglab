#include <TestObject.h>
#include <Manager/InputManager.h>
#include <Manager/SceneManager.h>
#include <iostream>

TestObject::TestObject() : object1("Resource/Player1.obj"), object2("Resource/Player1.obj"){
	object1.SetPosition(5, 0, 0);
	object2.SetPosition(-5, 0, 0);
	object2.SetVisible(true);
}

void TestObject::Draw(){
	object1.Draw();
	object2.Draw();
}

void TestObject::Update(){
	if(InputManager::GetInstance().IsRightButton){
		object1.Rotate(0, 0, 2);
	}
	if(InputManager::GetInstance().IsLeftButton){
		object1.Rotate(0, 0, -2);
	}
	if(InputManager::GetInstance().IsUpButton){
		object1.Rotate(2, 0, 0);
	}
	if(InputManager::GetInstance().IsDownButton){
		object1.Rotate(-2, 0, 0);
	}
	object1.MoveForward(0.1);
	object2.MoveForward(0.05);

	// Vector3 eular = object1.GetRotation().GetEularAngle();
	// object2.SetRotation(eular.x(), eular.y(), eular.z());
	// object2.SetRotationFromQuaternion(object1.GetRotation());
	object2.LookAt(object1.GetPosition());
	// std::cout << object2.GetRotation().GetEularAngle() << std::endl;
	
}

void TestObject::LateUpdate(){

}

