#include <Math/Quaternion.h>
#include <cmath>

Quaternion::Quaternion(){
	element = {1.0, 0.0, 0.0, 0.0};
}

Quaternion::Quaternion(float rad, Vector3 dir){
	element[0] = rad;
	element[1] = dir.x();
	element[2] = dir.y();
	element[3] = dir.z();

	// element[0] = rad / 2.0;
	// element[1] = dir.x() * sin(rad / 2.0);
	// element[2] = dir.y() * sin(rad / 2.0);
	// element[3] = dir.z() * sin(rad / 2.0);

	Normalize();
}

void Quaternion::Set(float w, float x, float y, float z){
	element[0] = w;
	element[1] = x;
	element[2] = y;
	element[3] = z;

	// element[0] = w / 2.0;
	// element[1] = x * sin(w / 2.0);
	// element[2] = y * sin(w / 2.0);
	// element[3] = z * sin(w / 2.0);

	Normalize();
}

void Quaternion::SetFromEulerAngle(float x, float y, float z){
	
	float rz = x * M_PI / 180;
	float ry = y * M_PI / 180;
	float rx = z * M_PI / 180;

	float C1 = cos(ry / 2);
	float C2 = cos(rx / 2);
	float C3 = cos(rz / 2);
	float S1 = sin(ry / 2);
	float S2 = sin(rx / 2);
	float S3 = sin(rz / 2);

	element[0] = ((C1 * C2 * C3) - (S1 * S2 * S3));
	element[1] = (S1 * S2 * C3) + (C1 * C2 * S3);
	element[2] = (S1 * C2 * C3) + (C1 * S2 * S3);
	element[3] = (C1 * S2 * C3) - (S1 * C2 * S3);
}

void Quaternion::SetFromAxisAngle(float rad, Vector3 dir){

	float halfRad = rad / 2.0;
	float s = sin(halfRad);
	// dir.Normalize();
	element[0] = cos(halfRad);
	element[1] = dir.x() * s;
	element[2] = dir.y() * s;
	element[3] = dir.z() * s;
}

float Quaternion::w() const{
	return element[0];
}

float Quaternion::x() const{
	return element[1];
}

float Quaternion::y() const{
	return element[2];
}

float Quaternion::z() const{
	return element[3];
}

std::array<float, 16> Quaternion::GetMatrix(){
	std::array<float, 16> matrix;

	float x2 = element[1] * element[1] * 2.0;
	float y2 = element[2] * element[2] * 2.0;

	float z2 = element[3] * element[3] * 2.0;
	float xy = element[1] * element[2] * 2.0;
	float yz = element[2] * element[3] * 2.0;
	float zx = element[3] * element[1] * 2.0;
	float xw = element[1] * element[0] * 2.0;
	float yw = element[2] * element[0] * 2.0;
	float zw = element[3] * element[0] * 2.0;

	matrix[0] = 1.0 - y2 - z2;
	matrix[1] = xy + zw;
	matrix[2] = zx - yw;
	matrix[4] = xy - zw;
	matrix[5] = 1.0 - z2 - x2;
	matrix[6] = yz + xw;
	matrix[8] = zx + yw;
	matrix[9] = yz - xw;
	matrix[10] = 1.0 - x2 - y2;
	matrix[3] = matrix[7] = matrix[11] = matrix[12] = matrix[13] = matrix[14] = 0.0;
	matrix[15] = 1.0;

	return matrix;
	
}

void Quaternion::Normalize(){
	float l = Length();
	element[0] /= l;
	element[1] /= l;
	element[2] /= l;
	element[3] /= l;
}

float Quaternion::Length(){
	return sqrt(element[0]*element[0] + 
				element[1]*element[1] + 
				element[2]*element[2] + 
				element[3]*element[3]);
}

Quaternion Quaternion::Conjugate(){
	Quaternion q;
	q.Set(element[0], -element[1], -element[2], -element[3]);
	return q;
}

Vector3 Quaternion::GetEularAngle(){
	Vector3 v;
	float y2 = element[2] * element[2];

	float t0 = +2.0 * (w() * x() + y() * z());
	float t1 = +1.0 - 2.0 * (x() * x() + y2);
	float roll = std::atan2(t0, t1);
	roll = roll * 180.0 / M_PI;

	float t2 = +2.0 * (w() * y() - z() * x());
	t2 = t2 > 1.0 ? 1.0 : t2;
	t2 = t2 < -1.0 ? -1.0 : t2;
	float pitch = std::asin(t2);
	pitch = pitch * 180.0 / M_PI;


	float t3 = +2.0 * (w() * z() + x() * y());
	float t4 = +1.0 - 2.0 * (y2 + z() * z());  
	float yaw = std::atan2(t3, t4);
	yaw = yaw * 180.0 / M_PI;

	v.Set(roll, pitch, yaw);
	return v;
}

Vector3 Quaternion::RotateVector(Vector3 vector){

	Vector3 v;
	float elm[4] = {0,0,0,1};
	float vi[4] = { vector.x(), vector.y(), vector.z(), 1.0};
	std::array<float,16> matrix = Conjugate().GetMatrix();

	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			elm[i] += matrix[4*i + j] * vi[j];
		}
	}

	v.Set(elm[0], elm[1], elm[2]);
	return v;
}


