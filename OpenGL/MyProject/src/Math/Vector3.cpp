#include <Math/Vector3.h>
#include <cmath>

Vector3::Vector3() : element_x(0), element_y(0), element_z(0){

}

Vector3::Vector3(float x, float y, float z) : element_x(x), element_y(y), element_z(z) {

}

// Vector3::Vector3(Vector3 vector){
// 	x = vector.x();
// 	y = vector.y();
// 	z = vector.z();
// }

float Vector3::x() const{
	return element_x;
}

float Vector3::y() const{
	return element_y;
}

float Vector3::z() const{
	return element_z;
}

void Vector3::Set(float x, float y, float z){
	element_x = x;
	element_y = y;
	element_z = z;
}

float Vector3::Length(){
	return sqrt(Length2());
}

float Vector3::Length2(){
	return (element_x * element_x + element_y * element_y + element_z * element_z);
}

void Vector3::Normalize(){
	float l = Length();
	if(l <= 0){
		return;
	}
	element_x /= l;
	element_y /= l;
	element_z /= l;
}

Vector3 Vector3::Normalized(){

	Vector3 vector(x(), y(), z());
	vector.Normalize();

	return vector;

}

float Vector3::Dot(const Vector3 &vector){
	return (element_x * vector.x() + element_y * vector.y() + element_z * vector.z());
}

Vector3 Vector3::Cross(Vector3 &vector){

	float x = element_y * vector.z() - element_z * vector.y();
	float y = element_z * vector.x() - element_x * vector.z();
	float z = element_x * vector.y() - element_y * vector.x();

	Vector3 vec(x, y, z);
	return vec;

}

float* Vector3::ToArray(){
	float array[3] = {x(), y(), z()};
	return array;
}

float* Vector3::ToArray(float value){
	float array[4] = {x(), y(), z(), value};
	return array;
}

Vector3 Vector3::Forward(){
	Vector3 v(0.0, 0.0, 1.0);
	return v;
}

Vector3 Vector3::Up(){
	Vector3 v(0.0, 1.0, 0.0);
	return v;
}

Vector3 Vector3::Right(){
	Vector3 v(1.0, 0.0, 0.0);
	return v;
}
