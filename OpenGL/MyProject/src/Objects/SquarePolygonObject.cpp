#include <Objects/SquarePolygonObject.h>
#include <GL/glut.h>
#include <fstream>
#include <iostream>

SquarePolygonObject::SquarePolygonObject(std::string objFileName) : PolygonObject(objFileName){
	LoadSquareFace(objFileName);
	std::cout << "quad faces " << squareFaces.size() << std::endl;
}

void SquarePolygonObject::LoadSquareFace(std::string objFileName){
	squareFaces.clear();
	std::ifstream file;
	char buf[256];
	float r = 0.8;
	float g = 0.8;
	float b = 0.8;

	file.open(objFileName.c_str());
	while(!file.eof()){
		file.getline(buf,sizeof(buf));
		if(buf[0] == 'f'){
			SetSquareFaces(buf, r, g, b);
		}
	}
	file.close();
}

void SquarePolygonObject::SetSquareFaces(char dataLine[], float r, float g, float b){
	int v0;
	int v1;
	int v2;
	int v3;
	int n0;
	int n1;
	int n2;
	int n3;
	sscanf(dataLine + 2, "%d//%d %d//%d %d//%d %d//%d", &v0, &n0, &v1, &n1, &v2, &n2, &v3, &n3);
	SquareFace f(v0, v1, v2, v3);
	f.SetDiffuseMaterial(r, g, b);
	squareFaces.push_back(f);
}

void SquarePolygonObject::Draw(){
	
	glPushMatrix();

	glTranslated(position.x(), position.y(), position.z());
	glRotated(rotation_x, 1.0, 0.0, 0.0);
	glRotated(rotation_y, 0.0, 1.0, 0.0);
	glRotated(rotation_z, 0.0, 0.0, 1.0);
	glTranslated(-position.x(), -position.y(), -position.z());

	glTranslated(position.x(), position.y(), position.z());

	if(isVisible){
		glBegin(GL_QUADS);
		for(int i = 0; i < squareFaces.size(); i++){

			int f0 = squareFaces[i].faceIndex0 - 1;
			int f1 = squareFaces[i].faceIndex1 - 1;
			int f2 = squareFaces[i].faceIndex2 - 1;
			int f3 = squareFaces[i].faceIndex3 - 1;

			float v0[] = {vertices[f0 * 3], vertices[f0 * 3 + 1], vertices[f0 * 3 + 2]};
			float v1[] = {vertices[f1 * 3], vertices[f1 * 3 + 1], vertices[f1 * 3 + 2]};
			float v2[] = {vertices[f2 * 3], vertices[f2 * 3 + 1], vertices[f2 * 3 + 2]};
			float v3[] = {vertices[f3 * 3], vertices[f3 * 3 + 1], vertices[f3 * 3 + 2]};

			float* n = PolygonObject::GetNormalOfPolygon(v0, v1, v2);

			glMaterialfv(GL_FRONT, GL_DIFFUSE, squareFaces[i].GetDiffuseMaterialArray());
			glNormal3fv(n);
			glVertex3fv(v0);
			glVertex3fv(v1);
			glVertex3fv(v2);
			glVertex3fv(v3);

		}
		glEnd();
	}

	glPopMatrix();

}