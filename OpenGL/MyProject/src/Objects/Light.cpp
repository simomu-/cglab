#include <GL/glut.h>
#include <Objects/Light.h>
#include <array>
#include <iostream>

Light::Light(int index) : lightIndex(index){
}

void Light::SetColor(float r, float g, float b){
	colorR = r;
	colorG = g;
	colorB = b;
	colorArray[0] = r;
	colorArray[1] = g;
	colorArray[2] = b;
	colorArray[3] = 1.0f;

	std::array<float,4> cArray = {colorR, colorG, colorB, 1.0};
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, colorArray.data());
	// glLightfv(GL_LIGHT0, GL_SPECULAR, cArray.data());
	glLightfv(GL_LIGHT0, GL_AMBIENT, colorArray.data());
;}

void Light::Draw(){
	glPushMatrix();
	float array[4] = {position.x(), position.y(), position.z(), 1.0};
	glLightfv(GL_LIGHT0, GL_POSITION, array);
	glPopMatrix();	
}