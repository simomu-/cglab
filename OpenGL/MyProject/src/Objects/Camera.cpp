#include <Objects/Camera.h>
#include <GL/glut.h>
#include <iostream>

void Camera::Draw(){

	gluLookAt(position.x(), position.y(), position.z(), 
			  position.x() + forward.x(), position.y() + forward.y(), position.z() + forward.z(),
			  up.x(), up.y(), up.z());
}
