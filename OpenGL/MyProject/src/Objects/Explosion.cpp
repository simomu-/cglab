#include <Objects/Explosion.h>
#include <Manager/SceneManager.h>
#include <GL/glut.h>
#include <cmath>

Explosion::Explosion(float s) : size(s), 
								cullentSize(0.0f), 
								lifeTime(0.5f){
	diffuseArray[0] = 0.0f;
	diffuseArray[1] = 0.0f;
	diffuseArray[2] = 0.0f;
	diffuseArray[3] = 0.0f;

	emissionArray[0] = 1.0f;
	emissionArray[1] = 0.0f;
	emissionArray[2] = 0.0f;
	emissionArray[3] = 0.0f;
	deltaSize = size / lifeTime;
	deltaColor = 1.0f / lifeTime;
}

void Explosion::Draw(){
	glPushMatrix();
	glTranslatef(position.x(), position.y(), position.z());
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseArray.data());
	// glMaterialfv(GL_FRONT, GL_DIFFUSE, emissionArray.data());
	glMaterialfv(GL_FRONT, GL_EMISSION, emissionArray.data());
	glutSolidSphere(cullentSize, 10, 10);

	float c[] = {0.0, 0.0, 0.0, 0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, c);

	glPopMatrix();
}

void Explosion::Update(){

	float deltaTime = SceneManager::GetInstance().GetUpdateDeltaTime();

	lifeTime -= deltaTime;
	emissionArray[1] += deltaColor * deltaTime;
	cullentSize = size * (1.0f - pow(1.0f/100.0f,emissionArray[1]));

	if(lifeTime < 0){
		lifeTime = 1.0f;
		isDelete = true;
	}
}