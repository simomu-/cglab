#include <Objects/FollowCamera.h>
#include <iostream>

void FollowCamera::Draw(){

	SetPosition(targetObject->PositionX(), 
				targetObject->PositionY(), 
				targetObject->PositionZ());

	float diff_x = targetObject->RotationX() - preRotationX;
	float diff_y = targetObject->RotationY() - preRotationY;
	float diff_z = targetObject->RotationZ() - preRotationZ;

	Rotate(diff_x, diff_y, diff_z);

	preRotationX = targetObject->RotationX();
	preRotationY = targetObject->RotationY();
	preRotationZ = targetObject->RotationZ();

	MoveForward(pivot.z());
	MoveRight(pivot.x());
	MoveUp(pivot.y());

	Camera::Draw();
}

void FollowCamera::SetTargetObject(ObjectBase* object){
	targetObject = object;
	// Rotate(30.0, 0.0, 0.0);
}

void FollowCamera::SetPivot(Vector3 v){
	pivot = v;
}