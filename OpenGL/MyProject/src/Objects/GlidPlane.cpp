#include <Objects/GlidPlane.h>
#include <GL/glut.h>
#include <array>
#include <iostream>

GlidPlane::GlidPlane(int s, float gSize) : size(s), glidSize(gSize){

}

void GlidPlane::Draw(){

	glPushMatrix();

	int glidStartPos = size / 2;
	glDisable(GL_LIGHTING);
	glDisable(GL_FOG);
	glBegin(GL_LINES);
	glColor3d(0.0, 0.0, 0.0);
	for(int i = -glidStartPos; i <= glidStartPos; i += glidSize){
        glVertex3d( i, position.y(), -size / 2);
        glVertex3d( i, position.y(),  size / 2);
	}
	for(int i = -glidStartPos; i <= glidStartPos; i += glidSize){
        glVertex3d( -size / 2, position.y(), i);
        glVertex3d(  size / 2, position.y(), i);
	}
	glEnd();
	glEnable(GL_LIGHTING);
	glEnable(GL_FOG);
	glPopMatrix();

}