#include <Objects/Image2D.h>

Image2D::Image2D(std::string fileName) : pictureName(fileName) , 
									   picture(fileName),
									   size(1.0),
									   pivot_x(0.0),
									   pivot_y(0.0){
	glEnable( GL_TEXTURE_2D );
	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	int sizeX = picture.GetWidth();
	int sizeY = picture.GetHeight();
	glTexImage2D( GL_TEXTURE_2D, 0, 3, sizeX, sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, picture.GetColorArray());
	glBindTexture( GL_TEXTURE_2D, 0 );
}

void Image2D::SetSize(float s){
	size = s;
}

float Image2D::GetSize() const{
	return size;
}

void Image2D::SetPivot(float x, float y){
	pivot_x = x;
	pivot_y = y;
}

void Image2D::Draw(){
	glPushMatrix();
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texture );

	glTranslated(position.x(), position.y(), position.z());
	glRotated(rotation_x, 1.0, 0.0, 0.0);
	glRotated(rotation_y, 0.0, 1.0, 0.0);
	glRotated(rotation_z, 0.0, 0.0, 1.0);
	glTranslated(-position.x(), -position.y(), -position.z());

	glTranslated(position.x(), position.y(), position.z());
	float color[] = {1.0, 1.0, 1.0, 1.0};
	glColor3fv(color);
	glBegin(GL_QUADS);

	float px = picture.GetWidth() * pivot_x * size;
	float py = picture.GetHeight() * pivot_y * size;

	glTexCoord2f(1.0f, 0.0f); 
	glVertex2d( picture.GetWidth() * size - px, picture.GetHeight() * size - py);

	glTexCoord2f(1.0f, 1.0f); 
	glVertex2d( picture.GetWidth() * size - px, -py);

	glTexCoord2f(0.0f, 1.0f); 
	glVertex2d(-px ,-py);

	glTexCoord2f(0.0f, 0.0f); 
	glVertex2d(-px, picture.GetHeight() * size - py);

	glEnd();

	glDisable(GL_TEXTURE_2D);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glPopMatrix();

}