#include <Objects/ObjectBase.h>
#include <Manager/SceneManager.h>
#include <GL/glut.h>
#include <cmath>
#include <iostream>

ObjectBase::ObjectBase() :  position(0, 0, 0),
							rotation_x(0),rotation_y(0),rotation_z(0){
	isDelete = false;

	forward.Set(0.0, 0.0, 1.0);
	up.Set(0.0, 1.0, 0.0);
	right.Set(1.0, 0.0, 0.0);
};

ObjectBase::~ObjectBase(){
}


float ObjectBase::PositionX() const{
	return position.x();
}

float ObjectBase::PositionY() const{
	return position.y();
}

float ObjectBase::PositionZ() const{
	return position.z();
}

Vector3 ObjectBase::GetPosition() const{
	return position;
}

float* ObjectBase::GetPositionArray(){
	float* array = new float[3];
	array[0] = position.x();
	array[1] = position.y();
	array[2] = position.z();
	return array;
}

void ObjectBase::SetPosition(float x, float y, float z){
	position.Set(x, y, z);
}

void ObjectBase::Translate(float x, float y, float z){
	Vector3 p(x, y, z);
	position += p;
}

float ObjectBase::RotationX() const{
	return rotation_x;
}

float ObjectBase::RotationY() const{
	return rotation_y;
}

float ObjectBase::RotationZ() const{
	return rotation_z;
}

Quaternion ObjectBase::GetRotation() const{
	return rotation;
};

void ObjectBase::SetRotation(float x, float y, float z){

	// rotation.Set(1.0, 0.0, 0.0, 0.0);
	forward.Set(0.0, 0.0, 1.0);
	up.Set(0.0, 1.0, 0.0);
	right.Set(1.0, 0.0, 0.0);
	// rotation_x = 0;
	// rotation_y = 0;
	// rotation_z = 0;
	
	// Rotate(x, y, z);
	rotation.SetFromEulerAngle(x, y, z);
	forward = rotation.RotateVector(forward);
	up = rotation.RotateVector(up);
	right = rotation.RotateVector(right);

}

void ObjectBase::SetRotationFromQuaternion(Quaternion q){
	rotation = q;

	forward.Set(0.0, 0.0, 1.0);
	up.Set(0.0, 1.0, 0.0);
	right.Set(1.0, 0.0, 0.0);
	
	forward = rotation.RotateVector(forward);
	up = rotation.RotateVector(up);
	right = rotation.RotateVector(right);
}

void ObjectBase::LookAt(Vector3 pos){

	Vector3 direction = pos - position;
	direction.Normalize();
	Vector3 axis = forward.Cross(direction);
	axis.Normalize();	
	
	float dot = forward.Dot(direction);
	if(std::abs(dot) >= 1.0){
		return;
	}

	float rad = std::acos(dot);
	Quaternion r;
	r.SetFromAxisAngle(rad, axis);
	rotation = r * rotation;

	forward.Set(0, 0, 1);
	up.Set(0, 1, 0);
	right.Set(1, 0, 0);

	forward = rotation.RotateVector(forward);
	up = rotation.RotateVector(up);
	right = rotation.RotateVector(right);

}

Vector3 ObjectBase::Forward(){
	return forward;
}

Vector3 ObjectBase::Up(){
	return up;
}

Vector3 ObjectBase::Right(){
	return right;
}


void ObjectBase::Rotate(float x, float y, float z){
	rotation_x += x;
	rotation_y += y;
	rotation_z += z;

	Quaternion q;
	q.SetFromAxisAngle(x * M_PI / 180.0, right);
	rotation = q * rotation;

	up = q.RotateVector(up);
	forward = q.RotateVector(forward);

	q.SetFromAxisAngle(y * M_PI / 180.0, up);
	rotation = q * rotation;

	right = q.RotateVector(right);
	forward = q.RotateVector(forward);

	q.SetFromAxisAngle(z * M_PI / 180.0, forward);
	rotation = q * rotation;

	right = q.RotateVector(right);
	up = q.RotateVector(up);

	// Vector3 eular = rotation.GetEularAngle();
	// std::cout << "convert " << eular << std::endl;
	// rotation_x = eular.x();
	// rotation_y = eular.y();
	// rotation_z = eular.z();

}

void ObjectBase::MoveForward(float v){	

	Translate(forward.x() * v, forward.y() * v, forward.z() * v);
}

void ObjectBase::MoveUp(float v){
	Translate(up.x() * v, up.y() * v, up.z() * v);
}

void ObjectBase::MoveRight(float v){
	Translate(right.x() * v, right.y() * v, right.z() * v);
}

float* GetRotateMatrix(){
}

void ObjectBase::Update(){};
void ObjectBase::LateUpdate(){};
void ObjectBase::Draw(){

	glPushMatrix();
	glDisable(GL_LIGHTING);
	glTranslatef(position.x(), position.y(), position.z());
	glLineWidth(5);

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(forward.x() * 10, forward.y() * 10, forward.z() * 10);
	glEnd();

	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(up.x() * 10, up.y() * 10, up.z() * 10);
	glEnd();

	glColor3f(0.0, 0.0, 1.0);
	glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(right.x() * 10, right.y() * 10, right.z() * 10);
	glEnd();

	glEnable(GL_LIGHTING);
	glLineWidth(1);
	glPopMatrix();

};
void ObjectBase::CheckDelete(){
	if(isDelete){
		Delete();
	}
}
void ObjectBase::Delete(){
	SceneManager::GetInstance().RemoveObject(this);
}