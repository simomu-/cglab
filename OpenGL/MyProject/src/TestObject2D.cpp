#include <TestObject2D.h>
#include <Manager/SceneManager.h>
#include <Manager/InputManager.h>
#include <Utility/DisplaySetting.h>
#include <Easing/Easing.h>
#include <iostream>

TestObject2D::TestObject2D(): Image2D("Resource/TestImage.bmp"), 
							  currentTime(0),
							  moveTime(1.0){
  	SetPivot(0.5, 0.5);
  	SetSize(5);
  	SetPosition(DisplaySetting::GetWidth()/2, DisplaySetting::GetHeight()/2, 0);
}

void TestObject2D::Update(){
	currentTime += SceneManager::GetInstance().GetUpdateDeltaTime();

	float p = DisplaySetting::GetWidth()/4 + easing::bounceOut(currentTime / moveTime) * DisplaySetting::GetWidth()/2;
	SetPosition(p, DisplaySetting::GetHeight()/2.0, 0.0);

	if(InputManager::GetInstance().IsReturnKey){
		currentTime = 0;
	}

}