#pragma once
#include <Objects/ObjectBase.h>
#include <Objects/ObjectBase2D.h>
#include <Collision/Collider.h>
#include <vector>
#include <string>

class SceneManager{
private:
	SceneManager();
	~SceneManager() = default;
	std::vector<ObjectBase*> sceneObjects;
	std::vector<ObjectBase2D*> sceneObjects2D;

	std::vector<ObjectBase*> nextSceneObjects;
	std::vector<ObjectBase2D*> nectSceneObjects2D;

	std::vector<Collider*> colliderObjects;
	const float deltaTime;
	
public:
	SceneManager(const SceneManager&) = delete;
	SceneManager& operator=(const SceneManager&) = delete;
    SceneManager(SceneManager&&) = delete;
    SceneManager& operator=(SceneManager&&) = delete;

    static SceneManager& GetInstance() {
        static SceneManager inst;
        return inst;
    }

	void AddObject(ObjectBase *object);
	void AddObject2D(ObjectBase2D *object);

	void AddNextSceneObject(ObjectBase *object);
	void AddNextSceneObject2D(ObjectBase2D *object);


	void RemoveObject(ObjectBase *object);
	void RemoveObject2D(ObjectBase2D *object);


	void DrawAllObjects();
	void DrawAllObjects2D();

	void UpdateAllObjects();
	void UpdateAllObjects2D();

	void LateUpdateAllObjects();
	void LateUpdateAllObjects2D();

	void CheckDeleteAllObjects();
	void CheckDeleteAddObjects2D();

	void AddCollider(Collider* collider);
	void RemoveCollider(Collider* collider);

	bool IsHit(Collider* collider) const;
	bool IsHit(Collider* collider, std::string name) const;

	void SceneChange();

	float GetUpdateDeltaTime() const;
};