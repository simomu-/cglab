#pragma once
#include <Objects/ObjectBase2D.h>
#include <Utility/BmpPicture.h>
#include <string>
#include <GL/glut.h>

class Image2D : public ObjectBase2D{

	std::string pictureName;
	BmpPicture picture;
	GLuint texture;
	float size;
	float pivot_x;
	float pivot_y;

public:
	Image2D(std::string fileName);
	~Image2D() = default;

	void SetSize(float s);
	float GetSize() const;

	void SetPivot(float x, float y);

	void Draw();
};