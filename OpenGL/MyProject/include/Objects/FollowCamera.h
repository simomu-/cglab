#pragma once
#include <Objects/Camera.h>
#include <Objects/ObjectBase.h>
#include <Math/Vector3.h>

class FollowCamera : public Camera{

	float preRotationX;
	float preRotationY;
	float preRotationZ;

protected:
	ObjectBase* targetObject;
	Vector3 pivot;	

public:
	FollowCamera() = default;
	~FollowCamera() = default;

	void Draw();

	void SetTargetObject(ObjectBase* object);
	void SetPivot(Vector3 v);
	
};