#pragma once
#include <Objects/ObjectBase.h>
#include <vector>
#include <array>
#include <string>
#include <cmath>

struct Face{

	int faceIndex0;
	int faceIndex1;
	int faceIndex2;

	int normalIndex0;
	int normalIndex1;
	int normalIndex2;	

	std::array<float , 4> diffuseMaterialArray;
	std::array<float , 4> specularMaterialArray;
	std::array<float , 4> ambientMaterialArray;
	std::array<float , 4> emissionMaterialArray;

	Face(int f0, int f1, int f2) : faceIndex0(f0) , faceIndex1(f1) , faceIndex2(f2) {
	}

	void SetNormalIndex(int n0, int n1, int n2){
		normalIndex0 = n0;
		normalIndex1 = n1;
		normalIndex2 = n2;
	}

	void SetDiffuseMaterial(float r, float g, float b){
		diffuseMaterialArray[0] = r;
		diffuseMaterialArray[1] = g;
		diffuseMaterialArray[2] = b;
		diffuseMaterialArray[3] = 1.0f;
	}

	void SetSpecularMaterial(float r, float g, float b){
		specularMaterialArray[0] = r;
		specularMaterialArray[1] = g;
		specularMaterialArray[2] = b;
		specularMaterialArray[3] = 1.0f;
	}

	void SetAmbientMaterial(float r, float g, float b){
		ambientMaterialArray[0] = r;
		ambientMaterialArray[1] = g;
		ambientMaterialArray[2] = b;
		ambientMaterialArray[3] = 1.0f;
 	}

 	void SetEmissionMaterial(float r, float g, float b){
 		emissionMaterialArray[0] = r;
 		emissionMaterialArray[1] = g;
 		emissionMaterialArray[2] = b;
 		emissionMaterialArray[3] = 1.0f;
 	}

	float* GetDiffuseMaterial(){
		return diffuseMaterialArray.data();
	}

	float* GetSpecularMaterial(){
		return specularMaterialArray.data();
	}

	float* GetAmbientMaterial(){
		return ambientMaterialArray.data();
	}

	float* GetEmissionMaterial(){
		return emissionMaterialArray.data();
	}
};


class PolygonObject : public ObjectBase{

protected:

	bool isVisible;

	std::string mtlFileName;

	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<Face> faces;
	std::vector<int> faceNormals;

	void SetVertex(char dataLine[]);
	void SetFaces(char dataLine[],float diffuce[], float specular[], float ambient[], float emission[]);
	void SetMaterial(std::string materialName, float diffuce[], float specular[], float ambient[], float emission[]);

public:
	PolygonObject();
	PolygonObject(std::string objFileName);
	void LoadObjFile(std::string objFileName);
	void Draw();
	void Update();
	void SetVisible(bool visible);
	static float* GetNormalOfPolygon(float v0[], float v1[], float v2[]);

};
