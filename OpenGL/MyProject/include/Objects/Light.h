#pragma once
#include <Objects/ObjectBase.h>
#include <array>

class Light : public ObjectBase{
	int lightIndex;
	float colorR;
	float colorG;
	float colorB;

	std::array<float,4> colorArray;

public:
	Light();
	Light(int index);
	void Draw();
	void SetColor(float r, float g, float b);

	
};