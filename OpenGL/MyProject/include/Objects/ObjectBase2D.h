#pragma once
#include <Objects/ObjectBase.h>
#include <functional>

class ObjectBase2D : public ObjectBase{

public:
	struct Compare : public std::binary_function<ObjectBase2D, ObjectBase2D, bool>{
		bool operator()( ObjectBase2D* lhs, ObjectBase2D* rhs){
		  return (lhs->PositionZ() < rhs->PositionZ());
		}
	};
};