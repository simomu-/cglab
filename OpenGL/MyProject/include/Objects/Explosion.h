#pragma once
#include <Objects/ObjectBase.h>
#include <array>

class Explosion : public ObjectBase{
private:
	float size;
	float cullentSize;
	float deltaSize;
	float deltaColor;
	float lifeTime;
	std::array<float, 4> diffuseArray;
	std::array<float, 4> emissionArray;

public:
	Explosion(float s);

	void Draw();
	void Update();
	
};