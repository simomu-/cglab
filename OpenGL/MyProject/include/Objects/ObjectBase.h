#pragma once
#include <array>
#include <Math/Quaternion.h>
#include <Math/Vector3.h>

class ObjectBase{

protected:
	bool isDelete;

	Vector3 position;

	float rotation_x;
	float rotation_y;
	float rotation_z;

	Quaternion rotation;
	Vector3 forward;
	Vector3 up;
	Vector3 right;
	
public:
	ObjectBase();
	virtual ~ObjectBase();
	float PositionX() const;
	float PositionY() const;
	float PositionZ() const;
	Vector3 GetPosition() const;
	float* GetPositionArray();

	void SetPosition(float x, float y, float z);
	void Translate(float x, float y, float z);

	float RotationX() const;
	float RotationY() const;
	float RotationZ() const;

	Quaternion GetRotation() const;
	
	void Rotate(float x, float y, float z);
	void SetRotation(float x, float y, float z);
	void SetRotationFromQuaternion(Quaternion q);
	void LookAt(Vector3 pos);
	Vector3 Forward();
	Vector3 Up();
	Vector3 Right();
	void MoveForward(float v);
	void MoveUp(float v);
	void MoveRight(float v);

	float* GetRotateMatrix();

	virtual void Update();
	virtual void LateUpdate();
	virtual void Draw();
	virtual void CheckDelete();
	virtual void Delete();
};