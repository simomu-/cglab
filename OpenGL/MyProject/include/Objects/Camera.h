#pragma once
#include <Objects/ObjectBase.h>
#include <array>

class Camera : public ObjectBase{

public:
	Camera() = default;
	void Draw();
};