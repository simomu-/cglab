
namespace easing{

	float clamp( float n, float lower, float upper );

	float clamp01(float n);

	float sine(float time);

	float quad(float time);

	float cubic(float time);

	float quart(float time);

	float expo(float time);

	float circ(float time);

	float back(float time);

	float elastic(float time);

	float bounceIn(float time);

	float bounceOut(float time);
}