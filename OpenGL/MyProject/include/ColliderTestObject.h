#pragma once
#include <Objects/PolygonObject.h>
#include <Collision/CapsuleCollider.h>
#include <Collision/SphereCollider.h>

class ColliderTestObject : public PolygonObject{

	SphereCollider collider;


public:
	ColliderTestObject();
	~ColliderTestObject() = default;

	void Draw();
	void Update();
	void LateUpdate();
	
};