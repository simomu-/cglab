#pragma once
#include <array>
#include <iostream>
#include <Math/Vector3.h>

class Quaternion{

	std::array<float, 4> element;
	std::array<float, 16> matrix;

public:
	Quaternion();
	Quaternion(float rad, Vector3 dir);
	~Quaternion() = default;

	void Set(float w, float x, float y, float z);
	void SetFromEulerAngle(float x, float y, float z);
	void SetFromAxisAngle(float rad, Vector3 dir);

	float w() const;
	float x() const;
	float y() const;
	float z() const;

	void Normalize();
	float Length();

	Quaternion Conjugate();

	Vector3 GetEularAngle();

	std::array<float, 16> GetMatrix();

	Vector3 RotateVector(Vector3 vector);

	friend const Quaternion operator *( const Quaternion& lhs, const Quaternion& rhs ){
		Quaternion q;

		// this._w = qaw * qbw - qax * qbx - qay * qby - qaz * qbz;
		// this._x = qax * qbw + qaw * qbx + qay * qbz - qaz * qby;
		// this._y = qay * qbw + qaw * qby + qaz * qbx - qax * qbz;
		// this._z = qaz * qbw + qaw * qbz + qax * qby - qay * qbx;

		float w = lhs.w() * rhs.w() - lhs.x() * rhs.x() - lhs.y() * rhs.y() - lhs.z() * rhs.z();
		float x = lhs.w() * rhs.x() + lhs.x() * rhs.w() + lhs.y() * rhs.z() - lhs.z() * rhs.y();
		float y = lhs.w() * rhs.y() - lhs.x() * rhs.z() + lhs.y() * rhs.w() + lhs.z() * rhs.x();
		float z = lhs.w() * rhs.z() + lhs.x() * rhs.y() - lhs.y() * rhs.x() + lhs.z() * rhs.w();

		q.Set(w,x,y,z);
		return q;
	}

	friend std::ostream& operator <<( std::ostream& os, const Quaternion& rhs ){
		return os << rhs.w() << "," << rhs.x() << "," << rhs.y() << "," << rhs.z();
	}
};