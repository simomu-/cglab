#pragma once
#include <iostream>

class Vector3 {

	float element_x;
	float element_y;
	float element_z;

public:
	Vector3();
	Vector3(float x, float y, float z);
	~Vector3() = default;

	float x() const;
	float y() const;
	float z() const;

	void Set(float x, float y, float z);

	float Length();
	float Length2();

	void Normalize();
	Vector3 Normalized();

	float Dot(const Vector3 &vector);
	Vector3 Cross(Vector3 &vector);

	float* ToArray();
	float* ToArray(float value);

	Vector3 operator+=(const Vector3& lhs){
		element_x += lhs.x();
		element_y += lhs.y();
		element_z += lhs.z();
		return *this;
	}

	Vector3 operator-=(const Vector3& lhs){
		element_x -= lhs.x();
		element_y -= lhs.y();
		element_z -= lhs.z();
		return *this;
	}

	Vector3 operator*=(const float rhs){
		element_x *= rhs;
		element_y *= rhs;
		element_z *= rhs;
		return *this;
	}

	Vector3 operator/=(const float rhs){
		element_x /= rhs;
		element_y /= rhs;
		element_z /= rhs;
		return *this;
	}

	friend const Vector3 operator +(const Vector3& rhs, const Vector3& lhs){
		Vector3 v;
		v = rhs;
		return v += lhs;
	}

	friend const Vector3 operator -(const Vector3& rhs, const Vector3& lhs){
		Vector3 v;
		v = rhs;
		return  v -= lhs;
	}

	friend const Vector3 operator *(const Vector3& lhs, const float rhs){
		Vector3 v;
		v = lhs;
		return v *= rhs;
	}

	friend const Vector3 operator /(const Vector3& lhs, const float rhs){
		Vector3 v;
		v = lhs;
		return v /= rhs;
	}

	friend std::ostream& operator <<( std::ostream& os, const Vector3& rhs ){
		return os << rhs.x() << "," << rhs.y() << "," << rhs.z();
	}

	static Vector3 Forward();
	static Vector3 Up();
	static Vector3 Right();
	
};