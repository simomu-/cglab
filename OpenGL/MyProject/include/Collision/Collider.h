#pragma once
#include <string>
#include <Math/Vector3.h>

enum ColliderType{
	SPHERE,
	CAPSULE,
	COLLIDER_TYPE_COUNT
};

class Collider{
protected:
	std::string nameTag;
	Vector3 position;
	ColliderType colliderType;

	static bool isDebug;

public:
	Collider();
	Collider(std::string name);
	~Collider() = default;

	void SetPosition(float x, float y, float z);
	void SetPosition(Vector3 v);

	Vector3 GetPosition() const;

	virtual void DrawForDebug();
	virtual ColliderType GetColliderType() const;
	std::string GetNameTag();

};