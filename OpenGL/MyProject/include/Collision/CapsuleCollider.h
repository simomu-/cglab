#pragma once
#include <Collision/Collider.h>
#include <Math/Vector3.h>

class CapsuleCollider : public Collider{

	float radius;
	float length;
	Vector3 direction;

public:
	CapsuleCollider(float r, float l);
	~CapsuleCollider() = default;

	Vector3 GetDirection() const;
	Vector3 GetStartPoint() const;
	Vector3 GetEndPoint() const;
	float GetRadius() const;
	void GetColliderInfo(float& r, float& l, Vector3& d);
	ColliderType GetColliderType() const;

	void SetDirection(Vector3 d);

	void Delete();

	void DrawForDebug();


};