#pragma once
#include <Collision/Collision.h>
#include <Collision/Collider.h>
#include <array>

class FlexibleCollision : public Collision{

protected:
	Collision* collisionArray[COLLIDER_TYPE_COUNT][COLLIDER_TYPE_COUNT];

public:
	FlexibleCollision();
	~FlexibleCollision();

	bool IsCollision(Collider* c1, Collider* c2);
	
};