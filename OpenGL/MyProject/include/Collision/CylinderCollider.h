#pragma once
#include <Collision/Collider.h>
#include <Math/Vector3.h>

class CylinderCollider : public Collider{

protected:
	float radius;
	float length;
	Vector3 direction;
	Vector3 position;

public:
	CylinderCollider(float radius, float length, Vector3 dir);
	~CylinderCollider() = default;

	void Delete();
	void GetCylinderColliderInfo(float &r, float &l, Vector3 &dir);
	void SetPosition(float x, float y, float z);
	void SetDirection(Vector3 dir);

	void DrawForDebug();
	
};