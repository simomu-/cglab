#pragma once
#include <Collision/Collision.h>

class SphereAndSphere : public Collision{
public:
	SphereAndSphere() = default;
	~SphereAndSphere() = default;

	bool IsCollision(Collider* c1, Collider* c2);
	
};