#pragma once
#include <Collision/Collider.h>

class Collision{
public:
	Collision() = default;
	virtual ~Collision() = default;

	virtual bool IsCollision(Collider* c1, Collider* c2);
	
};