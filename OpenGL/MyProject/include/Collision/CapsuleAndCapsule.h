#pragma once
#include <Collision/Collision.h>
#include <Collision/CapsuleCollider.h>

class CapsuleAndCapsule : public Collision{
public:
	CapsuleAndCapsule() = default;
	~CapsuleAndCapsule() = default;

	bool IsCollision(Collider* c1, Collider* c2);
	float GetPointLineDistance(Vector3 p, CapsuleCollider l); 
	float GetPointSegmentDistance(Vector3 p, CapsuleCollider c);
	float GetSegmentSegmentDistance(CapsuleCollider c1, CapsuleCollider c2);
	
};