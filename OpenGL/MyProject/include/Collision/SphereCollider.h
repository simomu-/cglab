#pragma once
#include <Collision/Collider.h>
#include <string>

class SphereCollider : public Collider{
private:
	float radius;
public:
	SphereCollider(float r);
	SphereCollider(float r, std::string name);
	~SphereCollider();

	void Delete();
	float GetRadius() const;
	void GetSphereColliderInfo(float &x, float &y, float &z, float &r);	

	ColliderType GetColliderType() const;
	void DrawForDebug();
};