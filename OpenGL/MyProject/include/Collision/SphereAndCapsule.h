#pragma once
#include <Collision/Collision.h>
#include <Collision/SphereCollider.h>
#include <Collision/CapsuleCollider.h>

class SphereAndCapsule : public Collision{
public:
	SphereAndCapsule() = default;
	~SphereAndCapsule() = default;

	bool IsCollision(Collider* c1, Collider* c2);
	float GetPointSegmentDistance(Vector3 p, CapsuleCollider c);
	
};

