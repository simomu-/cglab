#pragma once
#include <Objects/Image2D.h>

class TestObject2D : public Image2D{

	float currentTime;
	float moveTime;

public:
	TestObject2D();
	~TestObject2D() = default;

	void Update();
};