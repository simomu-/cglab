#pragma once

class DisplaySetting {

	static float width;
	static float height;
	static float feildOfView;

public:
	DisplaySetting() = delete;
	~DisplaySetting();

	static int GetWidth() { return width;};
	static int GetHeight() { return height;};
	static float GetFieldOfView() { return feildOfView;};
	
};