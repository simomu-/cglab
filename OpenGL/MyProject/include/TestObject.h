#pragma once
#include <Objects/ObjectBase.h>
#include <Objects/PolygonObject.h>
#include <Objects/Camera.h>
#include <Collision/CapsuleCollider.h>

class TestObject : public ObjectBase{

	PolygonObject object1;
	PolygonObject object2;

public:
	TestObject();
	~TestObject() = default;

	void Draw();
	void Update();
	void LateUpdate();
	
};