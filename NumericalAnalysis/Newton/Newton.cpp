#include <iostream>
#include <cmath>

const int MAX = 50;
const double X0 = 1.0;
const double ESP_FX = 1.0e-6;

inline double f(double x){return (x - cos(x)) ;}
inline double df(double x){return (1 + sin(x)) ;}

int main(int argc, char const *argv[])
{
	double x = X0;
	double delta_x, fx_err;

	std::cout << "x = " << x << std::endl;

	for(int i = 0; i < MAX; i++){
		delta_x = -f(x) / df(x);
		x += delta_x;

		std::cout << "x = " << x << std::endl;

		fx_err = fabs(f(x));

		if(fx_err < ESP_FX) {
			break;
		}

	}

	return 0;
}