#include <kvs/Vector3>
#include <iostream>

int main(int argc, char const *argv[]){

	kvs::Vector3d vec1(1.0, 2.0, 3.0);
	kvs::Vector3d vec2(2.0, 4.0, 6.0);

	std::cout << "vec1 " << vec1 << std::endl;
	std::cout << "vec2 " << vec2 << std::endl;

	std::cout << "vec1 + vec2 = " << vec1 + vec2 << std::endl;
	std::cout << "vec1 - vec2 = " << vec1 - vec2 << std::endl;
	std::cout << "vec1 * vec2 = " << vec1 * vec2 << std::endl;
	std::cout << "vec1 / vec2 = " << vec1 / vec2 << std::endl;

	std::cout << "vec1 length = " << vec1.length() << std::endl;
	std::cout << "vec1 normalize = " << vec1.normalized() << std::endl; 


	kvs::Vector3d ex(1.0, 0.0, 0.0);
	kvs::Vector3d ey(0.0, 1.0, 0.0);

	std::cout << "ex dot ye = " << ex.dot(ey) << std::endl;
	std::cout << "ex cross ye = " << ex.cross(ey) << std::endl;

	return 0;
}